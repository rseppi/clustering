import sys, os, glob
import numpy as np
from astropy.table import Table
from scipy import integrate
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm

'''
Runs bootstrap on the clusters and computes N_bootstrap correlation functions with the same randoms
and estimates the covariance matrix

All the necessary files are created and deleted after use
The covariance matrixes are stored in correlation_functions/covariance_matrix

These are bootstrap errors (e.g. Marulli+20):
1/(Nr-1) * sum[ (cf_i - avg(cf_i) * (cf_j - avg(cf_j))) ]
This is exactly what np.cov computes

Remember that for jackknife errors this would be different (e.g. Veropalumbo+13):
(Nsub-1)/Nsub * sum[ (cf_i - avg(cf_i) * (cf_j - avg(cf_j))) ]

R.Seppi (MPE)
11.03.2021
'''

p_2_cat = os.path.join('/ptmp/rseppi/catalogs','MD40_eRO_CLU_b8_CM_0_pixS_20.0.fits')
p_2_rand = os.path.join('/u/rseppi/clustering','correlation_functions')
p_2_out = os.path.join('/u/rseppi/clustering','correlation_functions','covariance_matrix')

p_2_commands = os.path.join('/u/rseppi/clustering','correlation_functions','covariance_matrix')
CUTE_cmd = '/u/rseppi/.local/CUTE/CUTE/CUTE_NO_MPI '

N_bootstrap = 100
log_cov = False

def write_commands(const, Z_lim, Z_up):
    """
    Write data and random files to estimate clustering afterwards
    const: string corresponding to number of bootstrap
    """
    # saves data with and without weights
    p_2_data = os.path.join(p_2_out, 'catalog_Zmin_' + str(np.round(Z_lim, 1)) + '_Zmax_' + str(np.round(Z_up, 1)) + '_RA_DEC_Z_W_bootstrap_'+const+'.ascii')
    # random arrays, at least 10-20 times larger in size compared to the data array
    p_2_random = os.path.join(p_2_rand, 'catalog_Zmin_' + str(np.round(Z_lim, 1)) + '_Zmax_' + str(np.round(Z_up, 1)) + '_RA_DEC_Z_W_RANDOMS.ascii')
    # with weights, replicate redshift and weight arrays to assign to randoms
    # Monopole
    f = open(os.path.join(p_2_out,'commands_Zmin_' + str(np.round(Z_lim, 1)) + '_Zmax_' + str(np.round(Z_up, 1)) + '_bootstrap_'+const+'.monopole.ini'), 'w')
    f.write('data_filename= ' +	p_2_data +' \n')
    f.write('random_filename= ' + p_2_random +' \n')
    f.write('input_format= 2 \n')
    f.write('output_filename= ' + p_2_out+'/Zmin_' + str(np.round(Z_lim, 1)) + '_Zmax_' + str(np.round(Z_up, 1)) + '_bootstrap_'+const+'.monopole.2pcf \n')
    f.write('corr_type= monopole \n')
    f.write('omega_M= 0.307 \n')
    f.write('omega_L= 0.693 \n')
    f.write('w= -1 \n')
    f.write('log_bin= 0 \n')
    f.write('dim1_min_logbin= 0.1 \n')
    f.write('dim1_max= 150. \n')
    f.write('dim1_nbin= 50 \n')
    f.write('dim2_max= 100. \n')
    f.write('dim2_nbin= 30 \n')
    f.write('dim3_min= 0.00 \n')
    f.write('dim3_max= 3. \n')
    f.write('dim3_nbin= 1 \n')
    f.close()
    # 3D clustering
    f = open(os.path.join(p_2_out, 'commands_Zmin_' + str(np.round(Z_lim, 1)) + '_Zmax_' + str(np.round(Z_up, 1)) + '_bootstrap_'+const+'.3dps.ini'), 'w')
    f.write('data_filename= ' +	p_2_data +' \n')
    f.write('random_filename= ' + p_2_random +' \n')
    f.write('input_format= 2 \n')
    f.write('output_filename= ' + p_2_out+'/Zmin_' + str(np.round(Z_lim, 1)) + '_Zmax_' + str(np.round(Z_up, 1)) + '_bootstrap_'+const+'.3dps.2pcf \n')
    f.write('corr_type= 3D_ps \n')
    f.write('omega_M= 0.307 \n')
    f.write('omega_L= 0.693 \n')
    f.write('w= -1 \n')
    f.write('log_bin= 0 \n')
    f.write('dim1_min_logbin= 0.2 \n')
    f.write('dim1_max= 100. \n')
    f.write('dim1_nbin= 50 \n')
    f.write('dim2_max= 60. \n')
    f.write('dim2_nbin= 60 \n')
    f.write('dim3_min= 0.00 \n')
    f.write('dim3_max= 6. \n')
    f.write('dim3_nbin= 1 \n')
    f.close()
    # Angular clustering
    f = open(os.path.join(p_2_out, 'commands_Zmin_' + str(np.round(Z_lim, 1)) + '_Zmax_' + str(np.round(Z_up, 1)) + '_bootstrap_'+const+'.wtheta.ini'), 'w')
    f.write('data_filename= ' +	p_2_data +' \n')
    f.write('random_filename= ' + p_2_random +' \n')
    f.write('input_format= 2 \n')
    f.write('output_filename= ' + p_2_out+'/Zmin_' + str(np.round(Z_lim, 1)) + '_Zmax_' + str(np.round(Z_up, 1)) + '_bootstrap_'+const+'.wtheta.2pcf \n')
    f.write('corr_type= angular \n')
    f.write('omega_M= 0.307 \n')
    f.write('omega_L= 0.693 \n')
    f.write('w= -1 \n')
    f.write('log_bin= 1 \n')
    f.write('dim1_min_logbin= 0.005 \n')
    f.write('dim1_max= 10. \n')
    f.write('dim1_nbin= 40 \n')
    f.write('dim2_max= 160. \n')
    f.write('dim2_nbin= 40 \n')
    f.write('dim3_min= 0.00 \n')
    f.write('dim3_max= 3. \n')
    f.write('dim3_nbin= 1 \n')
    f.write('use_pm= 0 \n')
    f.write('n_pix_sph= 1000000 \n')
    f.close()


print('Reading HMD...')
t = Table.read(p_2_cat)


Z_mins = np.array([0.1,0.3,0.5,0.7])
Z_maxs = np.array([0.3,0.5,0.7,0.9])

print('Starting bootstrap...')

for j in range(N_bootstrap):
    jj = str(j).zfill(3)
    print(jj,' of ',N_bootstrap)

    for Z_lim, Z_up in zip(Z_mins, Z_maxs):
        #define paths
        p_2_data = os.path.join(p_2_out, 'catalog_Zmin_' + str(np.round(Z_lim, 1)) + '_Zmax_' + str(np.round(Z_up, 1)) + '_RA_DEC_Z_W_bootstrap_'+jj+'.ascii')

        #cut at M500c 1e14 (to apply /h) #cut z <0.8 >0.1
        z_cut = (t['redshift_S']>Z_lim)&(t['redshift_S']<Z_up)
        mass_cut = t['HALO_M500c']>1e14
        t_cut=t[(mass_cut)&(z_cut)]        # redshift_S in (0.1,0.8) and HALO_M500c > 1e14 ---> 233477 halos

        ra,dec,z = t_cut['RA'], t_cut['DEC'],t_cut['redshift_S']
        weights = np.ones(len(ra))

        size = 100
        sel_index = np.random.randint(0,len(ra),size=size)
        uu = np.random.uniform(size=size)

        ra[sel_index] = np.random.uniform(size=size) * 2 * np.pi * 180 / np.pi
        dec[sel_index] = np.arccos(1 - 2 * uu) * 180 / np.pi - 90.
        z[sel_index] = np.random.uniform(size=size) * 0.2 + Z_lim

        DATA = np.transpose( [ra, dec, z, weights])
        np.savetxt(p_2_data, DATA, fmt="%s")

        write_commands(const=jj,Z_lim=Z_lim,Z_up=Z_up)



commands = glob.glob(os.path.join(p_2_commands,'*ini'))
commands.sort()
print('Now running CUTE...')
for kk,cmd_file in enumerate(commands):
    print(kk,' of ', len(commands))
    print(cmd_file)
    if os.path.isfile(os.path.join(cmd_file[:61]+cmd_file[70:-3] + '2pcf')):
        print('done')
    else:
        cmd = CUTE_cmd + cmd_file
        os.system(cmd)

print('Now computing the covariance matrixes for each correlation function...')

for Z_lim, Z_up in zip(Z_mins, Z_maxs):
    p_2_angular_cf = sorted(glob.glob(os.path.join(p_2_out,'Zmin_'+ str(np.round(Z_lim, 1)) + '_Zmax_' + str(np.round(Z_up, 1))+'*wtheta.2pcf')))
    p_2_monopole_cf = sorted(glob.glob(os.path.join(p_2_out,'Zmin_'+ str(np.round(Z_lim, 1)) + '_Zmax_' + str(np.round(Z_up, 1))+'*monopole.2pcf')))
    p_2_ps_cf = sorted(glob.glob(os.path.join(p_2_out,'Zmin_'+ str(np.round(Z_lim, 1)) + '_Zmax_' + str(np.round(Z_up, 1))+'*3dps.2pcf')))

    th, angular_cf = np.loadtxt(p_2_angular_cf[0],unpack=True,usecols=[0,1],dtype='float32')
    angular_cf_log = np.log10(angular_cf)
    notnan = np.where(np.isnan(angular_cf_log))
    angular_cf_log[notnan]=-10
    outpl=os.path.join(p_2_out,'correlation_functions_wtheta_bootstrap_Zmin_' + str(np.round(Z_lim, 1)) + '_Zmax_' + str(np.round(Z_up, 1))+'.png')
    plt.figure(figsize=(6,6))
    plt.plot(th,angular_cf,c='C0')
    for p2wth in p_2_angular_cf[1:]:
        wth = np.loadtxt(p2wth, unpack=True,usecols=[1],dtype='float32')
        wth_log = np.log10(wth)
        notnan = np.where(np.isnan(wth_log))
        wth_log[notnan] = -10
        angular_cf_log = np.vstack([angular_cf_log,wth_log])
        angular_cf = np.vstack([angular_cf,wth])
        plt.plot(th,wth,c='C0')

    plt.xticks(fontsize=13)
    plt.yticks(fontsize=13)
    plt.ylim(1e-3)
    plt.yscale('log')
    plt.xscale('log')
    plt.xlabel(r'$\theta$ (deg)', fontsize=13)
    plt.ylabel(r'$w(\theta)$', fontsize=13)
    plt.grid(True)
    plt.tight_layout()
    plt.savefig(outpl, overwrite=True)
    plt.close()

    if(log_cov==True):
        covariance_wtheta = np.cov(angular_cf_log.T) #np.cov already has 1./(N_bootstrap - 1)
        p_2_cov_fig = os.path.join(p_2_out,'covariance_matrix_wtheta_log_Zmin_' + str(np.round(Z_lim, 1)) + '_Zmax_' + str(np.round(Z_up, 1))+'.png')
        p_2_covariance_wtheta = os.path.join(p_2_out, 'covariance_matrix_wtheta_log_Zmin_' + str(
            np.round(Z_lim, 1)) + '_Zmax_' + str(np.round(Z_up, 1)) + '.txt')
    else:
        covariance_wtheta = np.cov(angular_cf.T)
        p_2_cov_fig = os.path.join(p_2_out,'covariance_matrix_wtheta_Zmin_' + str(np.round(Z_lim, 1)) + '_Zmax_' + str(np.round(Z_up, 1))+'.png')
        p_2_covariance_wtheta = os.path.join(p_2_out, 'covariance_matrix_wtheta_Zmin_' + str(
            np.round(Z_lim, 1)) + '_Zmax_' + str(np.round(Z_up, 1)) + '.txt')

    extent_2d = [th[0], th[-1], th[-1], th[0]]
    fig = plt.figure(figsize=(6,6))
    im=plt.imshow(covariance_wtheta, extent=extent_2d)#, norm=LogNorm())
    plt.xlabel(r'$\theta$ [deg]', fontsize=13)
    plt.ylabel(r'$\theta$ [deg]', fontsize=13)
    plt.tick_params(labelsize=13)
    cb = fig.colorbar(im)
    cb.ax.tick_params(labelsize=13)
    fig.suptitle(r'$w(\theta)$ {:.2f}<z<{:.2f}'.format(Z_lim,Z_up))
    fig.tight_layout()
    fig.savefig(p_2_cov_fig)
    plt.close()

    np.savetxt(p_2_covariance_wtheta, covariance_wtheta, fmt="%s" )

    r, monopole_cf = np.loadtxt(p_2_monopole_cf[0],unpack=True,usecols=[0,1],dtype='float32')
    monopole_cf_log = np.log10(monopole_cf)
    notnan = np.where(np.isnan(monopole_cf_log))
    monopole_cf_log[notnan] = -10
    outpl=os.path.join(p_2_out,'correlation_functions_monopole_bootstrap_Zmin_' + str(np.round(Z_lim, 1)) + '_Zmax_' + str(np.round(Z_up, 1))+'.png')
    plt.figure(figsize=(6,6))
    plt.plot(r,monopole_cf,c='C0')
    for p2mono in p_2_monopole_cf[1:]:
        monop = np.loadtxt(p2mono,unpack=True,usecols=[1],dtype='float32')
        monop_log = np.log10(monop)
        notnan = np.where(np.isnan(monop_log))
        monop_log[notnan] = -10
        monopole_cf_log = np.vstack([monopole_cf_log,monop_log])
        monopole_cf = np.vstack([monopole_cf,monop])
        plt.plot(r,monop,c='C0')

    plt.xticks(fontsize=13)
    plt.yticks(fontsize=13)
    plt.yscale('log')
    plt.xscale('log')
    plt.xlabel(r'$r$ (Mpc/h)',fontsize=13)
    plt.ylabel(r'$\xi(r)$',fontsize=13)
    plt.grid(True)
    plt.tight_layout()
    plt.savefig(outpl, overwrite=True)
    plt.close()

    if(log_cov==True):
        covariance_monopole = np.cov(monopole_cf_log.T) #np.cov already has 1./(N_bootstrap - 1)
        p_2_covariance_monopole = os.path.join(p_2_out, 'covariance_matrix_monopole_log_Zmin_' + str(
            np.round(Z_lim, 1)) + '_Zmax_' + str(np.round(Z_up, 1)) + '.txt')
        p_2_cov_fig = os.path.join(p_2_out, 'covariance_matrix_monopole_log_Zmin_' + str(np.round(Z_lim, 1)) + '_Zmax_' + str(
            np.round(Z_up, 1)) + '.png')
    else:
        p_2_covariance_monopole = os.path.join(p_2_out, 'covariance_matrix_monopole_Zmin_' + str(
            np.round(Z_lim, 1)) + '_Zmax_' + str(np.round(Z_up, 1)) + '.txt')
        p_2_cov_fig = os.path.join(p_2_out, 'covariance_matrix_monopole_Zmin_' + str(np.round(Z_lim, 1)) + '_Zmax_' + str(
            np.round(Z_up, 1)) + '.png')
        covariance_monopole = np.cov(monopole_cf.T)

    extent_3d = [r[0], r[-1], r[-1], r[0]]
    fig = plt.figure(figsize=(6,6))
    im=plt.imshow(covariance_monopole, extent=extent_3d)#, norm=LogNorm())
    plt.xlabel('r [Mpc/h]', fontsize=13)
    plt.ylabel('r [Mpc/h]', fontsize=13)
    plt.tick_params(labelsize=13)
    cb = fig.colorbar(im)
    cb.ax.tick_params(labelsize=13)
    fig.suptitle(r'$\xi(r)$ {:.2f}<z<{:.2f}'.format(Z_lim,Z_up))
    fig.tight_layout()
    fig.savefig(p_2_cov_fig)
    plt.close()

    np.savetxt(p_2_covariance_monopole, covariance_monopole, fmt='%s')


    pi, rp, xi, = np.loadtxt(p_2_ps_cf[0], unpack=True, usecols=[0,1,2],dtype='float32')
    pi = np.unique(pi)
    rp = np.unique(rp)
    xi = xi.reshape(len(rp), len(pi))
    wp_all = np.empty(len(rp))
    for jj in range(len(wp_all)):
        wp_all[jj] = 2.0 * integrate.simps(xi[jj, :], pi)
    wp_all_log = np.log10(wp_all)
    notnan = np.where(np.isnan(wp_all_log))
    wp_all_log[notnan] = -10

    outpl=os.path.join(p_2_out,'correlation_functions_wprp_bootstrap_Zmin_' + str(np.round(Z_lim, 1)) + '_Zmax_' + str(np.round(Z_up, 1))+'.png')
    plt.figure(figsize=(6,6))
    plt.plot(rp,wp_all,c='C0')

    for p2ps in p_2_ps_cf[1:]:
        xi = np.loadtxt(p2ps, unpack=True, usecols=[2],dtype='float32')
        xi = xi.reshape(len(rp), len(pi))
        wp = np.empty(len(rp))
        for jj in range(len(wp)):
            wp[jj] = 2.0 * integrate.simps(xi[jj, :], pi)
        wp_log = np.log10(wp)
        notnan = np.where(np.isnan(wp_log))
        wp_log[notnan] = -10
        wp_all_log = np.vstack([wp_all_log,wp_log])
        wp_all = np.vstack([wp_all,wp])
        plt.plot(rp,wp,c='C0')

    plt.tick_params(labelsize=13)
    plt.xscale('log')
    plt.yscale('log')
    plt.xlabel(r'$r_p$ (Mpc/h)', fontsize=13)
    plt.ylabel(r'$w_p(r_p)$', fontsize=13)
    plt.grid(True)
    plt.tight_layout()
    plt.savefig(outpl, overwrite=True)
    plt.close()

    if(log_cov==True):
        p_2_covariance_wprp = os.path.join(p_2_out,
                                           'covariance_matrix_wprp_log_Zmin_' + str(np.round(Z_lim, 1)) + '_Zmax_' + str(
                                               np.round(Z_up, 1)) + '.txt')
        p_2_cov_fig = os.path.join(p_2_out, 'covariance_matrix_wprp_log_Zmin_' + str(np.round(Z_lim, 1)) + '_Zmax_' + str(
            np.round(Z_up, 1)) + '.png')

        covariance_wprp = np.cov(wp_all_log.T) #np.cov already has 1./(N_bootstrap - 1)
    else:
        p_2_covariance_wprp = os.path.join(p_2_out,
                                           'covariance_matrix_wprp_Zmin_' + str(np.round(Z_lim, 1)) + '_Zmax_' + str(
                                               np.round(Z_up, 1)) + '.txt')
        p_2_cov_fig = os.path.join(p_2_out, 'covariance_matrix_wprp_Zmin_' + str(np.round(Z_lim, 1)) + '_Zmax_' + str(
            np.round(Z_up, 1)) + '.png')
        covariance_wprp = np.cov(wp_all.T)

    extent_wp = [rp[0], rp[-1], rp[-1], rp[0]]
    fig = plt.figure(figsize=(6,6))
    im=plt.imshow(covariance_wprp, extent=extent_wp)#, norm=LogNorm())
    plt.xlabel(r'r$_p$ [Mpc/h]', fontsize=13)
    plt.ylabel(r'r$_p$ [Mpc/h]', fontsize=13)
    plt.tick_params(labelsize=13)
    cb = fig.colorbar(im)
    cb.ax.tick_params(labelsize=13)
    fig.suptitle(r'$w_p(r_p)$ {:.2f}<z<{:.2f}'.format(Z_lim,Z_up))
    fig.tight_layout()
    fig.savefig(p_2_cov_fig)
    plt.close()

    np.savetxt(p_2_covariance_wprp, covariance_wprp, fmt='%s')

print('removing bootstrap catalogs...')
cmd_rm0 = 'rm /u/rseppi/clustering/correlation_functions/covariance_matrix/*ascii'
print(cmd_rm0)
os.system(cmd_rm0)

print('removing input files for CUTE (bootstrap 2pcf)...')
cmd_rm1 = 'rm /u/rseppi/clustering/correlation_functions/covariance_matrix/*ini'
print(cmd_rm1)
os.system(cmd_rm1)

#print('removing all the bootstrap 2pcf...')
#cmd_rm2 = 'rm /u/rseppi/clustering/correlation_functions/covariance_matrix/*2pcf'
#print(cmd_rm2)
#os.system(cmd_rm2)

print('Only the covariance matrixes are left!')
print('done!')
