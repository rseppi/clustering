import sys, os, glob
import numpy as np
from astropy.table import Table
from scipy import integrate
import matplotlib.pyplot as plt
from colossus.cosmology import cosmology
from colossus.lss import bias
from matplotlib.colors import LogNorm

'''
Uses the boss patch mocks catalogs to compute 2048x3 correlation functions
and estimate covariance matrix

All the necessary files are created and deleted after use
The covariance matrixes are stored in correlation_functions/covariance_matrix

R.Seppi (MPE)
15.03.2021
'''

cosmology.setCosmology('multidark-planck')

multiply=True

p_2_mocks = os.path.join('/ptmp/rseppi/patchy_mocks')
p_2_cat = glob.glob(os.path.join(p_2_mocks,'realizations','*dat'))
rand_dir = os.path.join('/ptmp/rseppi/patchy_mocks','randoms')
p_2_rand = os.path.join(rand_dir,'Patchy-Mocks-Randoms-DR12NGC-COMPSAM_V6C_x10.dat')
p_2_out = os.path.join('/u/rseppi/clustering','correlation_functions','covariance_matrix')

p_2_commands = os.path.join('/u/rseppi/clustering','correlation_functions','covariance_matrix')
CUTE_cmd = '/u/rseppi/.local/CUTE/CUTE/CUTE '

def write_commands(const, Z_lim, Z_up):
    """
    Write data and random files to estimate clustering afterwards
    const: string corresponding to number of bootstrap
    """
    # saves data with and without weights
    p_2_data = os.path.join(p_2_mocks, 'realizations','catalogs', 'patchy_mocks_Zmin_' + str(np.round(Z_lim, 1)) + '_Zmax_' + str(np.round(Z_up, 1)) + '_RA_DEC_Z_'+jj+'.ascii')
    # random arrays, at least 10-20 times larger in size compared to the data array
    p_2_random = os.path.join(rand_dir, 'patchy_mocks_Zmin_' + str(np.round(Z_lim, 1)) + '_Zmax_' + str(np.round(Z_up, 1)) + '_RA_DEC_Z_RANDOMS.ascii')
    # with weights, replicate redshift and weight arrays to assign to randoms
    # 3D clustering
    f = open(os.path.join(p_2_out, 'commands_Zmin_' + str(np.round(Z_lim, 1)) + '_Zmax_' + str(np.round(Z_up, 1)) +'_'+const+'.3dps.ini'), 'w')
    f.write('data_filename= ' +	p_2_data +' \n')
    f.write('random_filename= ' + p_2_random +' \n')
    f.write('input_format= 2 \n')
    f.write('output_filename= ' + p_2_out+'/Zmin_' + str(np.round(Z_lim, 1)) + '_Zmax_' + str(np.round(Z_up, 1)) +'_'+const+'.3dps.2pcf \n')
    f.write('corr_type= 3D_ps \n')
    f.write('omega_M= 0.307 \n')
    f.write('omega_L= 0.693 \n')
    f.write('w= -1 \n')
    f.write('log_bin= 1 \n')
    f.write('dim1_min_logbin= 0.2 \n')
    f.write('dim1_max= 100. \n')
    f.write('dim1_nbin= 50 \n')
    f.write('dim2_max= 60. \n')
    f.write('dim2_nbin= 60 \n')
    f.write('dim3_min= 0.00 \n')
    f.write('dim3_max= 6. \n')
    f.write('dim3_nbin= 1 \n')
    f.close()
    # Angular clustering
    #f = open(os.path.join(p_2_out, 'commands_Zmin_' + str(np.round(Z_lim, 1)) + '_Zmax_' + str(np.round(Z_up, 1)) +'_'+const+'.wtheta.ini'), 'w')
    #f.write('data_filename= ' +	p_2_data +' \n')
    #f.write('random_filename= ' + p_2_random +' \n')
    #f.write('input_format= 2 \n')
    #f.write('output_filename= ' + p_2_out+'/Zmin_' + str(np.round(Z_lim, 1)) + '_Zmax_' + str(np.round(Z_up, 1)) +'_'+const+'.wtheta.2pcf \n')
    #f.write('corr_type= angular \n')
    #f.write('omega_M= 0.307 \n')
    #f.write('omega_L= 0.693 \n')
    #f.write('w= -1 \n')
    #f.write('log_bin= 1 \n')
    #f.write('dim1_min_logbin= 0.005 \n')
    #f.write('dim1_max= 10. \n')
    #f.write('dim1_nbin= 40 \n')
    #f.write('dim2_max= 160. \n')
    #f.write('dim2_nbin= 40 \n')
    #f.write('dim3_min= 0.00 \n')
    #f.write('dim3_max= 3. \n')
    #f.write('dim3_nbin= 1 \n')
    #f.write('use_pm= 0 \n')
    #f.write('n_pix_sph= 1000000 \n')
    #f.close()
    # Monopole
    #f = open(os.path.join(p_2_out,'commands_Zmin_' + str(np.round(Z_lim, 1)) + '_Zmax_' + str(np.round(Z_up, 1)) +'_' +const+'.monopole.ini'), 'w')
    #f.write('data_filename= ' +	p_2_data +' \n')
    #f.write('random_filename= ' + p_2_random +' \n')
    #f.write('input_format= 2 \n')
    #f.write('output_filename= ' + p_2_out+'/Zmin_' + str(np.round(Z_lim, 1)) + '_Zmax_' + str(np.round(Z_up, 1)) +'_' +const+'.monopole.2pcf \n')
    #f.write('corr_type= monopole \n')
    #f.write('omega_M= 0.307 \n')
    #f.write('omega_L= 0.693 \n')
    #f.write('w= -1 \n')
    #f.write('log_bin= 0 \n')
    #f.write('dim1_min_logbin= 0.1 \n')
    #f.write('dim1_max= 150. \n')
    #f.write('dim1_nbin= 50 \n')
    #f.write('dim2_max= 100. \n')
    #f.write('dim2_nbin= 30 \n')
    #f.write('dim3_min= 0.00 \n')
    #f.write('dim3_max= 3. \n')
    #f.write('dim3_nbin= 1 \n')
    #f.close()


Z_mins = np.array([0.1,0.3,0.5,0.7])
Z_maxs = np.array([0.3,0.5,0.7,0.9])
M500c_av = np.array([1.05872e14,1.0004e14,9.43983e13,8.89497e13])
b = bias.haloBias(M500c_av, model='comparat17', z=(Z_mins+Z_maxs)/2., mdef='500c')
#bias_ratio = np.array([3.3995003964923782/2.0980324003710615, 3.990489683911425/2.172234802429641, 4.732889931811365/2.0580882806283576, 5.5844459331199765/2.357598557419572])
bias_ratio = np.array([b[0]/2.0980324003710615, b[1]/2.172234802429641, b[2]/2.0580882806283576, b[3]/2.357598557419572])

'''
print('preparing random catalogs...')
for Z_lim, Z_up in zip(Z_mins, Z_maxs):
    p_2_rand_out = os.path.join(rand_dir, 'patchy_mocks_Zmin_' + str(np.round(Z_lim, 1)) + '_Zmax_' + str(
        np.round(Z_up, 1)) + '_RA_DEC_Z_RANDOMS.ascii')
    if(os.path.isfile(p_2_rand_out)):
        continue
    else:
        randoms = np.loadtxt(p_2_rand, unpack=True)
        ra, dec, z, nbar, bias, veto, flag = randoms
        sel = (z>=Z_lim)&(z<Z_up)
        ra,dec,z,nbar,bias,veto,flag = ra[sel],dec[sel],z[sel],nbar[sel],bias[sel],veto[sel],flag[sel]
        weights = (veto * flag) / (1 + 10000 * nbar)

        RANDOMS = np.transpose([ra, dec, z, weights])
        np.savetxt(p_2_rand_out, RANDOMS, fmt='%s')

print('Start processing realizations...')

for j,cat in enumerate(p_2_cat):
    jj = str(j).zfill(4)
    print(jj,' of ',len(p_2_cat))

    for Z_lim, Z_up in zip(Z_mins, Z_maxs):
        #define paths
        p_2_cat_out = os.path.join(p_2_mocks, 'realizations', 'catalogs','patchy_mocks_Zmin_' + str(np.round(Z_lim, 1)) + '_Zmax_' + str(np.round(Z_up, 1)) + '_RA_DEC_Z_'+jj+'.ascii')
        write_commands(const=jj,Z_lim=Z_lim,Z_up=Z_up)

        if(os.path.isfile(p_2_cat_out)):
            continue
        else:
            data = np.loadtxt(cat,unpack=True)
            ra,dec,z,mstar,nbar,bias,veto,fiber = data
            sel = (z >= Z_lim) & (z < Z_up)
            ra, dec, z, mstar, nbar, bias, veto, fiber = ra[sel],dec[sel],z[sel],mstar[sel],nbar[sel],bias[sel],veto[sel],fiber[sel]
            weights = (veto * fiber) / (1 + 10000 * nbar)
            DATA = np.transpose( [ra, dec, z, weights])
            np.savetxt(p_2_cat_out, DATA, fmt="%s")
'''

#commands = glob.glob(os.path.join(p_2_commands,'*ini'))
#commands.sort()
#print('Now running CUTE...')
#for kk,cmd_file in enumerate(commands):
#    print(kk,' of ', len(commands))
#    print(cmd_file)
#    if os.path.isfile(os.path.join(cmd_file[:61]+cmd_file[70:-3] + '2pcf')):
#        print('done')
#    else:
#        cmd = CUTE_cmd + cmd_file
#        os.system(cmd)

print('Now computing the covariance matrixes for each correlation function...')

for Z_lim, Z_up, br in zip(Z_mins, Z_maxs, bias_ratio):
    p_2_angular_cf = sorted(glob.glob(os.path.join(p_2_out,'Zmin_'+ str(np.round(Z_lim, 1)) + '_Zmax_' + str(np.round(Z_up, 1))+'*wtheta.2pcf')))
    p_2_monopole_cf = sorted(glob.glob(os.path.join(p_2_out,'Zmin_'+ str(np.round(Z_lim, 1)) + '_Zmax_' + str(np.round(Z_up, 1))+'*monopole.2pcf')))
    p_2_ps_cf = sorted(glob.glob(os.path.join(p_2_out,'Zmin_'+ str(np.round(Z_lim, 1)) + '_Zmax_' + str(np.round(Z_up, 1))+'*3dps.2pcf')))

    th, angular_cf = np.loadtxt(p_2_angular_cf[0],unpack=True,usecols=[0,1],dtype='float32')
    angular_cf = angular_cf * br**2
    th_angular_cf = th*angular_cf
    outpl=os.path.join(p_2_out,'correlation_functions_wtheta_patchy_mocks_Zmin_' + str(np.round(Z_lim, 1)) + '_Zmax_' + str(np.round(Z_up, 1))+'.png')
    plt.figure(figsize=(6,6))
    plt.plot(th,angular_cf,c='C0')
    for p2wth in p_2_angular_cf[1:]:
        wth = np.loadtxt(p2wth, unpack=True,usecols=[1],dtype='float32')
        wth = wth * br**2
        th_wth = th*wth
        angular_cf = np.vstack([angular_cf,wth])
        th_angular_cf = np.vstack([th_angular_cf, th_wth])
        plt.plot(th,wth,c='C0')

    plt.xticks(fontsize=13)
    plt.yticks(fontsize=13)
    plt.ylim(1e-3)
    plt.yscale('log')
    plt.xscale('log')
    plt.xlabel(r'$\theta$ (deg)', fontsize=13)
    plt.ylabel(r'$w(\theta)$', fontsize=13)
    plt.grid(True)
    plt.tight_layout()
    plt.savefig(outpl, overwrite=True)
    plt.close()

    if(multiply==True):
        covariance_wtheta = np.cov(th_angular_cf.T) #np.cov already has 1./(N_bootstrap - 1)
        p_2_cov_fig = os.path.join(p_2_out,'covariance_matrix_theta_wtheta_Zmin_' + str(np.round(Z_lim, 1)) + '_Zmax_' + str(np.round(Z_up, 1))+'.png')
        p_2_covariance_wtheta = os.path.join(p_2_out, 'covariance_matrix_theta_wtheta_Zmin_' + str(
            np.round(Z_lim, 1)) + '_Zmax_' + str(np.round(Z_up, 1)) + '.txt')
    else:
        covariance_wtheta = np.cov(angular_cf.T)
        p_2_cov_fig = os.path.join(p_2_out,'covariance_matrix_wtheta_Zmin_' + str(np.round(Z_lim, 1)) + '_Zmax_' + str(np.round(Z_up, 1))+'.png')
        p_2_covariance_wtheta = os.path.join(p_2_out, 'covariance_matrix_wtheta_Zmin_' + str(
            np.round(Z_lim, 1)) + '_Zmax_' + str(np.round(Z_up, 1)) + '.txt')

    extent_2d = [th[0], th[-1], th[-1], th[0]]
    fig = plt.figure(figsize=(6,6))
    im=plt.imshow(covariance_wtheta, extent=extent_2d)#, norm=LogNorm())
    plt.xlabel(r'$\theta$ [deg]', fontsize=13)
    plt.ylabel(r'$\theta$ [deg]', fontsize=13)
    plt.tick_params(labelsize=13)
    cb = fig.colorbar(im)
    cb.ax.tick_params(labelsize=13)
    fig.suptitle(r'$w(\theta)$ {:.2f}<z<{:.2f}'.format(Z_lim,Z_up))
    fig.tight_layout()
    fig.savefig(p_2_cov_fig)
    plt.close()

    np.savetxt(p_2_covariance_wtheta, covariance_wtheta, fmt="%s" )

    r, monopole_cf = np.loadtxt(p_2_monopole_cf[0],unpack=True,usecols=[0,1],dtype='float32')
    monopole_cf = monopole_cf * br**2
    r2_monopole_cf = r**2*monopole_cf
    outpl=os.path.join(p_2_out,'correlation_functions_monopole_patchy_mocks_Zmin_' + str(np.round(Z_lim, 1)) + '_Zmax_' + str(np.round(Z_up, 1))+'.png')
    plt.figure(figsize=(6,6))
    plt.plot(r,monopole_cf,c='C0')
    for p2mono in p_2_monopole_cf[1:]:
        monop = np.loadtxt(p2mono,unpack=True,usecols=[1],dtype='float32')
        monop = monop * br**2
        r2_monop = r**2 * monop
        monopole_cf = np.vstack([monopole_cf,monop])
        r2_monopole_cf = np.vstack([r2_monopole_cf, r2_monop])
        plt.plot(r,monop,c='C0')

    plt.xticks(fontsize=13)
    plt.yticks(fontsize=13)
    plt.yscale('log')
    plt.xscale('log')
    plt.xlabel(r'$r$ (Mpc/h)',fontsize=13)
    plt.ylabel(r'$\xi(r)$',fontsize=13)
    plt.grid(True)
    plt.tight_layout()
    plt.savefig(outpl, overwrite=True)
    plt.close()

    if(multiply==True):
        covariance_monopole = np.cov(r2_monopole_cf.T) #np.cov already has 1./(N_bootstrap - 1)
        p_2_covariance_monopole = os.path.join(p_2_out, 'covariance_matrix_r2_monopole_Zmin_' + str(
            np.round(Z_lim, 1)) + '_Zmax_' + str(np.round(Z_up, 1)) + '.txt')
        p_2_cov_fig = os.path.join(p_2_out, 'covariance_matrix_r2_monopole_Zmin_' + str(np.round(Z_lim, 1)) + '_Zmax_' + str(
            np.round(Z_up, 1)) + '.png')
    else:
        p_2_covariance_monopole = os.path.join(p_2_out, 'covariance_matrix_monopole_Zmin_' + str(
            np.round(Z_lim, 1)) + '_Zmax_' + str(np.round(Z_up, 1)) + '.txt')
        p_2_cov_fig = os.path.join(p_2_out, 'covariance_matrix_monopole_Zmin_' + str(np.round(Z_lim, 1)) + '_Zmax_' + str(
            np.round(Z_up, 1)) + '.png')
        covariance_monopole = np.cov(monopole_cf.T)

    extent_3d = [r[0], r[-1], r[-1], r[0]]
    fig = plt.figure(figsize=(6,6))
    im=plt.imshow(covariance_monopole, extent=extent_3d)#, norm=LogNorm())
    plt.xlabel('r [Mpc/h]', fontsize=13)
    plt.ylabel('r [Mpc/h]', fontsize=13)
    plt.tick_params(labelsize=13)
    cb = fig.colorbar(im)
    cb.ax.tick_params(labelsize=13)
    fig.suptitle(r'$\xi(r)$ {:.2f}<z<{:.2f}'.format(Z_lim,Z_up))
    fig.tight_layout()
    fig.savefig(p_2_cov_fig)
    plt.close()

    np.savetxt(p_2_covariance_monopole, covariance_monopole, fmt='%s')


    pi, rp, xi, = np.loadtxt(p_2_ps_cf[0], unpack=True, usecols=[0,1,2],dtype='float32')
    pi = np.unique(pi)
    rp = np.unique(rp)
    xi = xi.reshape(len(rp), len(pi))
    wp_all = np.empty(len(rp))
    for jj in range(len(wp_all)):
        wp_all[jj] = 2.0 * integrate.simps(xi[jj, :], pi)
    wp_all = wp_all * br**2
    rp_wp_all = rp*wp_all

    outpl=os.path.join(p_2_out,'correlation_functions_wprp_patchy_mocks_Zmin_' + str(np.round(Z_lim, 1)) + '_Zmax_' + str(np.round(Z_up, 1))+'.png')
    plt.figure(figsize=(6,6))
    plt.plot(rp,wp_all,c='C0')

    for p2ps in p_2_ps_cf[1:]:
        xi = np.loadtxt(p2ps, unpack=True, usecols=[2],dtype='float32')
        xi = xi.reshape(len(rp), len(pi))
        wp = np.empty(len(rp))
        for jj in range(len(wp)):
            wp[jj] = 2.0 * integrate.simps(xi[jj, :], pi)
        wp = wp * br**2
        rp_wp = rp*wp
        wp_all = np.vstack([wp_all,wp])
        rp_wp_all = np.vstack([rp_wp_all,rp_wp])
        plt.plot(rp,wp,c='C0')

    plt.tick_params(labelsize=13)
    plt.xscale('log')
    plt.yscale('log')
    plt.xlabel(r'$r_p$ (Mpc/h)', fontsize=13)
    plt.ylabel(r'$w_p(r_p)$', fontsize=13)
    plt.grid(True)
    plt.tight_layout()
    plt.savefig(outpl, overwrite=True)
    plt.close()

    if(multiply==True):
        p_2_covariance_wprp = os.path.join(p_2_out,
                                           'covariance_matrix_rp_wprp_Zmin_' + str(np.round(Z_lim, 1)) + '_Zmax_' + str(
                                               np.round(Z_up, 1)) + '.txt')
        p_2_cov_fig = os.path.join(p_2_out, 'covariance_matrix_rp_wprp_Zmin_' + str(np.round(Z_lim, 1)) + '_Zmax_' + str(
            np.round(Z_up, 1)) + '.png')

        covariance_wprp = np.cov(rp_wp_all.T) #np.cov already has 1./(N_bootstrap - 1)
    else:
        p_2_covariance_wprp = os.path.join(p_2_out,
                                           'covariance_matrix_wprp_Zmin_' + str(np.round(Z_lim, 1)) + '_Zmax_' + str(
                                               np.round(Z_up, 1)) + '.txt')
        p_2_cov_fig = os.path.join(p_2_out, 'covariance_matrix_wprp_Zmin_' + str(np.round(Z_lim, 1)) + '_Zmax_' + str(
            np.round(Z_up, 1)) + '.png')
        covariance_wprp = np.cov(wp_all.T)

    extent_wp = [rp[0], rp[-1], rp[-1], rp[0]]
    fig = plt.figure(figsize=(6,6))
    im=plt.imshow(covariance_wprp, extent=extent_wp)#, norm=LogNorm())
    plt.xlabel(r'r$_p$ [Mpc/h]', fontsize=13)
    plt.ylabel(r'r$_p$ [Mpc/h]', fontsize=13)
    plt.tick_params(labelsize=13)
    cb = fig.colorbar(im)
    cb.ax.tick_params(labelsize=13)
    fig.suptitle(r'$w_p(r_p)$ {:.2f}<z<{:.2f}'.format(Z_lim,Z_up))
    fig.tight_layout()
    fig.savefig(p_2_cov_fig)
    plt.close()

    np.savetxt(p_2_covariance_wprp, covariance_wprp, fmt='%s')

#print('removing bootstrap catalogs...')
#cmd_rm0 = 'rm /u/rseppi/clustering/correlation_functions/covariance_matrix/*ascii'
#cmd_rm0 = 'rm /ptmp/rseppi/patchy_mocks/realizations/catalogs/*ascii && rm /ptmp/rseppi/patchy_mocks/randoms/*ascii'
#print(cmd_rm0)
#os.system(cmd_rm0)

print('removing input files for CUTE...')
cmd_rm1 = 'rm /u/rseppi/clustering/correlation_functions/covariance_matrix/*ini'
print(cmd_rm1)
os.system(cmd_rm1)

#print('removing all the bootstrap 2pcf...')
#cmd_rm2 = 'rm /u/rseppi/clustering/correlation_functions/covariance_matrix/*2pcf'
#print(cmd_rm2)
#os.system(cmd_rm2)

print('Only the covariance matrixes are left!')
print('done!')
