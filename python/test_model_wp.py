import sys,os,glob
import numpy as np
import pyccl as ccl
from astropy.table import Table
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import corner
from colossus.cosmology import cosmology
from colossus.lss import bias
from colossus.lss import peaks
import ultranest
from ultranest.plot import PredictionBand
from scipy import integrate

'''
Test code to compute clustering and fits cosmology
Uses eroconda (rseppipy) and pyccl

Needs AXV instructions

R.Seppi (MPE)
04.02.2021

Fitting rp>1Mpc/h and rp<70Mpc/h
'''
p_2_cat = os.path.join('/home/rseppi/Downloads','MD40_eRO_CLU_b8_CM_0_pixS_20.0.fits')
p_2_data = sorted(glob.glob(os.path.join('/home/rseppi/clustering','correlation_functions','*3dps.2pcf')))
outdir = os.path.join('/home/rseppi/clustering','figures','wp_limit')
logdir = os.path.join('/home/rseppi/clustering','my_fit_wp')

#p_2_cat = os.path.join('/ptmp/rseppi/catalogs','MD40_eRO_CLU_b8_CM_0_pixS_20.0.fits')
#p_2_data = sorted(glob.glob(os.path.join('/u/rseppi/clustering','correlation_functions','*3dps.2pcf')))
#outdir = os.path.join('/u/rseppi/clustering','figures')
#logdir = os.path.join('/u/rseppi/clustering','my_fit_wp')


# Turn off multi-threading to avoid issues with MPIPool.
os.environ["OMP_NUM_THREADS"] = "1"

#Use MultiDark cosmology
Om_MD = 0.307115 #matter
Ob_MD = 0.048206 #baryon
Oc_MD = Om_MD-Ob_MD #cold dark matter
print('Setting up cosmology...')
cosmo_planck18 = ccl.Cosmology(
    Omega_c = 0.11933/0.6766**2, # 0.26066
    Omega_b = 0.02242/0.6766**2, # 0.04897
    h = 0.6766,
    n_s = 0.9665,
    sigma8 = 0.8228,
    #A_s = np.e**(3.047)/10**10, # 2.1052e-09
    Omega_k = 0.0,
    Omega_g = None,
    Neff = 3.04, # standard model of particle physics
    m_nu = 0.0, # in eV
    m_nu_type = None, # 'inverted', 'normal', 'equal' or 'list'
    w0 = -1.0,
    wa = 0.0,
    T_CMB = ccl.physical_constants.T_CMB, # 2.725
    bcm_log10Mc=14.079181246047625, # BCM baryon correction model on PK https://arxiv.org/abs/1510.06034
    bcm_etab=0.5,                   # BCM
    bcm_ks=55.0,                    # BCM
    mu_0=0.0,                       # modified gravity model parameter
    sigma_0=0.0,                    # modified gravity model parameter
    z_mg=None,                      # modified growth rate parameter
    df_mg=None,                     # modified growth rate parameter
    transfer_function='boltzmann_camb',  # for power spectrum analysis
    matter_power_spectrum='halofit',     # for power spectrum analysis
    baryons_power_spectrum='nobaryons',  # for power spectrum analysis
    mass_function='tinker10',            # for mass function analysis
    halo_concentration='duffy2008',      # for mass function analysis
    emulator_neutrinos='strict',         # for power spectrum analysis
)
cosmo = cosmo_planck18
#cosmo = ccl.Cosmology(Omega_c = Oc_MD, Omega_b = Ob_MD, h = 0.6777, sigma8 = 0.8228, n_s = 0.96)
cosmo_colossus = cosmology.setCosmology('multidark-planck')

#get distribution with redshift
print('Reading table...')
t0=Table.read(p_2_cat)
mass_cut = t0['HALO_M500c']>1e14
t_=t0[(mass_cut)]
print('Now work on the model')
def model(x_rp, x_pi, z_low, z_up, pars):
    '''
    :param x_rp: array. Separation perpendicular to the line of sight in Mpc/h
    :param pi: array. Separation along the line of sight in Mpc/h
    :param pars: cosmological parameters. Available: OmegaM, sigma8, h, b.
    :return: projected correlation function wp(rp). Same dimension of x
    '''
    #Om,s8,h,b = pars
    Om,s8, h = pars
    Oc = Om - Ob_MD
    cosmol = ccl.Cosmology(
        Omega_c = Oc, # 0.26066
        Omega_b = 0.02242/0.6766**2, # 0.04897
        h = h,
        n_s = 0.9665,
        sigma8 = s8,
        Omega_k = 0.0,
        Omega_g = None,
        Neff = 3.04, # standard model of particle physics
        m_nu = 0.0, # in eV
        m_nu_type = None, # 'inverted', 'normal', 'equal' or 'list'
        w0 = -1.0,
        wa = 0.0,
        T_CMB = ccl.physical_constants.T_CMB, # 2.725
        bcm_log10Mc=14.079181246047625, # BCM baryon correction model on PK https://arxiv.org/abs/1510.06034
        bcm_etab=0.5,                   # BCM
        bcm_ks=55.0,                    # BCM
        mu_0=0.0,                       # modified gravity model parameter
        sigma_0=0.0,                    # modified gravity model parameter
        z_mg=None,                      # modified growth rate parameter
        df_mg=None,                     # modified growth rate parameter
        transfer_function='boltzmann_camb',  # for power spectrum analysis
        matter_power_spectrum='halofit',     # for power spectrum analysis
        baryons_power_spectrum='nobaryons',  # for power spectrum analysis
        mass_function='tinker10',            # for mass function analysis
        halo_concentration='duffy2008',      # for mass function analysis
        emulator_neutrinos='strict',         # for power spectrum analysis
    )

    z_cut = (t_['redshift_S'] > z_low) & (t_['redshift_S'] < z_up)
    t = t_[z_cut]
    halo_mass = np.average(t['HALO_M500c'])
    a = 1 / (1 + np.average(t['redshift_S']))
    f = ccl.background.growth_rate(cosmol, a)
    b = ccl.massfunction.halo_bias(cosmol, float(halo_mass/h), a, overdensity=500)
    beta = f / b
    xi_model = np.empty((len(x_pi), len(x_rp)))
    for jj, pi_ in enumerate(x_pi):
        xi_model[jj] = ccl.correlation_pi_sigma(cosmol, a=a, beta=beta, pi=pi_/h,sig=x_rp/h)
    #in ccl.correlation_pi_sigma beta is used to model redshift space distrotion
    #We still need to rescale the correlation function from large scale to halos.
    xi_model = xi_model * b**2
    wp_model = np.empty(len(x_rp))
    for jj in range(len(wp_model)):
        wp_model[jj] = 2.0*integrate.simps(xi_model[:, jj], x_pi)
    rp_wp_model = x_rp * wp_model
    return rp_wp_model


Z_mins = np.array([0.1,0.3,0.5,0.7])
Z_maxs = np.array([0.3,0.5,0.7,0.9])

for data, zmin, zmax in zip(p_2_data, Z_mins, Z_maxs):
    print('redshift {:.2f} {:.2f}'.format(zmin,zmax))
    plt.figure(figsize=(6,6))
    outfig = os.path.join(outdir, 'Zmin_{:.2f}_Zmax_{:.2f}_wprp_models.png'.format(zmin,zmax))

    rp=np.logspace(0,2,50)
    pi = np.linspace(0,60,60)
    param = [[0.31,0.8228,0.6777],[0.25,0.75,0.7],[0.25,1.0,0.7],[0.28,0.78,0.65],
             [0.28,0.78,0.75],[0.33,0.82,0.67],
             [0.35,0.85,0.65],[0.35,0.85,0.75],
             [0.35,1.0,0.7]]
    for p in param:
        parameters = np.array(p)#,4.2])
        label = [str(pa) for pa in p]
        test_wp_model = model(rp,pi,zmin,zmax,parameters)
        plt.plot(rp, test_wp_model, label=label)

    pi_data, rp_data, xi_data, DD_cts, DR_cts, RR_cts = np.loadtxt(data, unpack=True, usecols=[0,1,2,3,4,6])#,dtype='float32')

    pi_data = np.unique(pi_data)
    rp_data = np.unique(rp_data)
    xi_data = xi_data.reshape(len(rp_data), len(pi_data))
    wp_data = np.empty(len(rp_data))
    for jj in range(len(wp_data)):
        wp_data[jj] = 2.0 * integrate.simps(xi_data[jj, :], pi_data)

    plt.scatter(rp_data,rp_data*wp_data,label='data',color='black')

    plt.title('z={:.1f}'.format(zmin)+'-{:.1f}'.format(zmax), fontsize=13)
    plt.tick_params(labelsize=13)
    plt.xscale('log')
    plt.legend(fontsize=11)
    plt.xlabel(r'$r_p$ (Mpc/h)',fontsize=13)
    plt.ylabel(r'$r_p w_p(r_p)$',fontsize=13)
    plt.grid(True)
    plt.tight_layout()
    plt.savefig(outfig)
    plt.close()

print('done!')