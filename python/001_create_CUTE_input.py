import numpy as np
import os, sys, glob
import astropy.io.fits as fits
from astropy.table import Table, Column

'''
Creates input files to compute wprp clustering with CUTE

R.Seppi(MPE)
26.02.2021
'''
p_2_cat = os.path.join('/ptmp/rseppi/catalogs','MD40_eRO_CLU_b8_CM_0_pixS_20.0.fits')
p_2_out = os.path.join('/u/rseppi/clustering','correlation_functions')

#p_2_cat = os.path.join('/home/rseppi/Downloads','MD40_eRO_CLU_b8_CM_0_pixS_20.0.fits')
#p_2_out = os.path.join('/home/rseppi/clustering','correlation_functions')

Z_mins = np.array([0.1,0.3,0.5,0.7])
Z_maxs = np.array([0.3,0.5,0.7,0.9])

def write_commands():
	"""
	Write data and random files to estimate clustering afterwards
	A set with weights and photoz PDF
	A set without weights and direct photoZ value
	"""
	# likelihood selection
	# loops over redshift values
	for Z_lim, Z_up in zip(Z_mins, Z_maxs):
		# saves data with and without weights
		p_2_data = os.path.join(p_2_out, 'catalog_Zmin_' + str(np.round(Z_lim, 1)) + '_Zmax_' + str(np.round(Z_up, 1)) + '_RA_DEC_Z_W.ascii')
		# random arrays, at least 10-20 times larger in size compared to the data array
		p_2_random = os.path.join(p_2_out, 'catalog_Zmin_' + str(np.round(Z_lim, 1)) + '_Zmax_' + str(np.round(Z_up, 1)) + '_RA_DEC_Z_W_RANDOMS.ascii')
		# with weights, replicate redshift and weight arrays to assign to randoms
		# Monopole
		f = open(os.path.join(p_2_out,'commands_Zmin_' + str(np.round(Z_lim, 1)) + '_Zmax_' + str(np.round(Z_up, 1)) + '.monopole.ini'), 'w')
		f.write('data_filename= ' +	p_2_data +' \n')
		f.write('random_filename= ' + p_2_random +' \n')
		f.write('input_format= 2 \n')
		f.write('output_filename= ' + p_2_out+'/Zmin_' + str(np.round(Z_lim, 1)) + '_Zmax_' + str(np.round(Z_up, 1)) + '.monopole.2pcf \n')
		f.write('corr_type= monopole \n')
		f.write('omega_M= 0.307 \n')
		f.write('omega_L= 0.693 \n')
		f.write('w= -1 \n')
		f.write('log_bin= 0 \n')
		f.write('dim1_min_logbin= 0.1 \n')
		f.write('dim1_max= 150. \n')
		f.write('dim1_nbin= 50 \n')
		f.write('dim2_max= 100. \n')
		f.write('dim2_nbin= 30 \n')
		f.write('dim3_min= 0.00 \n')
		f.write('dim3_max= 3. \n')
		f.write('dim3_nbin= 1 \n')
		f.close()
		# 3D clustering
		f = open(os.path.join(p_2_out, 'commands_Zmin_' + str(np.round(Z_lim, 1)) + '_Zmax_' + str(np.round(Z_up, 1)) + '.3dps.ini'), 'w')
		f.write('data_filename= ' +	p_2_data +' \n')
		f.write('random_filename= ' + p_2_random +' \n')
		f.write('input_format= 2 \n')
		f.write('output_filename= ' + p_2_out+'/Zmin_' + str(np.round(Z_lim, 1)) + '_Zmax_' + str(np.round(Z_up, 1)) + '.3dps.2pcf \n')
		f.write('corr_type= 3D_ps \n')
		f.write('omega_M= 0.307 \n')
		f.write('omega_L= 0.693 \n')
		f.write('w= -1 \n')
		f.write('log_bin= 1 \n')
		f.write('dim1_min_logbin= 0.2 \n')
		f.write('dim1_max= 100. \n')
		f.write('dim1_nbin= 50 \n')
		f.write('dim2_max= 60. \n')
		f.write('dim2_nbin= 60 \n')
		f.write('dim3_min= 0.00 \n')
		f.write('dim3_max= 6. \n')
		f.write('dim3_nbin= 1 \n')
		f.close()
		# Angular clustering
		f = open(os.path.join(p_2_out, 'commands_Zmin_' + str(np.round(Z_lim, 1)) + '_Zmax_' + str(np.round(Z_up, 1)) + '.wtheta.ini'), 'w')
		f.write('data_filename= ' +	p_2_data +' \n')
		f.write('random_filename= ' + p_2_random +' \n')
		f.write('input_format= 2 \n')
		f.write('output_filename= ' + p_2_out+'/Zmin_' + str(np.round(Z_lim, 1)) + '_Zmax_' + str(np.round(Z_up, 1)) + '.wtheta.2pcf \n')
		f.write('corr_type= angular \n')
		f.write('omega_M= 0.307 \n')
		f.write('omega_L= 0.693 \n')
		f.write('w= -1 \n')
		f.write('log_bin= 1 \n')
		f.write('dim1_min_logbin= 0.005 \n')
		f.write('dim1_max= 10. \n')
		f.write('dim1_nbin= 40 \n')
		f.write('dim2_max= 160. \n')
		f.write('dim2_nbin= 40 \n')
		f.write('dim3_min= 0.00 \n')
		f.write('dim3_max= 3. \n')
		f.write('dim3_nbin= 1 \n')
		f.write('use_pm= 0 \n')
		f.write('n_pix_sph= 1000000 \n')
		f.close()

write_commands()

print('done!')