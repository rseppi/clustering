import os,glob,sys
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import ScalarFormatter


'''
Reads the txt files created after 000 with cosmology params and scaling of the cov matrix
Makes plots
'''

p_2_wp = os.path.join('/u/rseppi/clustering/figures/wp_rp','values_wp_lightcone.txt')

p_2_out = os.path.join('/u/rseppi/clustering/figures/wp_rp')

data_wp = np.loadtxt(p_2_wp,unpack=True)

OmegaM_planck = 0.307115
sigma8_planck = 0.8228
h_planck = 0.6766
N_erass8 = np.sqrt(0.98e6/1e5)
N_erass1 = np.sqrt(0.98e6/1e4)

N_wp, OmegaM_wp, sigma8_wp, h_wp, OmegaM_err_wp, sigma8_err_wp, h_err_wp = np.loadtxt(p_2_wp,unpack=True)
N_wp, index_wp = np.unique(N_wp,return_index=True)
OmegaM_wp, sigma8_wp, h_wp = OmegaM_wp[index_wp], sigma8_wp[index_wp], h_wp[index_wp]
OmegaM_err_wp, sigma8_err_wp, h_err_wp = OmegaM_err_wp[index_wp], sigma8_err_wp[index_wp], h_err_wp[index_wp]


outpl = os.path.join(p_2_out,'all_cosmo_trend.png')
fig = plt.figure(figsize=(6,6))
gs = fig.add_gridspec(3,1,hspace=0.0)
ax1 = fig.add_subplot(gs[0, :])
ax1.errorbar(N_wp,OmegaM_wp,yerr=OmegaM_err_wp, capsize=0, fmt='.',label=r'w$_{p}(r_p)$',color='C0')
ax1.hlines(OmegaM_planck,0,23, color='red')
ax2 = fig.add_subplot(gs[1, :])
ax2.errorbar(N_wp,sigma8_wp, yerr=sigma8_err_wp, capsize=0, fmt='.',label=r'w$_{p}(r_p)$',color='C0')
ax2.hlines(sigma8_planck,0,23, color='red')
ax3 = fig.add_subplot(gs[2, :])
ax3.errorbar(N_wp, h_wp, yerr=h_err_wp, capsize=0, fmt='.',label=r'w$_{p}(r_p)$',color='C0')
ax3.hlines(h_planck,0,23, color='red')

ymin, ymax = ax1.get_ylim()
ax1.vlines(N_erass8,ymin, ymax,label='eRASS8',color='black')
ax1.vlines(N_erass1,ymin, ymax,label='eRASS1',color='black',linestyle='dashed')
ymin, ymax = ax2.get_ylim()
ax2.vlines(N_erass8,ymin, ymax,color='black')
ax2.vlines(N_erass1,ymin, ymax,color='black',linestyle='dashed')
ymin, ymax = ax3.get_ylim()
ax3.vlines(N_erass8,ymin, ymax,color='black')
ax3.vlines(N_erass1,ymin, ymax,color='black',linestyle='dashed')


ax1.legend(fontsize=14,bbox_to_anchor=(0, 1.03, 1.0, .3), loc='lower left', ncol=3, mode="expand", borderaxespad=0.)
ax1.tick_params(labelsize=14)
ax2.tick_params(labelsize=14)
ax3.tick_params(labelsize=14)
ax1.set_xscale('log')
ax2.set_xscale('log')
ax3.set_xscale('log')
ax1.grid(True)
ax2.grid(True)
ax3.grid(True)
ax1.set_ylabel(r'$\Omega_M$', fontsize=14)
ax2.set_ylabel(r'$\sigma_8$', fontsize=14)
ax3.set_ylabel(r'$h$', fontsize=14)
ax3.set_xlabel('N', fontsize=14)

ax1.set_xticks([0.1,0.5,1.0,5.0,10.0,20.0])
ax2.set_xticks([0.1,0.5,1.0,5.0,10.0,20.0])
ax3.set_xticks([0.1,0.5,1.0,5.0,10.0,20.0])
ax3.set_xticklabels([0.1,0.5,1.0,5.0,10.0,20.0])

plt.setp(ax1.get_xticklabels(), visible=False)
plt.setp(ax2.get_xticklabels(), visible=False)

fig.tight_layout()
fig.savefig(outpl)


outpl = os.path.join(p_2_out,'all_cosmo_error_trend.png')
fig = plt.figure(figsize=(6,6))
gs = fig.add_gridspec(3,1,hspace=0.0)
ax1 = fig.add_subplot(gs[0, :])
ax1.plot(N_wp,OmegaM_err_wp,label=r'w$_{p}(r_p)$',color='C0')
ax2 = fig.add_subplot(gs[1, :])
ax2.plot(N_wp, sigma8_err_wp, label=r'w$_{p}(r_p)$',color='C0')
ax3 = fig.add_subplot(gs[2, :])
ax3.plot(N_wp, h_err_wp, label=r'w$_{p}(r_p)$',color='C0')

ymin, ymax = ax1.get_ylim()
ax1.vlines(N_erass8,ymin, ymax,label='eRASS8',color='black')
ax1.vlines(N_erass1,ymin,ymax,label='eRASS1',color='black',linestyle='dashed')
ymin, ymax = ax2.get_ylim()
ax2.vlines(N_erass8,ymin, ymax,color='black')
ax2.vlines(N_erass1,ymin,ymax,color='black',linestyle='dashed')
ymin, ymax = ax3.get_ylim()
ax3.vlines(N_erass8,ymin, ymax,color='black')
ax3.vlines(N_erass1,ymin,ymax,color='black',linestyle='dashed')


ax1.legend(fontsize=14,bbox_to_anchor=(0, 1.03, 1.0, .3), loc='lower left', ncol=3, mode="expand", borderaxespad=0.)
ax1.tick_params(labelsize=14)
ax2.tick_params(labelsize=14)
ax3.tick_params(labelsize=14)
ax1.grid(True)
ax2.grid(True)
ax3.grid(True)
#ax1.set_yscale('log')
ax1.set_xscale('log')
#ax2.set_yscale('log')
ax2.set_xscale('log')
#ax3.set_yscale('log')
ax3.set_xscale('log')
ax1.set_ylabel(r'$\delta \Omega_M$', fontsize=14)
ax2.set_ylabel(r'$\delta \sigma_8$', fontsize=14)
ax3.set_ylabel(r'$\delta h$', fontsize=14)
ax3.set_xlabel('N', fontsize=14)

ax1.set_xticks([0.1,0.5,1.0,5.0,10.0,20.0])
ax2.set_xticks([0.1,0.5,1.0,5.0,10.0,20.0])
ax3.set_xticks([0.1,0.5,1.0,5.0,10.0,20.0])
ax3.set_xticklabels([0.1,0.5,1.0,5.0,10.0,20.0])

plt.setp(ax1.get_xticklabels(), visible=False)
plt.setp(ax2.get_xticklabels(), visible=False)

fig.tight_layout()
fig.savefig(outpl)


outpl = os.path.join(p_2_out,'all_cosmo_error_percent_trend.png')
fig = plt.figure(figsize=(6,6))
gs = fig.add_gridspec(3,1,hspace=0.0)
ax1 = fig.add_subplot(gs[0, :])
ax1.plot(N_wp,OmegaM_err_wp/OmegaM_wp,label=r'w$_{p}(r_p)$',color='C0')
ax2 = fig.add_subplot(gs[1, :])
ax2.plot(N_wp, sigma8_err_wp/sigma8_wp, label=r'w$_{p}(r_p)$',color='C0')
ax3 = fig.add_subplot(gs[2, :])
ax3.plot(N_wp, h_err_wp/h_wp, label=r'w$_{p}(r_p)$',color='C0')

ymin, ymax = ax1.get_ylim()
ax1.vlines(N_erass8,ymin, ymax,label='eRASS8',color='black')
ax1.vlines(N_erass1,ymin,ymax,label='eRASS1',color='black',linestyle='dashed')
ymin, ymax = ax2.get_ylim()
ax2.vlines(N_erass8,ymin, ymax,color='black')
ax2.vlines(N_erass1,ymin,ymax,color='black',linestyle='dashed')
ymin, ymax = ax3.get_ylim()
ax3.vlines(N_erass8,ymin, ymax,color='black')
ax3.vlines(N_erass1,ymin,ymax,color='black',linestyle='dashed')


ax1.legend(fontsize=14,bbox_to_anchor=(0, 1.03, 1.0, .3), loc='lower left', ncol=3, mode="expand", borderaxespad=0.)
ax1.tick_params(labelsize=14)
ax2.tick_params(labelsize=14)
ax3.tick_params(labelsize=14)
ax1.grid(True)
ax2.grid(True)
ax3.grid(True)
#ax1.set_yscale('log')
ax1.set_xscale('log')
#ax2.set_yscale('log')
ax2.set_xscale('log')
#ax3.set_yscale('log')
ax3.set_xscale('log')
ax1.set_ylabel(r'$\delta \Omega_M / \Omega_M$', fontsize=14)
ax2.set_ylabel(r'$\delta \sigma_8 / \sigma_8$', fontsize=14)
ax3.set_ylabel(r'$\delta h /h$', fontsize=14)
ax3.set_xlabel('N', fontsize=14)

ax1.set_xticks([0.1,0.5,1.0,5.0,10.0,20.0])
ax2.set_xticks([0.1,0.5,1.0,5.0,10.0,20.0])
ax3.set_xticks([0.1,0.5,1.0,5.0,10.0,20.0])

ax1.set_yticks([0.05,0.10,0.15,0.20])
ax1.set_yticklabels(['0.05','0.10','0.15','0.20'])

ax3.set_yticks([0.05,0.10,0.15,0.20])
ax3.set_yticklabels(['0.05','0.10','0.15','0.20'])

ax3.set_xticklabels([0.1,0.5,1.0,5.0,10.0,20.0])

plt.setp(ax1.get_xticklabels(), visible=False)
plt.setp(ax2.get_xticklabels(), visible=False)

fig.tight_layout()
fig.savefig(outpl)






Nclu_erass8 = 1e5
Nclu_erass1 = 1e4
Nclu_wp = 0.98e6/N_wp**2
outpl = os.path.join(p_2_out,'all_cosmo_error_percent_trend_Nclu.png')
fig = plt.figure(figsize=(6,6))
gs = fig.add_gridspec(3,1,hspace=0.0)
ax1 = fig.add_subplot(gs[0, :])
ax1.plot(Nclu_wp,OmegaM_err_wp/OmegaM_wp,label=r'w$_{p}(r_p)$',color='C0')
ax2 = fig.add_subplot(gs[1, :])
ax2.plot(Nclu_wp, sigma8_err_wp/sigma8_wp, label=r'w$_{p}(r_p)$',color='C0')
ax3 = fig.add_subplot(gs[2, :])
ax3.plot(Nclu_wp, h_err_wp/h_wp, label=r'w$_{p}(r_p)$',color='C0')

ymin, ymax = ax1.get_ylim()
ax1.vlines(Nclu_erass8,ymin, ymax,label='eRASS8',color='black')
ax1.vlines(Nclu_erass1,ymin,ymax,label='eRASS1',color='black',linestyle='dashed')
ymin, ymax = ax2.get_ylim()
ax2.vlines(Nclu_erass8,ymin, ymax,color='black')
ax2.vlines(Nclu_erass1,ymin,ymax,color='black',linestyle='dashed')
ymin, ymax = ax3.get_ylim()
ax3.vlines(Nclu_erass8,ymin, ymax,color='black')
ax3.vlines(Nclu_erass1,ymin,ymax,color='black',linestyle='dashed')


ax1.legend(fontsize=14,bbox_to_anchor=(0, 1.03, 1.0, .3), loc='lower left', ncol=3, mode="expand", borderaxespad=0.)
ax1.tick_params(labelsize=14)
ax2.tick_params(labelsize=14)
ax3.tick_params(labelsize=14)
ax1.grid(True)
ax2.grid(True)
ax3.grid(True)
#ax1.set_yscale('log')
ax1.set_xscale('log')
#ax2.set_yscale('log')
ax2.set_xscale('log')
#ax3.set_yscale('log')
ax3.set_xscale('log')
ax1.set_ylabel(r'$\delta \Omega_M / \Omega_M$', fontsize=14)
ax2.set_ylabel(r'$\delta \sigma_8 / \sigma_8$', fontsize=14)
ax3.set_ylabel(r'$\delta h /h$', fontsize=14)
ax3.set_xlabel('Number of clusters', fontsize=14)

xticks_latex = [r'$2\times 10^5$',r'$1\times 10^5$',r'$3\times 10^4$',r'$1\times 10^4$',r'$3\times 10^3$']
xticks = ['2e5','1e5','3e4','1e4','3e3']
ax1.set_xticks(np.array(xticks,dtype=float))
ax2.set_xticks(np.array(xticks,dtype=float))
ax3.set_xticks(np.array(xticks,dtype=float))

ax1.set_yticks([0.05,0.10,0.15,0.20])
ax1.set_yticklabels(['0.05','0.10','0.15','0.20'])

ax3.set_yticks([0.05,0.10,0.15,0.20])
ax3.set_yticklabels(['0.05','0.10','0.15','0.20'])

ax3.set_xticklabels(xticks_latex)
ax1.set_xlim(3e3,2.5e5)
ax2.set_xlim(3e3,2.5e5)
ax3.set_xlim(3e3,2.5e5)

ax1.set_ylim(0.075)
ax2.set_ylim(0.02)
ax3.set_ylim(0.12)

#ax3.xaxis.set_major_formatter(ScalarFormatter())
#ax3.ticklabel_format(axis='x',useOffset=True)

plt.setp(ax1.get_xticklabels(), visible=False)
plt.setp(ax2.get_xticklabels(), visible=False)

fig.tight_layout()
fig.savefig(outpl)


sys.exit()



























outpl = os.path.join(p_2_out,'wp_cosmo_trend.png')
fig = plt.figure(figsize=(6,6))
gs = fig.add_gridspec(3,1,hspace=0.0)
ax1 = fig.add_subplot(gs[0, :])
ax1.errorbar(N,OmegaM,yerr=OmegaM_err, capsize=0, fmt='.')
ax1.hlines(OmegaM_planck,0,10, color='red')
ax2 = fig.add_subplot(gs[1, :])
ax2.errorbar(N,sigma8, yerr=sigma8_err, capsize=0, fmt='.')
ax2.hlines(sigma8_planck,0,10, color='red')
ax3 = fig.add_subplot(gs[2, :])
ax3.errorbar(N, h, yerr=h_err, capsize=0, fmt='.')
ax3.hlines(h_planck,0,10, color='red')


ax1.tick_params(labelsize=14)
ax2.tick_params(labelsize=14)
ax3.tick_params(labelsize=14)
ax1.grid(True)
ax2.grid(True)
ax3.grid(True)
ax1.set_ylabel(r'$\Omega_M$', fontsize=14)
ax2.set_ylabel(r'$\sigma_8$', fontsize=14)
ax3.set_ylabel(r'$h$', fontsize=14)
ax3.set_xlabel('N', fontsize=14)

ax1.set_title(r'$wp(r_p)$', fontsize=14)
fig.tight_layout()
fig.savefig(outpl)

outpl = os.path.join(p_2_out,'wp_cosmo_error_trend.png')
fig = plt.figure(figsize=(6,6))
gs = fig.add_gridspec(3,1,hspace=0.0)
ax1 = fig.add_subplot(gs[0, :])
ax1.plot(N,OmegaM_err)
ax2 = fig.add_subplot(gs[1, :])
ax2.plot(N,sigma8_err)
ax3 = fig.add_subplot(gs[2, :])
ax3.plot(N,h_err)

ax1.tick_params(labelsize=14)
ax2.tick_params(labelsize=14)
ax3.tick_params(labelsize=14)
ax1.grid(True)
ax2.grid(True)
ax3.grid(True)
ax1.set_ylabel(r'$\delta\ \Omega_M$', fontsize=14)
ax2.set_ylabel(r'$\delta\ \sigma_8$', fontsize=14)
ax3.set_ylabel(r'$\delta$ h', fontsize=14)
ax3.set_xlabel('N', fontsize=14)

ax1.set_title(r'$wp(r_p)$', fontsize=14)
fig.tight_layout()
fig.savefig(outpl)




outpl = os.path.join(p_2_out,'monopole_cosmo_trend.png')
fig = plt.figure(figsize=(6,6))
gs = fig.add_gridspec(3,1,hspace=0.0)
ax1 = fig.add_subplot(gs[0, :])
ax1.errorbar(N,OmegaM,yerr=OmegaM_err, capsize=0, fmt='.')
ax1.hlines(OmegaM_planck,0,10, color='red')
ax2 = fig.add_subplot(gs[1, :])
ax2.errorbar(N,sigma8, yerr=sigma8_err, capsize=0, fmt='.')
ax2.hlines(sigma8_planck,0,10, color='red')
ax3 = fig.add_subplot(gs[2, :])
ax3.errorbar(N, h, yerr=h_err, capsize=0, fmt='.')
ax3.hlines(h_planck,0,10, color='red')

ax1.tick_params(labelsize=14)
ax2.tick_params(labelsize=14)
ax3.tick_params(labelsize=14)
ax1.grid(True)
ax2.grid(True)
ax3.grid(True)
ax1.set_ylabel(r'$\Omega_M$', fontsize=14)
ax2.set_ylabel(r'$\sigma_8$', fontsize=14)
ax3.set_ylabel(r'$h$', fontsize=14)
ax3.set_xlabel('N', fontsize=14)

ax1.set_title(r'$\xi(r)$', fontsize=14)
fig.tight_layout()
fig.savefig(outpl)

outpl = os.path.join(p_2_out,'monopole_cosmo_error_trend.png')
fig = plt.figure(figsize=(6,6))
gs = fig.add_gridspec(3,1,hspace=0.0)
ax1 = fig.add_subplot(gs[0, :])
ax1.scatter(N,OmegaM_err)
ax2 = fig.add_subplot(gs[1, :])
ax2.scatter(N,sigma8_err)
ax3 = fig.add_subplot(gs[2, :])
ax3.scatter(N,h_err)

ax1.tick_params(labelsize=14)
ax2.tick_params(labelsize=14)
ax3.tick_params(labelsize=14)
ax1.grid(True)
ax2.grid(True)
ax3.grid(True)
ax1.set_ylabel(r'$\delta\ \Omega_M$', fontsize=14)
ax2.set_ylabel(r'$\delta\ \sigma_8$', fontsize=14)
ax3.set_ylabel(r'$\delta$ h', fontsize=14)
ax3.set_xlabel('N', fontsize=14)

ax1.set_title(r'$\xi(r)$', fontsize=14)
fig.tight_layout()
fig.savefig(outpl)




outpl = os.path.join(p_2_out,'angular_cosmo_trend.png')
fig = plt.figure(figsize=(6,6))
gs = fig.add_gridspec(3,1,hspace=0.0)
ax1 = fig.add_subplot(gs[0, :])
ax1.errorbar(N,OmegaM,yerr=OmegaM_err, capsize=0, fmt='.')
ax1.hlines(OmegaM_planck,0,10, color='red')
ax2 = fig.add_subplot(gs[1, :])
ax2.errorbar(N,sigma8, yerr=sigma8_err, capsize=0, fmt='.')
ax2.hlines(sigma8_planck,0,10, color='red')
ax3 = fig.add_subplot(gs[2, :])
ax3.errorbar(N, h, yerr=h_err, capsize=0, fmt='.')
ax3.hlines(h_planck,0,10, color='red')

ax1.tick_params(labelsize=14)
ax2.tick_params(labelsize=14)
ax3.tick_params(labelsize=14)
ax1.grid(True)
ax2.grid(True)
ax3.grid(True)
ax1.set_ylabel(r'$\Omega_M$', fontsize=14)
ax2.set_ylabel(r'$\sigma_8$', fontsize=14)
ax3.set_ylabel(r'$h$', fontsize=14)
ax3.set_xlabel('N', fontsize=14)

ax1.set_title(r'$w(\theta)$', fontsize=14)
fig.tight_layout()
fig.savefig(outpl)

outpl = os.path.join(p_2_out,'angular_cosmo_error_trend.png')
fig = plt.figure(figsize=(6,6))
gs = fig.add_gridspec(3,1,hspace=0.0)
ax1 = fig.add_subplot(gs[0, :])
ax1.scatter(N,OmegaM_err)
ax2 = fig.add_subplot(gs[1, :])
ax2.scatter(N,sigma8_err)
ax3 = fig.add_subplot(gs[2, :])
ax3.scatter(N,h_err)

ax1.tick_params(labelsize=14)
ax2.tick_params(labelsize=14)
ax3.tick_params(labelsize=14)
ax1.grid(True)
ax2.grid(True)
ax3.grid(True)
ax1.set_ylabel(r'$\delta\ \Omega_M$', fontsize=14)
ax2.set_ylabel(r'$\delta\ \sigma_8$', fontsize=14)
ax3.set_ylabel(r'$\delta$ h', fontsize=14)
ax3.set_xlabel('N', fontsize=14)

ax1.set_title(r'$w(\theta)$', fontsize=14)
fig.tight_layout()
fig.savefig(outpl)

