import os,glob
'''
Runs CUTE to tabulate correlation functions and pairs
uses catalogs and input commands already prepared in previous steps 
000 001

R.Seppi (MPE)
26.02.2021
'''

#p_2_commands = os.path.join('/u/rseppi/clustering','correlation_functions')
#CUTE_cmd = '/u/rseppi/.local/CUTE/CUTE/CUTE '

p_2_commands = os.path.join('/home/rseppi/clustering','correlation_functions')
CUTE_cmd = '/home/rseppi/.local/CUTE/CUTE/CUTE '

commands = glob.glob(os.path.join(p_2_commands,'*ini'))
commands.sort()

for cmd_file in commands:
    print(cmd_file)
    if os.path.isfile(os.path.join(cmd_file[:-3] + '2pcf')):
        print('done')
    else:
        cmd = CUTE_cmd + cmd_file
        print(cmd)
        os.system(cmd)