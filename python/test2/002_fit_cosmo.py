import sys,os,glob
import numpy as np
import pyccl as ccl
from astropy.table import Table
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import corner
from colossus.cosmology import cosmology
from colossus.lss import bias
from colossus.lss import peaks
import ultranest
from ultranest.plot import runplot
from ultranest.plot import PredictionBand

'''
see if you can recover Planck cosmology from a single z snapshot
'''

#Use MultiDark cosmology
Om_MD = 0.307115 #matter
Ob_MD = 0.048206 #baryon
Oc_MD = Om_MD-Ob_MD #cold dark matter
print('Setting up cosmology...')
cosmo_planck18 = ccl.Cosmology(
    Omega_c = 0.11933/0.6766**2, # 0.26066
    Omega_b = 0.02242/0.6766**2, # 0.04897
    h = 0.6766,
    n_s = 0.9665,
    sigma8 = 0.8228,
    #A_s = np.e**(3.047)/10**10, # 2.1052e-09
    Omega_k = 0.0,
    Omega_g = None,
    Neff = 3.04, # standard model of particle physics
    m_nu = 0.0, # in eV
    m_nu_type = None, # 'inverted', 'normal', 'equal' or 'list'
    w0 = -1.0,
    wa = 0.0,
    T_CMB = ccl.physical_constants.T_CMB, # 2.725
    bcm_log10Mc=14.079181246047625, # BCM baryon correction model on PK https://arxiv.org/abs/1510.06034
    bcm_etab=0.5,                   # BCM
    bcm_ks=55.0,                    # BCM
    mu_0=0.0,                       # modified gravity model parameter
    sigma_0=0.0,                    # modified gravity model parameter
    z_mg=None,                      # modified growth rate parameter
    df_mg=None,                     # modified growth rate parameter
    transfer_function='boltzmann_camb',  # for power spectrum analysis
    matter_power_spectrum='halofit',     # for power spectrum analysis
    baryons_power_spectrum='nobaryons',  # for power spectrum analysis
    mass_function='tinker10',            # for mass function analysis
    halo_concentration='duffy2008',      # for mass function analysis
    emulator_neutrinos='strict',         # for power spectrum analysis
)
cosmo = cosmo_planck18
cosmo_colossus = cosmology.setCosmology('multidark-planck')


p_2_cf = sorted(glob.glob(os.path.join('/u/rseppi/clustering','python','test2','MD_BOX_z*.2pcf')))
outdir = os.path.join('/u/rseppi/clustering','python','test2')
logdir = os.path.join('/u/rseppi/clustering','python','test2','my_fit_3d')


# Turn off multi-threading to avoid issues with MPIPool.
os.environ["OMP_NUM_THREADS"] = "1"

#Use MultiDark cosmology
Om_MD = 0.307115 #matter
Ob_MD = 0.048206 #baryon
Oc_MD = Om_MD-Ob_MD #cold dark matter
h_MD = 0.6766

def model(x, z, mass, pars):
    '''
    :param x: array. Separation in Mpc/h
    :param pars: cosmological parameters. Available: OmegaM, sigma8, h, b.
    :return: 3D correlation function xi(r). Same dimension of x
    '''
    #Om,s8,h,b = pars
    Om,s8, h = pars
    xh = x/h
    Oc = Om - Ob_MD
    #nu = peaks.peakHeight(np.average(t['HALO_Mvir']), np.average(t['redshift_S']))
    #b = bias.haloBiasFromNu(nu, model='comparat17')
    cosmol = ccl.Cosmology(
        Omega_c = Oc, # 0.26066
        Omega_b = 0.02242/0.6766**2, # 0.04897
        h = h,
        n_s = 0.9665,
        sigma8 = s8,
        Omega_k = 0.0,
        Omega_g = None,
        Neff = 3.04, # standard model of particle physics
        m_nu = 0.0, # in eV
        m_nu_type = None, # 'inverted', 'normal', 'equal' or 'list'
        w0 = -1.0,
        wa = 0.0,
        T_CMB = ccl.physical_constants.T_CMB, # 2.725
        bcm_log10Mc=14.079181246047625, # BCM baryon correction model on PK https://arxiv.org/abs/1510.06034
        bcm_etab=0.5,                   # BCM
        bcm_ks=55.0,                    # BCM
        mu_0=0.0,                       # modified gravity model parameter
        sigma_0=0.0,                    # modified gravity model parameter
        z_mg=None,                      # modified growth rate parameter
        df_mg=None,                     # modified growth rate parameter
        transfer_function='boltzmann_camb',  # for power spectrum analysis
        matter_power_spectrum='halofit',     # for power spectrum analysis
        baryons_power_spectrum='nobaryons',  # for power spectrum analysis
        mass_function='tinker10',            # for mass function analysis
        halo_concentration='duffy2008',      # for mass function analysis
        emulator_neutrinos='strict',         # for power spectrum analysis
    )

    cosmopars = {'flat': True, 'H0': h * 100, 'Om0': Om, 'Ob0': 0.02242 / 0.6766 ** 2, 'sigma8': s8, 'ns': 0.9665}
    cosmol_colossus = cosmology.setCosmology('newcosmo', cosmopars)

    #b = ccl.massfunction.halo_bias(cosmol, float(mass / h), a=1 / (1 + z), overdensity=500)
    f=ccl.background.growth_rate(cosmol, a=1 / (1 + z))
    b = bias.haloBias(mass, model='comparat17', z=z, mdef='500c')
    beta = f/b
    xi_model = ccl.correlation_3d(cosmol, a=1 / (1 + z), r=xh)
    xi_model = xi_model * b**2 * (1 + 2./3.*beta + 1./5.*beta**2)
    r2_xi_model = x**2 * xi_model
    return r2_xi_model

#halo_mass = 1.6138e14 #table distinct_0.6712 (MD40_ero)--> M500c > 1e14 --> get mean --> 1.59 (1.6138)
#z = 1/0.6712 - 1
halo_mass = np.array([1.0417e14, 9.04949e13]) #table distinct_0.6712 (MD40_ero)--> M500c > 1e14 --> get mean --> 1.59 (1.6138)
a = np.array([1,0.6712])
z = 1/a - 1
#f = ccl.background.growth_rate(cosmo, a)
#b = bias.haloBias(halo_mass, model='comparat17', z=z, mdef='500c')
#beta = f/b
#print(f,b,beta)

cf_list, icov_list, err_list = [],[],[]

outpl = os.path.join(outdir,'corr_func_3d_check.png')
plt.figure(figsize=(6,6))

for cf,mass,reds in zip(p_2_cf,halo_mass,z):
    print(cf,mass,reds)
    r,xi,err,DD = np.loadtxt(cf, unpack=True)
    #xi = xi * ( 1 + 2./3.*bet + 1./5.*bet**2)
    cf_list.append(xi)
    err = 0.2*(r**2*xi)
    err_list.append(err)
    cov = np.diag(err ** 2)
    icov = np.linalg.inv(cov)
    icov_list.append(icov)
    plt.scatter(r,r**2*xi,label='MD_BOX z={:.2f}'.format(reds))
    plt.plot(r,model(r,reds,mass,[0.309,0.8228,0.67]),label='pyccl Planck')

plt.xticks(fontsize=13)
plt.yticks(fontsize=13)
plt.legend(fontsize=13)
plt.xlabel(r'$r$ [Mpc/h]', fontsize=13)
plt.ylabel(r'$r^2\xi(r)$ [Mpc$^2$/h$^2$]', fontsize=13)
plt.grid(True)
plt.tight_layout()
plt.savefig(outpl)


def lnprior(cube):
    pars = cube.copy()
    pars[0] = (cube[0])*0.45 + 0.15  #OmegaM = 0.15 0.4
    pars[1] = (cube[1])*0.6 + 0.5  #sigma8 = 0.5 1.5
    pars[2] = (cube[2])*0.6 + 0.4  #h = 0.4 1.4
    #pars[3] = (cube[3])*5.5 + 0.5  #h = 0.5 5.5
    return pars

def lnlike(pars):
    likelihood = 0
    for xi,reds,mass,icov in zip(cf_list,z,halo_mass,icov_list):
        try:
            diff = r**2*xi - model(r, reds, mass, pars)
            likelihood += -np.dot(diff,np.dot(icov,diff))/2.0
        except:
            likelihood = -1e20
        return likelihood


names = [r'$\Omega_{M}$', r'$\sigma_8$', 'h']#, 'b']
print('initialize sampler...')
sampler = ultranest.ReactiveNestedSampler(param_names=names, loglike=lnlike, transform=lnprior, log_dir=logdir, resume=True)
print('run sampler...')
result = sampler.run(min_num_live_points=400)

print('print results...')
sampler.print_results()
print('plot sampler...')
sampler.plot()

outpl = os.path.join(outdir, 'corr_func_3d_final.png')
colors = ['C0', 'C1', 'C2', 'C3']
plt.figure(figsize=(5., 5.))
for xi, reds, mass, err in zip(cf_list,z,halo_mass,err_list):
    plt.errorbar(r, (r**2 * xi), yerr=err, fmt=".", capsize=0, label='z={:.1f}'.format(reds))

    r_grid = np.linspace(1, 150, 200)
    band = PredictionBand(r_grid)
    for Om_, s8_, h_ in sampler.results['samples']:
    # compute for each time the y value
        band.add(model(r_grid, reds, mass, [Om_, s8_, h_]))#, b_]))

    band.line(color='C0')
    # add 1 sigma quantile
    band.shade(alpha=0.4, color='C0')#label=r'1$\sigma$')
    # add 2 sigma quantile
    band.shade(q=0.477, alpha=0.2, color='C0')#label=r'2$\sigma$')
plt.xticks(fontsize=13)
plt.yticks(fontsize=13)
#plt.yscale('log')
#plt.xscale('log')
plt.legend(fontsize=13)
plt.xlabel(r'$r$ [Mpc/h]', fontsize=13)
plt.ylabel(r'$r^2\xi(r)  [Mpc$^2$/h$^2$]$', fontsize=13)
plt.grid(True)
plt.tight_layout()
plt.savefig(outpl, overwrite=True)