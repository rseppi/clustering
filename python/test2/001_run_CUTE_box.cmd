#!/bin/bash -l
#SBATCH -J T_CUTE_box
#SBATCH -o /u/rseppi/clustering/logs/test2_cute.out.%j
#SBATCH -e /u/rseppi/clustering/logs/test2_cute.err.%j
#SBATCH -D ./
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1
#SBATCH --cpus-per-task=96
#SBATCH --time=1:00:00

module load gcc/10 impi/2019.9 gsl/2.4 mpip/3.4.1
export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK
LD_LIBRARY_PATH=${GSL_HOME}/lib
export LD_LIBRARY_PATH
# For pinning threads correctly:
#export OMP_PLACES=cores

# Run the program:
echo "SLURM_JOB_NUM_NODES ${SLURM_JOB_NUM_NODES}"
echo "SLURM_NTASKS_PER_NODE ${SLURM_NTASKS_PER_NODE}"
echo "SLURM_CPUS_PER_TASK ${SLURM_CPUS_PER_TASK}"
echo "SLURM_MEM_PER_NODE ${SLURM_MEM_PER_NODE}"
n="$((${SLURM_JOB_NUM_NODES} * ${SLURM_NTASKS_PER_NODE} * ${SLURM_CPUS_PER_TASK}))"
echo "TOTAL CPUS ${n}"

srun /u/rseppi/.local/CUTE/CUTE_box/CUTE_box commands.box.ini

# you can also run it with srun -n 8 -p interactive /u/rseppi/.local/CUTE/CUTE_box/CUTE_box commands.box.ini (it's fast)