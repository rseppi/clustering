import os, sys, glob
import numpy as np
from astropy.table import Table
import random

'''
Create catalogs used by CUTE
Compute clustering in a 3d box at fixed redshift
'''
p_2_cat = glob.glob(os.path.join('/ptmp/rseppi/catalogs','distinct_*.fits.gz'))
p_2_out = os.path.join('/u/rseppi/clustering','python','test2')

for cat in p_2_cat:
    a = float(os.path.basename(cat[:-8]).split('_')[1])
    redshift = 1/a - 1
    print('z = ',redshift)
    p_2_cat_out = os.path.join(p_2_out,'MD_CUBE_DATA_z_{:.2f}.ascii'.format(redshift))
    p_2_rand_out = os.path.join(p_2_out,'MD_CUBE_RANDOM_z_{:.2f}.ascii'.format(redshift))

    print('Reading HMD...')
    t_ = Table.read(cat)

    print('cut HMD...')
    mass_cut = t_['M500c']>5e13
    t = t_[mass_cut]

    x,y,z = t['x'], t['y'], t['z']

    print('Saving HMD...')
    side = 4000 #Mpc/h
    DATA=np.transpose([x,y,z])
    np.savetxt(p_2_cat_out, DATA, fmt="%s")

    #x_rand = random.sample(range(0,side),N*len(x))
    #y_rand = random.sample(range(0,side),N*len(x))
    #z_rand = random.sample(range(0,side),N*len(x))
    #RANDOM = np.transpose([x_rand,y_rand,z_rand])

    print('writing CUTE_box input...')

    f = open(os.path.join(p_2_out,'commands_z_{:.2f}.box.ini'.format(redshift)), 'w')
    f.write('data_filename= ' +	p_2_cat_out +' \n')
    f.write('num_lines = all \n')
    f.write('input_format= 0 \n')
    f.write('output_filename= ' + p_2_out+'/MD_BOX_z_{:.2f}.2pcf \n'.format(redshift))
    f.write('box_size= '+str(side)+'. \n')
    f.write('use_pm= 0 \n')
    f.write('use_tree= 0 \n')
    f.close()


print('done!')
