import sys,os,glob
import numpy as np
import pyccl as ccl
from astropy.table import Table
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import corner
from colossus.cosmology import cosmology
from colossus.lss import bias
from colossus.lss import peaks
import ultranest
from ultranest.plot import PredictionBand
from scipy import integrate

'''
Test code to compute clustering and fits cosmology
Uses eroconda (rseppipy) and pyccl

Needs AXV instructions

R.Seppi (MPE)
04.02.2021

Fitting rp>1Mpc/h and rp<70Mpc/h
'''

N = sys.argv[1]

#p_2_cat = os.path.join('/home/rseppi/Downloads','MD40_eRO_CLU_b8_CM_0_pixS_20.0.fits')
#p_2_data = sorted(glob.glob(os.path.join('/home/rseppi/clustering','correlation_functions','*3dps.2pcf')))
#outdir = os.path.join('/home/rseppi/clustering','figures')
#logdir = os.path.join('/home/rseppi/clustering','my_fit_wp')

p_2_cat = os.path.join('/ptmp/rseppi/catalogs','MD40_eRO_CLU_b8_CM_0_pixS_20.0.fits')
p_2_data = sorted(glob.glob(os.path.join('/u/rseppi/clustering','correlation_functions','*3dps.2pcf')))
outdir = os.path.join('/u/rseppi/clustering','figures','wp_rp')
logdir = os.path.join('/u/rseppi/clustering','my_fit_wp_N'+N)


# Turn off multi-threading to avoid issues with MPIPool.
os.environ["OMP_NUM_THREADS"] = "1"

#Use MultiDark cosmology
Om_MD = 0.307115 #matter
Ob_MD = 0.048206 #baryon
Oc_MD = Om_MD-Ob_MD #cold dark matter
print('Setting up cosmology...')
cosmo_planck18 = ccl.Cosmology(
    Omega_c = 0.11933/0.6766**2, # 0.26066
    Omega_b = 0.02242/0.6766**2, # 0.04897
    h = 0.6766,
    n_s = 0.9665,
    sigma8 = 0.8228,
    #A_s = np.e**(3.047)/10**10, # 2.1052e-09
    Omega_k = 0.0,
    Omega_g = None,
    Neff = 3.04, # standard model of particle physics
    m_nu = 0.0, # in eV
    m_nu_type = None, # 'inverted', 'normal', 'equal' or 'list'
    w0 = -1.0,
    wa = 0.0,
    T_CMB = ccl.physical_constants.T_CMB, # 2.725
    bcm_log10Mc=14.079181246047625, # BCM baryon correction model on PK https://arxiv.org/abs/1510.06034
    bcm_etab=0.5,                   # BCM
    bcm_ks=55.0,                    # BCM
    mu_0=0.0,                       # modified gravity model parameter
    sigma_0=0.0,                    # modified gravity model parameter
    z_mg=None,                      # modified growth rate parameter
    df_mg=None,                     # modified growth rate parameter
    transfer_function='boltzmann_camb',  # for power spectrum analysis
    matter_power_spectrum='halofit',     # for power spectrum analysis
    baryons_power_spectrum='nobaryons',  # for power spectrum analysis
    mass_function='tinker10',            # for mass function analysis
    halo_concentration='duffy2008',      # for mass function analysis
    emulator_neutrinos='strict',         # for power spectrum analysis
)
cosmo = cosmo_planck18
#cosmo = ccl.Cosmology(Omega_c = Oc_MD, Omega_b = Ob_MD, h = 0.6777, sigma8 = 0.8228, n_s = 0.96)
cosmo_colossus = cosmology.setCosmology('multidark-planck')

#get distribution with redshift
print('Reading table...')
t0=Table.read(p_2_cat)
mass_cut = t0['HALO_M500c']>5e13
t_=t0[(mass_cut)]
print('Now work on the model')
def model(x_rp, x_pi, z_low, z_up, pars):
    '''
    :param x_rp: array. Separation perpendicular to the line of sight in Mpc/h
    :param pi: array. Separation along the line of sight in Mpc/h
    :param pars: cosmological parameters. Available: OmegaM, sigma8, h, b.
    :return: projected correlation function wp(rp). Same dimension of x
    '''
    #Om,s8,h,b = pars
    Om,s8, h = pars
    Oc = Om - Ob_MD
    cosmol = ccl.Cosmology(
        Omega_c = Oc, # 0.26066
        Omega_b = 0.02242/0.6766**2, # 0.04897
        h = h,
        n_s = 0.9665,
        sigma8 = s8,
        Omega_k = 0.0,
        Omega_g = None,
        Neff = 3.04, # standard model of particle physics
        m_nu = 0.0, # in eV
        m_nu_type = None, # 'inverted', 'normal', 'equal' or 'list'
        w0 = -1.0,
        wa = 0.0,
        T_CMB = ccl.physical_constants.T_CMB, # 2.725
        bcm_log10Mc=14.079181246047625, # BCM baryon correction model on PK https://arxiv.org/abs/1510.06034
        bcm_etab=0.5,                   # BCM
        bcm_ks=55.0,                    # BCM
        mu_0=0.0,                       # modified gravity model parameter
        sigma_0=0.0,                    # modified gravity model parameter
        z_mg=None,                      # modified growth rate parameter
        df_mg=None,                     # modified growth rate parameter
        transfer_function='boltzmann_camb',  # for power spectrum analysis
        matter_power_spectrum='halofit',     # for power spectrum analysis
        baryons_power_spectrum='nobaryons',  # for power spectrum analysis
        mass_function='tinker10',            # for mass function analysis
        halo_concentration='duffy2008',      # for mass function analysis
        emulator_neutrinos='strict',         # for power spectrum analysis
    )

    cosmopars = {'flat': True, 'H0': h * 100, 'Om0': Om, 'Ob0': 0.02242 / 0.6766 ** 2, 'sigma8': s8, 'ns': 0.9665}
    cosmol_colossus = cosmology.setCosmology('newcosmo', cosmopars)

    z_cut = (t_['redshift_S'] > z_low) & (t_['redshift_S'] < z_up)
    t = t_[z_cut]
    halo_mass = np.average(t['HALO_M500c'])
    z = np.average(t['redshift_S'])
    a = 1 / (1 + z)
    f = ccl.background.growth_rate(cosmol, a)
    #b = ccl.massfunction.halo_bias(cosmol, float(halo_mass / h), a, overdensity=500)
    b = bias.haloBias(halo_mass, model='comparat17', z=z, mdef='500c')
    beta = f / b
    xi_model = np.empty((len(x_pi), len(x_rp)))
    for jj, pi_ in enumerate(x_pi):
        xi_model[jj] = ccl.correlation_pi_sigma(cosmol, a=a, beta=beta, pi=pi_/h,sig=x_rp/h)
    #in ccl.correlation_pi_sigma beta is used to model redshift space distrotion
    #We still need to rescale the correlation function from large scale to halos.
    xi_model = xi_model * b**2
    wp_model = np.empty(len(x_rp))
    for jj in range(len(wp_model)):
        wp_model[jj] = 2.0*integrate.simps(xi_model[:, jj], x_pi)
    rp_wp_model = x_rp * wp_model
    return rp_wp_model


outpl1 = os.path.join(outdir,'corr_func_wp.png')
fig1 = plt.figure(figsize=(6,6))
gs1 = fig1.add_gridspec(4,1,hspace=0.0)
ax11 = fig1.add_subplot(gs1[0:3, :])
ax12 = fig1.add_subplot(gs1[3, :], sharex=ax11)

outpl2 = os.path.join(outdir,'corr_func_wp_loglog.png')
fig2 = plt.figure(figsize=(6,6))
gs2 = fig2.add_gridspec(4,1,hspace=0.0)
ax21 = fig2.add_subplot(gs2[0:3, :])
ax22 = fig2.add_subplot(gs2[3, :], sharex=ax21)

pi_list, rp_list, wp_list, DD_list, DR_list, RR_list = [], [], [], [], [], []
cov_list = []
Z_mins = np.array([0.1,0.3,0.5,0.7])
Z_maxs = np.array([0.3,0.5,0.7,0.9])
colors = ['C0', 'C1', 'C2', 'C3']

for data, zmin, zmax, col in zip(p_2_data[:-1], Z_mins[:-1], Z_maxs[:-1], colors):
    pi, rp, xi, DD_cts, DR_cts, RR_cts = np.loadtxt(data, unpack=True, usecols=[0,1,2,3,4,6])#,dtype='float32')

    pi = np.unique(pi)
    rp = np.unique(rp)
    xi = xi.reshape(len(rp), len(pi))
    wp = np.empty(len(rp))
    for jj in range(len(wp)):
        wp[jj] = 2.0 * integrate.simps(xi[jj, :], pi)

    pi_list.append(pi)
    rp_list.append(rp)
    wp_list.append(wp)
    DD_list.append(DD_cts)
    DR_list.append(DR_cts)
    RR_list.append(RR_list)

    #p_2_cov_mat = os.path.join('/u/rseppi/clustering', 'correlation_functions', 'covariance_matrix',
    #                           'covariance_matrix_rp_wprp_Zmin_' + str(np.round(zmin, 1)) + '_Zmax_' + str(
    #                               np.round(zmax, 1)) + '.txt')

    p_2_cov_mat = os.path.join('/u/rseppi/clustering', 'correlation_functions', 'covariance_matrix',
                               'covariance_matrix_rp_wprp_Zmin_' + str(np.round(zmin, 1)) + '_Zmax_' + str(
                                   np.round(zmax, 1)) + '.txt')

    cov_mat = np.loadtxt(p_2_cov_mat,unpack=True)
    cov_mat = float(N)*cov_mat
    cov_list.append(cov_mat)

    err = np.sqrt(np.diag(cov_mat))

    parameters = np.array([Om_MD,0.8228,0.6777])#,4.2])
    test_wp_model = model(rp,pi,zmin,zmax,parameters)

    ax11.errorbar(rp, rp*wp, yerr=err, fmt=".", capsize=0, color=col, label='z={:.1f}'.format(zmin)+'-{:.1f}'.format(zmax))
    ax11.plot(rp, test_wp_model)
    ax12.errorbar(rp, rp*wp/test_wp_model, yerr=err/test_wp_model, fmt=".", capsize=0, color=col)

    ax21.scatter(rp, wp, label='z={:.1f}'.format(zmin)+'-{:.1f}'.format(zmax))
    ax21.plot(rp, test_wp_model/rp)
    ax22.scatter(rp,wp/test_wp_model*rp)

cov_list = np.array(cov_list)

ax12.hlines(1,min(rp),max(rp), color='black')
ax22.hlines(1,min(rp),max(rp), color='black')
ax12.set_ylim(0.3,1.7)
ax22.set_ylim(0.3,1.7)

ax11.tick_params(labelsize=13)
ax12.tick_params(labelsize=13)
ax11.set_xscale('log')
ax12.set_xscale('log')
ax11.legend(fontsize=13)
ax12.set_xlabel(r'$r_p$ (Mpc/h)',fontsize=13)
ax11.set_ylabel(r'$r_p w_p(r_p)$',fontsize=13)
ax12.set_ylabel('Data/Model',fontsize=13)
ax11.grid(True)
ax12.grid(True)
fig1.tight_layout()
fig1.savefig(outpl1, overwrite=True)

ax21.tick_params(labelsize=13)
ax22.tick_params(labelsize=13)
ax21.set_xscale('log')
ax22.set_xscale('log')
ax21.set_yscale('log')
ax21.legend(loc='upper right', fontsize=13)
ax22.set_xlabel(r'$r_p$ (Mpc/h)',fontsize=13)
ax21.set_ylabel(r'$w_p(r_p)$',fontsize=13)
ax22.set_ylabel('Data/Model',fontsize=13)
ax21.grid(True)
ax22.grid(True)
fig2.tight_layout()
fig2.savefig(outpl2, overwrite=True)

# C_ell^2/(Area of erass survey*ell*delta ell)
#cov = np.diag(cls_clu**2)*2*np.pi/((4*np.pi*18000)*(np.pi/180)**2*ell*delta_ell) 
icov = []
sel=[]
dwp_list = []
for wp_rp, rp_, DDc, covar in zip(wp_list, rp_list, DD_list, cov_list):
    sel_ = ~np.isnan(wp_rp)&(rp_>8)&(rp_<84)&(wp_rp>1e-1)
    sel.append(sel_)    #frac_err = 1./(np.sqrt(DD_cts[sel]))
    #frac_err = 0.2
    #d_wp = frac_err*wp_rp[sel_]
    #cov = np.diag((d_wp)**2)
    cov = covar[sel_][:,sel_]
    icov_ = np.linalg.inv(cov) #inverse of cov
    icov.append(icov_)
    #dwp_list.append(d_wp)
    dwp_list.append(np.sqrt(np.diag(cov)))


#define Likelihood - fits Omegac sigma8

def lnprior(cube):
    pars = cube.copy()
    pars[0] = (cube[0])*0.45 + 0.15  #OmegaM = 0.15 0.4
    pars[1] = (cube[1])*0.6 + 0.5  #sigma8 = 0.5 1.5
    pars[2] = (cube[2])*0.6 + 0.4  #h = 0.4 1.4
    #pars[3] = (cube[3])*5.5 + 0.5  #h = 0.5 5.5
    return pars

def lnlike(pars):
    likelihood = 0
    for w,p,r,sele,ic,zl,zu in zip(wp_list, pi_list, rp_list, sel, icov, Z_mins, Z_maxs):
        try:
            diff = r[sele]*w[sele]-model(r[sele], p, zl, zu, pars)
            likelihood += -np.dot(diff,np.dot(ic,diff))/2.0
        except:
            likelihood = -1e20
    return likelihood

names = [r'$\Omega_{M}$', r'$\sigma_8$', 'h']#, 'b']
print('initialize sampler...')
sampler = ultranest.ReactiveNestedSampler(param_names=names, loglike=lnlike, transform=lnprior, log_dir=logdir, resume=True)
print('run sampler...')
result = sampler.run(min_num_live_points=400)

print('print results...')
sampler.print_results()
print('plot sampler...')
sampler.plot()

popt = np.array(result['posterior']['mean'])
pvar = np.array(result['posterior']['stdev'])

fil=open(os.path.join(outdir,'values_wp_lightcone.txt'),'a+')
#fil.write('OmegaM sigma8 h deltaOmegaM deltasigma8 deltah\n')
fil.write('\n{:.2f} {:.3f} {:.3f} {:.3f} {:.3f} {:.3f} {:.3f}\n'.format(float(N),popt[0],popt[1],popt[2], pvar[0],pvar[1],pvar[2]))
fil.close()

print('plotting corner...')
outpl = os.path.join(outdir, 'corner_wp_N'+N+'.png')
parameters = np.array(result['weighted_samples']['points'])
weights = np.array(result['weighted_samples']['weights'])
weights /= weights.sum()
cumsumweights = np.cumsum(weights)
mask = cumsumweights > 1e-4

# note about sigmas: 1sigma in 2d contains 1-e^-0.5 (39%, not 68%)
# note about sigmas: 2sigma in 2d contains 1-e^-2 (86%, not 95%)
fig=corner.corner(parameters[mask,:], weights=weights[mask], labels=names, show_titles=True, color='r',
                  bins=50,smooth=True, smooth1d=True, quantiles=[0.025,0.16,0.84,0.975],
                  label_kwargs={'fontsize':15,'labelpad':20}, title_kwargs={"fontsize":15},
                  levels=[0.393, 0.865], fill_contours=True,title_fmt='.3f',
                  truths=[Om_MD,0.8228,0.6766],truth_color='C0')


fig.tight_layout()
fig.savefig(outpl, overwrite=True)


print('final plot...')
outpl = os.path.join(outdir, 'corr_func_wp_final_N'+N+'.png')
fig3 = plt.figure(figsize=(6,6))
gs3 = fig3.add_gridspec(4,1,hspace=0.0)
ax31 = fig3.add_subplot(gs3[0:3, :])

for sig, wp_rp, s, dwp, zl,zu, col in zip(rp_list,wp_list,sel, dwp_list, Z_mins, Z_maxs, colors):
    ax31.errorbar(sig[s], sig[s]*wp_rp[s], yerr=dwp, fmt=".", capsize=0, color=col, label='z={:.1f}'.format(zl)+'-{:.1f}'.format(zu))

    rp_grid = np.logspace(np.log10(7.5), np.log10(85), 200)
    band = PredictionBand(rp_grid)
    for Om_, s8_, h_ in sampler.results['samples']:
    # compute for each time the y value
        band.add(model(rp_grid,pi, zl, zu, [Om_, s8_, h_]))#, b_]))

    band.line(color=col)
    # add 1 sigma quantile
    band.shade(alpha=0.4, color=col)#label=r'1$\sigma$')
    # add 2 sigma quantile
    band.shade(q=0.477, alpha=0.2, color=col)#label=r'2$\sigma$')

ax32 = fig3.add_subplot(gs3[3, :], sharex=ax31)
for sig, wp_rp, s, dwp, zl,zu, col in zip(rp_list,wp_list,sel, dwp_list, Z_mins, Z_maxs, colors):
    ax32.errorbar(sig[s], sig[s]*wp_rp[s]/model(sig[s],pi,zl,zu,popt), yerr=dwp/model(sig[s],pi,zl,zu,popt), fmt=".", capsize=0,color=col)

ax32.hlines(1,min(rp),max(rp), color='black')

ax31.tick_params(labelsize=13)
ax32.tick_params(labelsize=13)
ax32.set_ylim(0.3,1.7)
ax31.set_xlim(7,86)
ax32.set_xlim(7,86)
#plt.ylim(0,1300)
ax31.set_xscale('log')
ax32.set_xscale('log')
ax31.legend(fontsize=13)
ax32.set_xlabel(r'$r_p$ (Mpc/h)',fontsize=13)
ax31.set_ylabel(r'$r_p w_p(r_p)$',fontsize=13)
ax32.set_ylabel('Data/Model',fontsize=13)
ax31.grid(True)
ax32.grid(True)
fig3.tight_layout()
fig3.savefig(outpl, overwrite=True)


print('done!')

