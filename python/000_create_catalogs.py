import os, sys, glob
import numpy as np
from astropy.table import Table
import random

'''
Creates catalogs needed by CUTE
Data and Randoms

RA DEC z

R.Seppi (MPE)
26.02.2021
'''
p_2_cat = os.path.join('/ptmp/rseppi/catalogs','MD40_eRO_CLU_b8_CM_0_pixS_20.0.fits')
p_2_out = os.path.join('/u/rseppi/clustering','correlation_functions')

#p_2_cat = os.path.join('/home/rseppi/Downloads','MD40_eRO_CLU_b8_CM_0_pixS_20.0.fits')
#p_2_out = os.path.join('/home/rseppi/clustering','correlation_functions')

print('Reading HMD...')
t = Table.read(p_2_cat)

Z_mins = np.array([0.1,0.3,0.5,0.7])
Z_maxs = np.array([0.3,0.5,0.7,0.9])

for Z_lim, Z_up in zip(Z_mins, Z_maxs):
    #define paths
    p_2_data = os.path.join(p_2_out, 'catalog_Zmin_' + str(np.round(Z_lim, 1)) + '_Zmax_' + str(np.round(Z_up, 1)) + '_RA_DEC_Z_W.ascii')
    # random arrays, at least 10-20 times larger in size compared to the data array
    p_2_random = os.path.join(p_2_out, 'catalog_Zmin_' + str(np.round(Z_lim, 1)) + '_Zmax_' + str(np.round(Z_up, 1)) + '_RA_DEC_Z_W_RANDOMS.ascii')

    #cut at M500c 1e14 (to apply /h) #cut z <0.9 >0.1
    z_cut = (t['redshift_S']>Z_lim)&(t['redshift_S']<Z_up)
    mass_cut = t['HALO_M500c']>5e13 #1e14 the min value is 5.32e13
    t_cut=t[(mass_cut)&(z_cut)]        # redshift_S in (0.1,0.8) and HALO_M500c > 1e14 ---> 233477 halos

    ra,dec,z = t_cut['RA'], t_cut['DEC'],t_cut['redshift_S']
    weights = np.ones(len(ra))
    DATA = np.transpose( [ra, dec, z, weights])
    np.savetxt(p_2_data, DATA, fmt="%s")

    # take N random = 10*N_data :
    N=10
    size = N*len(t_cut)
    print(size)
    uu = np.random.uniform(size=size)
    decr = np.arccos(1 - 2 * uu) * 180 / np.pi - 90.
    rar = np.random.uniform(size=size) * 2 * np.pi * 180 / np.pi
    zr = np.tile(z,N)
    print(zr)
    random.shuffle(zr)
    weightsr = np.ones(len(rar))

    RANDOM = np.transpose( [rar, decr, zr, weightsr])
    np.savetxt(p_2_random, RANDOM, fmt="%s" )

print('done!')
