import sys, os, glob
import numpy as np

'''
Compute correlation function from one of the 2048 patchy mocks 
and compare to Rodriguez Torres 
'''

p_2_mocks = os.path.join('/ptmp/rseppi/patchy_mocks')
p_2_cat = sorted(glob.glob(os.path.join(p_2_mocks,'realizations','*dat')))[0]
rand_dir = os.path.join('/ptmp/rseppi/patchy_mocks','randoms')
p_2_rand = os.path.join(rand_dir,'Patchy-Mocks-Randoms-DR12NGC-COMPSAM_V6C_x10.dat')
p_2_out = os.path.join('/u/rseppi/clustering','python','test1')

print('creating data...')
p_2_cat_out = os.path.join(p_2_out,'patchy_mocks0_data.ascii')
if not os.path.isfile(p_2_cat_out):
    data = np.loadtxt(p_2_cat, unpack=True)
    ra, dec, z, mstar, nbar, bias, veto, fiber = data
    sel = (z > 0.43) & (z < 0.7)
    ra, dec, z, mstar, nbar, bias, veto, fiber = ra[sel], dec[sel], z[sel], mstar[sel], nbar[sel], bias[sel], veto[sel], fiber[sel]
    weights = (veto * fiber) / (1 + 10000 * nbar)
    DATA = np.transpose([ra, dec, z, weights])
    np.savetxt(p_2_cat_out, DATA, fmt="%s")

print('creating randoms...')
p_2_rand_out = os.path.join(p_2_out,'patchy_mocks0_randoms.ascii')
if not os.path.isfile(p_2_rand_out):
    randoms = np.loadtxt(p_2_rand, unpack=True)
    ra, dec, z, nbar, bias, veto, flag = randoms
    sel = (z>0.43)&(z<0.7)
    ra, dec, z, nbar, bias, veto, flag = ra[sel], dec[sel], z[sel], nbar[sel], bias[sel], veto[sel], flag[sel]
    weights = (veto * flag) / (1 + 10000 * nbar)
    RANDOMS = np.transpose([ra, dec, z, weights])
    np.savetxt(p_2_rand_out, RANDOMS, fmt='%s')

print('create CUTE input...')
# saves data with and without weights
p_2_data = os.path.join(p_2_cat_out)
# random arrays, at least 10-20 times larger in size compared to the data array
p_2_random = os.path.join(p_2_rand_out)
# with weights, replicate redshift and weight arrays to assign to randoms
# Monopole
f = open(os.path.join(p_2_out,'commands.monopole.ini'), 'w')
f.write('data_filename= ' +	p_2_data +' \n')
f.write('random_filename= ' + p_2_random +' \n')
f.write('input_format= 2 \n')
f.write('output_filename= ' + p_2_out+'/patchy_mock.monopole.2pcf \n')
f.write('corr_type= monopole \n')
f.write('omega_M= 0.307 \n')
f.write('omega_L= 0.693 \n')
f.write('w= -1 \n')
f.write('log_bin= 0 \n')
f.write('dim1_min_logbin= 0.1 \n')
f.write('dim1_max= 200. \n')
f.write('dim1_nbin= 40 \n')
f.write('dim2_max= 100. \n')
f.write('dim2_nbin= 30 \n')
f.write('dim3_min= 0.00 \n')
f.write('dim3_max= 3. \n')
f.write('dim3_nbin= 1 \n')
f.close()
# 3D clustering
f = open(os.path.join(p_2_out, 'commands.3dps.ini'), 'w')
f.write('data_filename= ' +	p_2_data +' \n')
f.write('random_filename= ' + p_2_random +' \n')
f.write('input_format= 2 \n')
f.write('output_filename= ' + p_2_out+'/patchy_mock.3dps.2pcf \n')
f.write('corr_type= 3D_ps \n')
f.write('omega_M= 0.307 \n')
f.write('omega_L= 0.693 \n')
f.write('w= -1 \n')
f.write('log_bin= 1 \n')
f.write('dim1_min_logbin= 0.4 \n')
f.write('dim1_max= 100. \n')
f.write('dim1_nbin= 20 \n')
f.write('dim2_max= 100. \n')
f.write('dim2_nbin= 20 \n')
f.write('dim3_min= 0.00 \n')
f.write('dim3_max= 6. \n')
f.write('dim3_nbin= 1 \n')
f.close()