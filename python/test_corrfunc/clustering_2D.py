import sys,os,glob
import numpy as np
import pyccl as ccl
from astropy.table import Table
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import corner
from colossus.cosmology import cosmology
from colossus.lss import bias
from colossus.lss import peaks
import ultranest
from ultranest.plot import runplot
from ultranest.plot import PredictionBand

'''
Test code to compute clustering and fits cosmology
Uses eroconda (rseppipy) and pyccl

Needs AXV instructions

R.Seppi (MPE)
04.02.2021

todo: compute directly correlation function
then fit it with the model obtained from power spectrum
'''
#p_2_cat = os.path.join('/home/rseppi/Downloads','MD40_eRO_CLU_b8_CM_0_pixS_20.0.fits')
#p_2_data = os.path.join('/home/rseppi/clustering','correlation_functions','corr_func_2D.txt')
#outdir = os.path.join('/home/rseppi/clustering','figures')
#logdir = os.path.join('/home/rseppi/clustering','my_fit_2d')

p_2_cat = os.path.join('/ptmp/rseppi/catalogs','MD40_eRO_CLU_b8_CM_0_pixS_20.0.fits')
p_2_data = sorted(glob.glob(os.path.join('/u/rseppi/clustering','correlation_functions','corr_func_2D*z*.txt')))
outdir = os.path.join('/u/rseppi/clustering','figures')
logdir = os.path.join('/u/rseppi/clustering','my_fit_2d')

# Turn off multi-threading to avoid issues with MPIPool.
os.environ["OMP_NUM_THREADS"] = "1"

#Use MultiDark cosmology
Om_MD = 0.307115 #matter
Ob_MD = 0.048206 #baryon
Oc_MD = Om_MD-Ob_MD #cold dark matter
print('Setting up cosmology...')
cosmo_planck18 = ccl.Cosmology(
    Omega_c = 0.11933/0.6766**2, # 0.26066
    Omega_b = 0.02242/0.6766**2, # 0.04897
    h = 0.6766,
    n_s = 0.9665,
    sigma8 = None,
    A_s = np.e**(3.047)/10**10, # 2.1052e-09
    Omega_k = 0.0,
    Omega_g = None,
    Neff = 3.046, # standard model of particle physics
    m_nu = 0.0, # in eV
    m_nu_type = None, # 'inverted', 'normal', 'equal' or 'list'
    w0 = -1.0,
    wa = 0.0,
    T_CMB = ccl.physical_constants.T_CMB, # 2.725
    bcm_log10Mc=14.079181246047625, # BCM baryon correction model on PK https://arxiv.org/abs/1510.06034
    bcm_etab=0.5,                   # BCM
    bcm_ks=55.0,                    # BCM
    mu_0=0.0,                       # modified gravity model parameter
    sigma_0=0.0,                    # modified gravity model parameter
    z_mg=None,                      # modified growth rate parameter
    df_mg=None,                     # modified growth rate parameter
    transfer_function='boltzmann_camb',  # for power spectrum analysis
    matter_power_spectrum='halofit',     # for power spectrum analysis
    baryons_power_spectrum='nobaryons',  # for power spectrum analysis
    mass_function='tinker10',            # for mass function analysis
    halo_concentration='duffy2008',      # for mass function analysis
    emulator_neutrinos='strict',         # for power spectrum analysis
)
cosmo = cosmo_planck18
#cosmo = ccl.Cosmology(Omega_c = Oc_MD, Omega_b = Ob_MD, h = 0.6777, sigma8 = 0.8228, n_s = 0.96)
cosmo_colossus = cosmology.setCosmology('multidark-planck')


outpl1 = os.path.join(outdir,'corr_func_2D.png')
fig1, ax1 = plt.subplots(1,1,figsize=(5,5))
outpl2 = os.path.join(outdir,'corr_func_2D_loglog.png')
fig2, ax2 = plt.subplots(1,1,figsize=(5,5))

#get distribution with redshift
print('Reading table...')
t_=Table.read(p_2_cat)
mass_cut = t_['HALO_M500c']>1e14


def model(x, zl, zu, pars):
    '''
    :param x: array. Angles in degrees
    :param zl: float. Lower redshift limit
    :param zu: float. Upper redshift limit
    :param pars: cosmological parameters. Available: OmegaM, sigma8, h, b.
    :return: 2D correlation function w(theta). Same dimension of x
    '''
    # Om,s8,h,b = pars
    Om, s8, h = pars
    Oc = Om - Ob_MD

    z_cut = (t_['redshift_S'] > zl) & (t_['redshift_S'] < zu)
    t = t_[(mass_cut) & (z_cut)]
    Nz, zbins = np.histogram(t['redshift_S'], bins=50)
    z = (zbins[1:] + zbins[:-1]) / 2.

    nu = peaks.peakHeight(np.average(t['HALO_Mvir']), np.average(t['redshift_S']))
    b = bias.haloBiasFromNu(nu, model='comparat17')
    b = np.tile(b, len(z))

    cosmol = ccl.Cosmology(
        Omega_c=Oc,  # 0.26066
        Omega_b=0.02242 / 0.6766 ** 2,  # 0.04897
        h=h,
        n_s=0.9665,
        sigma8=s8,
        Omega_k=0.0,
        Omega_g=None,
        Neff=3.046,  # standard model of particle physics
        m_nu=0.0,  # in eV
        m_nu_type=None,  # 'inverted', 'normal', 'equal' or 'list'
        w0=-1.0,
        wa=0.0,
        T_CMB=ccl.physical_constants.T_CMB,  # 2.725
        bcm_log10Mc=14.079181246047625,  # BCM baryon correction model on PK https://arxiv.org/abs/1510.06034
        bcm_etab=0.5,  # BCM
        bcm_ks=55.0,  # BCM
        mu_0=0.0,  # modified gravity model parameter
        sigma_0=0.0,  # modified gravity model parameter
        z_mg=None,  # modified growth rate parameter
        df_mg=None,  # modified growth rate parameter
        transfer_function='boltzmann_camb',  # for power spectrum analysis
        matter_power_spectrum='halofit',  # for power spectrum analysis
        baryons_power_spectrum='nobaryons',  # for power spectrum analysis
        mass_function='tinker10',  # for mass function analysis
        halo_concentration='duffy2008',  # for mass function analysis
        emulator_neutrinos='strict',  # for power spectrum analysis
    )
    # cosmol = ccl.Cosmology(Omega_c = Oc, Omega_b = Ob_MD, h = h, sigma8 = s8, n_s = 0.96)
    tracer = ccl.NumberCountsTracer(cosmol, has_rsd=True, dndz=(z, Nz), bias=(z, b))
    ell = 180. / x
    ell = np.sort(ell)
    cl = ccl.angular_cl(cosmol, tracer, tracer, ell)  # Clustering: angular power spectrum
    wtheta_model = ccl.correlation(cosmol, ell, cl, x, type='NN', method='FFTLog')
    return wtheta_model

theta_list, wtheta_list, DD_list, DR_list, RR_list = [], [], [], [], []
z_low_list, z_up_list = [], []
for data in p_2_data:
    z_low = float(data[58:61])
    z_up = float(data[62:65])

    z_low_list.append(z_low)
    z_up_list.append(z_up)

    theta, wtheta, DD_cts, DR_cts, RR_cts = np.loadtxt(data, unpack=True, dtype='float32')

    theta_list.append(theta)
    wtheta_list.append(wtheta)
    DD_list.append(DD_cts)
    DR_list.append(DR_cts)
    RR_list.append(RR_list)

    print('Now work on the model')
    parameters = np.array([Om_MD,0.8228,0.6777])#,4.2])
    test_model = model(theta, z_low, z_up, parameters)

    ax1.scatter(theta, wtheta, label='z={:.1f}'.format(z_low)+'-{:.1f}'.format(z_up))
    ax1.plot(theta, test_model)

    ax2.scatter(theta, wtheta, label='z={:.1f}'.format(z_low)+'-{:.1f}'.format(z_up))
    ax2.plot(theta, test_model)

ax1.tick_params(labelsize=13)
ax1.set_xscale('log')
ax1.legend(loc='upper right', fontsize=13)
ax1.set_xlabel(r'$\theta$ (deg)',fontsize=13)
ax1.set_ylabel(r'$w(\theta)$',fontsize=13)
ax1.grid(True)
fig1.tight_layout()
fig1.savefig(outpl1, overwrite=True)

ax2.tick_params(labelsize=13)
ax2.set_ylim(1e-3)
ax2.set_xscale('log')
ax2.set_yscale('log')
ax2.legend(loc='upper right', fontsize=13)
ax2.set_xlabel(r'$\theta$ (deg)',fontsize=13)
ax2.set_ylabel(r'$w(\theta)$',fontsize=13)
ax2.grid(True)
fig2.tight_layout()
fig2.savefig(outpl2, overwrite=True)



# C_ell^2/(Area of erass survey*ell*delta ell)
#cov = np.diag(cls_clu**2)*2*np.pi/((4*np.pi*18000)*(np.pi/180)**2*ell*delta_ell) 
#cov = np.diag((0.1*wtheta)**2)
icov = []
sel=[]
dw_list = []
for wth, DDc in zip(wtheta_list, DD_list):
    sel_ = ~np.isnan(wth)
    sel.append(sel_)
    #frac_err = 1./(np.sqrt(DDc[sel]))
    frac_err = 0.2
    d_wtheta = frac_err*wth[sel_]
    cov = np.diag((d_wtheta)**2)
    print(cov)
    icov_ = np.linalg.inv(cov) #inverse of cov
    icov.append(icov_)
    dw_list.append(d_wtheta)

#define Likelihood - fits Omegac sigma8

def lnprior(cube):
    pars = cube.copy()
    pars[0] = (cube[0])*0.3 + 0.2  #OmegaM = 0.2 0.5
    pars[1] = (cube[1])*0.6 + 0.5  #sigma8 = 0.5 1.1
    pars[2] = (cube[2])*0.4 + 0.5  #h = 0.5 0.9
    return pars

def lnlike(pars):
    likelihood = 0
    for w,t,s,ic,zl,zu in zip(wtheta_list, theta_list, sel, icov, z_low_list, z_up_list):
        diff = w[s]-model(t[s], zl, zu, pars)
        likelihood += -np.dot(diff,np.dot(ic,diff))/2.0
    return likelihood


names = [r'$\Omega_{M}$', r'$\sigma_8$', 'h'] #, 'b']
sampler = ultranest.ReactiveNestedSampler(param_names=names, loglike=lnlike, transform=lnprior, log_dir=logdir, resume=True)
result = sampler.run(min_num_live_points=500)

sampler.print_results()
#runplot(results=result)


outpl = os.path.join(outdir, 'corr_func_2d_final.png')
colors = ['C0', 'C1', 'C2', 'C3']
plt.figure(figsize=(5., 5.))
for th, wth, s, dw, zl,zu, col in zip(theta_list,wtheta_list,sel, dw_list, z_low_list, z_up_list, colors):
    plt.errorbar(th[s], wth[s], yerr=dw, fmt=".", capsize=0, label='z={:.1f}'.format(zl)+'-{:.1f}'.format(zu))

    theta_grid = np.logspace(-2, np.log10(4), 200)
    band = PredictionBand(theta_grid)
    for Om_, s8_, h_ in sampler.results['samples']:
    # compute for each time the y value
        band.add(model(theta_grid, zl, zu, [Om_, s8_, h_]))#, h]))

    band.line(color=col)
    # add 1 sigma quantile
    band.shade(alpha=0.4, color=col)#label=r'1$\sigma$')
    # add 2 sigma quantile
    band.shade(q=0.477, alpha=0.2, color=col)#label=r'2$\sigma$')
plt.xticks(fontsize=13)
plt.yticks(fontsize=13)
plt.ylim(1e-3)
plt.yscale('log')
plt.xscale('log')
plt.legend(fontsize=13)
plt.xlabel(r'$\theta$ (deg)', fontsize=13)
plt.ylabel(r'$w(\theta)$', fontsize=13)
plt.grid(True)
plt.tight_layout()
plt.savefig(outpl, overwrite=True)

outpl = os.path.join(outdir, 'corner_2D.png')
parameters = np.array(result['weighted_samples']['points'])
weights = np.array(result['weighted_samples']['weights'])
weights /= weights.sum()
cumsumweights = np.cumsum(weights)
mask = cumsumweights > 1e-4

# note about sigmas: 1sigma in 2d contains 1-e^-0.5 (39%, not 68%)
# note about sigmas: 2sigma in 2d contains 1-e^-2 (86%, not 95%)
fig=corner.corner(parameters[mask,:], weights=weights[mask], labels=names, show_titles=True, color='r',bins=50,smooth=True,
                  smooth1d=True, quantiles=[0.025,0.16,0.84,0.975], label_kwargs={'fontsize':15,'labelpad':20},
                  title_kwargs={"fontsize":15},levels=[0.393, 0.865], fill_contours=True,title_fmt='.3f')


fig.tight_layout()
fig.savefig(outpl, overwrite=True)
sampler.plot()

print('done!')

