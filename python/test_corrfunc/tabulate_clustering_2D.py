import sys,os
import numpy as np
import pyccl as ccl
from astropy.table import Table
import matplotlib
matplotlib.use('Agg')
from Corrfunc.utils import convert_3d_counts_to_cf
from Corrfunc.mocks.DDtheta_mocks import DDtheta_mocks
from colossus.cosmology import cosmology

'''
Test code to compute clustering and fits cosmology
Uses eroconda (rseppipy) and pyccl

Needs AXV instructions

R.Seppi (MPE)
04.02.2021

todo: compute directly correlation function
then fit it with the model obtained from power spectrum
'''

#p_2_cat = os.path.join('/data37s/simulation_1/MD/MD_4.0Gpc','MD40_eRO_CLU_b8_CM_0_pixS_20.0.fits')
#p_2_cat = os.path.join('/home/rseppi/Downloads','MD40_eRO_CLU_b8_CM_0_pixS_20.0.fits')
#p_2_rand = os.path.join('/home/rseppi/clustering/randoms','randoms.txt')
#outfile = os.path.join('/home/rseppi/clustering','correlation_functions','corr_func_2D.txt')

p_2_cat = os.path.join('/ptmp/rseppi/catalogs','MD40_eRO_CLU_b8_CM_0_pixS_20.0.fits')
p_2_rand = os.path.join('/u/rseppi/clustering/randoms','randoms.txt')
outfile = os.path.join('/u/rseppi/clustering','correlation_functions','corr_func_2D.txt')

#Use MultiDark cosmology
Om_MD = 0.307115 #matter
Ob_MD = 0.048206 #baryon
Oc_MD = Om_MD-Ob_MD #cold dark matter
cosmo_planck18 = ccl.Cosmology(
    Omega_c = 0.11933/0.6766**2, # 0.26066
    Omega_b = 0.02242/0.6766**2, # 0.04897
    h = 0.6766,
    n_s = 0.9665,
    sigma8 = None,
    A_s = np.e**(3.047)/10**10, # 2.1052e-09
    Omega_k = 0.0,
    Omega_g = None,
    Neff = 3.046, # standard model of particle physics
    m_nu = 0.0, # in eV
    m_nu_type = None, # 'inverted', 'normal', 'equal' or 'list'
    w0 = -1.0,
    wa = 0.0,
    T_CMB = ccl.physical_constants.T_CMB, # 2.725
    bcm_log10Mc=14.079181246047625, # BCM baryon correction model on PK https://arxiv.org/abs/1510.06034
    bcm_etab=0.5,                   # BCM
    bcm_ks=55.0,                    # BCM
    mu_0=0.0,                       # modified gravity model parameter
    sigma_0=0.0,                    # modified gravity model parameter
    z_mg=None,                      # modified growth rate parameter
    df_mg=None,                     # modified growth rate parameter
    transfer_function='boltzmann_camb',  # for power spectrum analysis
    matter_power_spectrum='halofit',     # for power spectrum analysis
    baryons_power_spectrum='nobaryons',  # for power spectrum analysis
    mass_function='tinker10',            # for mass function analysis
    halo_concentration='duffy2008',      # for mass function analysis
    emulator_neutrinos='strict',         # for power spectrum analysis
)
cosmo = cosmo_planck18
#cosmo = ccl.Cosmology(Omega_c = Oc_MD, Omega_b = Ob_MD, h = 0.6777, sigma8 = 0.8228, n_s = 0.96)
cosmo_colossus = cosmology.setCosmology('multidark-planck')


print('Reading HMD...')
t = Table.read(p_2_cat)
#cut at M500c 1e14 (to apply /h) 
mass_cut = t['HALO_M500c']>1e14
z_cut = (t['redshift_S']>0.1)&(t['redshift_S']<0.8)
t=t[(mass_cut)&(z_cut)]        # redshift_S in (0.1,0.8) and HALO_M500c > 1e14 ---> 233477 halos


ra = np.array(t['RA'].byteswap().newbyteorder())
dec = np.array(t['DEC'].byteswap().newbyteorder())
N=len(ra)
print(ra,dec)

print('Reading randoms')
rand_ra , rand_dec = np.loadtxt(p_2_rand, unpack=True, dtype='float32')
rand_N = len(rand_ra)


#thetamin = 0.002
#thetamax = 4.
nbins = 30
#bins = np.logspace(np.log10(thetamin), np.log10(thetamax), nbins+1)
bins = np.hstack(( np.arange(0.02, 0.1, 0.002), np.arange(0.1,1.0,0.05), np.arange(1.0, 4., 0.1) ))
#bins = np.hstack(( np.arange(0.0, 0.1, 0.002), np.arange(0.1, 5., 0.1) ))
bins = np.logspace(np.log10(2e-2),np.log10(4),nbins+1)
theta = (bins[1:]+bins[:-1])/2.

print('Compute pair counts')
nthreads = 4
autocorr = 1
# Auto pairs counts in RR
print('DD')
DD_counts = DDtheta_mocks(autocorr, nthreads, bins, ra, dec)

# Auto pairs counts in DR
print('DR')
autocorr = 0
DR_counts = DDtheta_mocks(autocorr, nthreads, bins, RA1=ra, DEC1=dec, RA2=rand_ra, DEC2=rand_dec)

# Auto pairs counts in RR
print('RR')
autocorr=1
RR_counts = DDtheta_mocks(autocorr, nthreads, bins, rand_ra, rand_dec)

# All the pair counts are done, get the angular correlation function
print('Compute the correlation function')
wtheta = convert_3d_counts_to_cf(N, N, rand_N, rand_N, DD_counts, DR_counts, DR_counts, RR_counts)

np.savetxt(outfile, np.transpose([theta, wtheta, DD_counts['npairs'] , DR_counts['npairs'] , RR_counts['npairs'] ]))
