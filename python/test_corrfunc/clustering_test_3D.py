import sys,os
import numpy as np
import pyccl as ccl
from astropy.table import Table
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import emcee
import corner
from colossus.cosmology import cosmology
from colossus.lss import bias
from colossus.lss import peaks
from schwimmbad import MPIPool
from shutil import copy2

# Turn off multi-threading to avoid issues with MPIPool.
os.environ["OMP_NUM_THREADS"] = "1"

'''
Test code to compute clustering and fits cosmology
Uses eroconda (rseppipy) and pyccl

Needs AXV instructions

R.Seppi (MPE)
04.02.2021

todo: compute directly correlation function
then fit it with the model obtained from power spectrum

predict 3d corrfunc from model of P(k) - with pyccl
'''

#p_2_cat = os.path.join('/data37s/simulation_1/MD/MD_4.0Gpc','MD40_eRO_CLU_b8_CM_0_pixS_20.0.fits')
#p_2_cat = os.path.join('/home/rseppi/Downloads','MD40_eRO_CLU_b8_CM_0_pixS_20.0.fits')
#p_2_data = os.path.join('/home/rseppi/clustering','correlation_functions','corr_func_3D.txt')
#outdir = os.path.join('/home/rseppi/clustering','figures')
#backendfile = os.path.join('/home/rseppi/clustering','logs','backend_3D.h5')
#backendfile_cp = os.path.join('/home/rseppi/clustering','logs','backend_3D_copy.h5')

p_2_cat = os.path.join('/ptmp/rseppi/catalogs','MD40_eRO_CLU_b8_CM_0_pixS_20.0.fits')
p_2_data = os.path.join('/u/rseppi/clustering','correlation_functions','corr_func_3D.txt')
outdir = os.path.join('/u/rseppi/clustering','figures')
backendfile = os.path.join('/u/rseppi/clustering','logs','backend_3D.h5')
backendfile_cp = os.path.join('/u/rseppi/clustering','logs','backend_3D_copy.h5')


#Use MultiDark cosmology
Om_MD = 0.307115 #matter
Ob_MD = 0.048206 #baryon
Oc_MD = Om_MD-Ob_MD #cold dark matter
#cosmo = ccl.Cosmology(Omega_c = Oc_MD, Omega_b = Ob_MD, h = 0.6777, sigma8 = 0.8228, n_s = 0.96, matter_power_spectrum='emu', emulator_neutrinos='equalize', transfer_function=None)
cosmo_planck18 = ccl.Cosmology(
    Omega_c = 0.11933/0.6766**2, # 0.26066
    Omega_b = 0.02242/0.6766**2, # 0.04897
    h = 0.6766,
    n_s = 0.9665,
    sigma8 = None,
    A_s = np.e**(3.047)/10**10, # 2.1052e-09
    Omega_k = 0.0,
    Omega_g = None,
    Neff = 3.046, # standard model of particle physics
    m_nu = 0.0, # in eV
    m_nu_type = None, # 'inverted', 'normal', 'equal' or 'list'
    w0 = -1.0,
    wa = 0.0,
    T_CMB = ccl.physical_constants.T_CMB, # 2.725
    bcm_log10Mc=14.079181246047625, # BCM baryon correction model on PK https://arxiv.org/abs/1510.06034
    bcm_etab=0.5,                   # BCM
    bcm_ks=55.0,                    # BCM
    mu_0=0.0,                       # modified gravity model parameter
    sigma_0=0.0,                    # modified gravity model parameter
    z_mg=None,                      # modified growth rate parameter
    df_mg=None,                     # modified growth rate parameter
    transfer_function='boltzmann_camb',  # for power spectrum analysis
    matter_power_spectrum='halofit',     # for power spectrum analysis
    baryons_power_spectrum='nobaryons',  # for power spectrum analysis
    mass_function='tinker10',            # for mass function analysis
    halo_concentration='duffy2008',      # for mass function analysis
    emulator_neutrinos='strict',         # for power spectrum analysis
)
cosmo = cosmo_planck18
#cosmo = ccl.Cosmology(Omega_c = Oc_MD, Omega_b = Ob_MD, h = 0.6777, sigma8 = 0.8228, n_s = 0.96, matter_power_spectrum='halofit', transfer_function='bbks')
cosmo_colossus = cosmology.setCosmology('multidark-planck')

t=Table.read(p_2_cat)
mass_cut = t['HALO_M500c']>1e14
z_cut = (t['redshift_S']>0.1)&(t['redshift_S']<0.3)
t=t[(mass_cut)&(z_cut)]

r, xi, DD_cts, DR_cts, RR_cts = np.loadtxt(p_2_data, unpack=True, dtype='float32')


print('Model of the correlation function')
def model(x, pars):
    Om,s8,h,b = pars
    Oc = Om - Ob_MD
    b = np.tile(b,len(x))
    cosmol = ccl.Cosmology(Omega_c=Oc, Omega_b=Ob_MD, h=h, sigma8=s8, n_s=0.96,
                          matter_power_spectrum='halofit', transfer_function='bbks')
    xi_model = ccl.correlation_3d(cosmol, a=1 / (1 + np.average(t['redshift_S'])), r=x)
    xi_model = xi_model * b**2
    return xi_model

nu = peaks.peakHeight(np.average(t['HALO_Mvir']), np.average(t['redshift_S']))
b = bias.haloBiasFromNu(nu, model='comparat17')
parameters = np.array([Oc_MD, 0.8228, 0.677, b])
test_xi_model = model(r,parameters)


outpl = os.path.join(outdir,'corr_func_3d.png')
plt.figure(figsize=(5,5))
plt.scatter(r, xi, label='MD40')
plt.plot(r, test_xi_model, label='Model MD')
plt.xticks(fontsize=13)
plt.yticks(fontsize=13)
#plt.ylim(top=0.3)
plt.xscale('log')
plt.legend(loc='upper right', fontsize=13)
plt.xlabel(r'$r$ (Mpc)',fontsize=13)
plt.ylabel(r'$\xi(r)$',fontsize=13)
plt.grid(True)
plt.tight_layout()
plt.savefig(outpl, overwrite=True)

outpl = os.path.join(outdir,'corr_func_3d_loglog.png')
plt.figure(figsize=(5,5))
plt.scatter(r, xi, label='MD40')
plt.plot(r, test_xi_model, label='Model MD')
plt.xticks(fontsize=13)
plt.yticks(fontsize=13)
#plt.ylim(top=0.3)
plt.yscale('log')
plt.xscale('log')
plt.legend(loc='upper right', fontsize=13)
plt.xlabel(r'$r$ (Mpc)',fontsize=13)
plt.ylabel(r'$\xi(r)$',fontsize=13)
plt.grid(True)
plt.tight_layout()
plt.savefig(outpl, overwrite=True)



#C_ell^2/(Area of erass survey*ell*delta ell)
#cov = np.diag(cls_clu**2)*2*np.pi/((4*np.pi*18000)*(np.pi/180)**2*ell*delta_ell) 
#cov = np.diag(xi_clu**2)*2*np.pi/((4*np.pi*42195)*(np.pi/180)**2*xi_clu*delta_xi)
sel = ~np.isnan(xi)
frac_err = 1./(np.sqrt(DD_cts[sel]))
d_xi = frac_err*xi[sel]
cov = np.diag((d_xi)**2)
icov = np.linalg.inv(cov) #inverse of cov



#define Likelihood - fits Omegac sigma8

def lnprior(pars):
    Om, s8, h, b = pars
    if 0.1<Om<0.5 and 0.5<s8<1.2 and 0.5<h<0.9 and 0.1<b<7:
        return 0.0
    else:
        return -np.inf

def lnlike(pars, x, y):#, yerr):
    diff = y-model(x, pars)
    return -np.dot(diff,np.dot(icov,diff))/2.0

def lnprob(pars):
    lp = lnprior(pars)
    if not np.isfinite(lp):
        return -np.inf
    return lp + lnlike(pars, r[sel], xi[sel])


ndim=4 #Omegac sigma8 h b

#start from 0.29 0.83 0.7 4.2
nwalkers = 200*ndim #100
nsteps = 1200 #1000
fburn = 0.25

def nr(loc,var):
    return np.random.normal(loc,var)

def make_plots(sampler):
    '''
    Function that makes fitting plots.
    Chains as a function of number of steps
    Corner plot with OmegaM, sigma8, h, bias
    Best fit model with 100 additional models picked randomly from the burnt-in chain.

    :param sampler: an emcee sampler
    '''
    names = [r'$\Omega_{M}$', r'$\sigma_8$', 'h', 'b']
    samples = sampler.get_chain()

    fig, axes = plt.subplots(ndim, 1, figsize=(10, 6))
    outpl = os.path.join(outdir, 'chains_3D.png')
    for i in range(ndim):
        ax = axes[i]
        ax.plot(samples[:, :, i], "k", alpha=0.3)
        ax.set_xlim(0, len(samples))
        ax.set_ylabel(names[i])
    axes[-1].set_xlabel("step number")
    fig.savefig(outpl, overwrite=True)

    flat_samples = sampler.get_chain(discard=int(fburn * nsteps), flat=True)  # 400
    popt = np.percentile(np.array(flat_samples), 50, axis=0)
    outpl = os.path.join(outdir, 'corner_3D.png')
    # note about sigmas: 1sigma in 2d contains 1-e^-0.5 (39%, not 68%)
    # note about sigmas: 2sigma in 2d contains 1-e^-2 (86%, not 95%)
    fig = corner.corner(flat_samples, labels=names, truths=popt, show_titles=True, color='r', bins=50, smooth=True,
                        smooth1d=True, quantiles=[0.025, 0.16, 0.84, 0.975],
                        label_kwargs={'fontsize': 20, 'labelpad': 20}, title_kwargs={"fontsize": 17},
                        levels=[0.393, 0.865], fill_contours=True, title_fmt='.3f')
    fig.tight_layout()
    fig.savefig(outpl, overwrite=True)

    outpl = os.path.join(outdir, 'corr_func_3d_final.png')
    plt.figure(figsize=(5., 5.))
    inds = np.random.randint(len(flat_samples), size=100)
    for ind in inds:
        sample = flat_samples[ind]
        plt.plot(r, model(r, sample), "C1", alpha=0.1)
    plt.errorbar(r[sel], xi[sel], yerr=d_xi, fmt=".C0", capsize=0)
    plt.plot(r, model(r, popt), "C2", label="truth")
    plt.xticks(fontsize=13)
    plt.yticks(fontsize=13)
    # plt.ylim(1e-3)
    plt.yscale('log')
    plt.xscale('log')
    plt.legend(fontsize=13)
    plt.xlabel(r'$r$ (Mpc)', fontsize=13)
    plt.ylabel(r'$\xi(r)$', fontsize=13)
    plt.grid(True)
    plt.tight_layout()
    plt.savefig(outpl, overwrite=True)

def run_mcmc(p0, pool, backend):
    '''
    Function to run mcmc sampling
    :param p0: starting points in the parameter space
    :param pool: object needed for mpi parallelization
    :param backend: class used to append to existing chains
    '''
    # initialize sampler
    sampler = emcee.EnsembleSampler(nwalkers, ndim, lnprob, pool=pool, backend=backend)

    # if the max iterations have already been done, exit

    # We'll track how the average autocorrelation time estimate changes
    index, autocorr, old_tau = 0, np.empty(nsteps), np.inf

    # Now we'll sample for up to steps
    # The mcmc breaks if the chain is longer than 100correlation time
    # or the relative change in autocorr is < 1%
    for _ in sampler.sample(p0, iterations=nsteps, progress=True):
        # Only check convergence every 100 steps
        if sampler.iteration % 100:
            continue

        # Save backend every 100 steps to safely avoid corruption.
        if os.path.isfile(backendfile):
            os.remove(backendfile)
        copy2(backendfile_cp, backendfile)
        # Compute the autocorrelation time so far
        # Using tol=0 means that we'll always get an estimate even
        # if it isn't trustworthy
        tau = sampler.get_autocorr_time(tol=0)
        autocorr[index] = np.mean(tau)
        index += 1

        # Check convergence
        converged = np.all(tau * 100 < sampler.iteration)
        converged &= np.all(np.abs(old_tau - tau) / tau < 0.01)
        if converged or sampler.iteration > nsteps:
            break
        old_tau = tau
    make_plots(sampler)

#initialize sampler
#set it up for MPI
mpi = True  #these will have to be from keyboard or command line
append = True

if mpi:
    pool = MPIPool()
    if not pool.is_master():
        pool.wait()
        sys.exit(0)
else:
    pool = None

#setup backend --> this has to be figured out
if os.path.isfile(backendfile_cp):
    os.remove(backendfile_cp)

if os.path.isfile(backendfile):
    copy2(backendfile, backendfile_cp)
backend = emcee.backends.HDFBackend(backendfile_cp)
try:
    if append:
        # Append chain, initial position in param space -> last position.
        p0 = backend.get_chain()[-1, :, :]
    else:
        # Chain is not appendable.
        raise AttributeError

except AttributeError:
    # Reset chain, initial position in param space -> fiducial + noise.
    backend.reset(nwalkers, ndim)
    p0 = [np.array([nr(0.28, 0.05), nr(0.82, 0.05), nr(0.67, 0.05), np.abs(nr(4.2, 0.2))]) for i in range(nwalkers)]
    p0 = np.array(p0)

run_mcmc(p0, pool, backend)

print('done!')

