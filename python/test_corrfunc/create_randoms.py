import os, sys, glob
import numpy as np

'''
Create random catalogs
Necessary to compute correlation function

R.Seppi (MPE)
05.02.2021
'''

# about 2e6 full sky randoms :
size = int(2000000)

uu = np.random.uniform(size=size)
dec = np.arccos(1 - 2 * uu) * 180 / np.pi - 90.
ra = np.random.uniform(size=size) * 2 * np.pi * 180 / np.pi


outdir = os.path.join('/home/rseppi/clustering','randoms')

DATA = np.transpose( [ra ,dec ])
np.savetxt(os.path.join(outdir, 'randoms.txt'), DATA )

print('done!')
