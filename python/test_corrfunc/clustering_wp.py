import sys,os
import numpy as np
import pyccl as ccl
from astropy.table import Table
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import corner
from colossus.cosmology import cosmology
from colossus.lss import bias
from colossus.lss import peaks
import ultranest
from ultranest.plot import PredictionBand
from scipy import integrate

'''
Test code to compute clustering and fits cosmology
Uses eroconda (rseppipy) and pyccl

Needs AXV instructions

R.Seppi (MPE)
04.02.2021

todo: compute directly correlation function
then fit it with the model obtained from power spectrum
'''
#p_2_cat = os.path.join('/home/rseppi/Downloads','MD40_eRO_CLU_b8_CM_0_pixS_20.0.fits')
#p_2_data = os.path.join('/home/rseppi/clustering','correlation_functions','corr_func_3D.txt')
#outdir = os.path.join('/home/rseppi/clustering','figures')
#logdir = os.path.join('/home/rseppi/clustering','my_fit_wp')

p_2_cat = os.path.join('/ptmp/rseppi/catalogs','MD40_eRO_CLU_b8_CM_0_pixS_20.0.fits')
p_2_data = os.path.join('/u/rseppi/clustering','correlation_functions','corr_func_wp.txt')
outdir = os.path.join('/u/rseppi/clustering','figures')
logdir = os.path.join('/u/rseppi/clustering','my_fit_wp')

# Turn off multi-threading to avoid issues with MPIPool.
os.environ["OMP_NUM_THREADS"] = "1"

#Use MultiDark cosmology
Om_MD = 0.307115 #matter
Ob_MD = 0.048206 #baryon
Oc_MD = Om_MD-Ob_MD #cold dark matter
print('Setting up cosmology...')
cosmo_planck18 = ccl.Cosmology(
    Omega_c = 0.11933/0.6766**2, # 0.26066
    Omega_b = 0.02242/0.6766**2, # 0.04897
    h = 0.6766,
    n_s = 0.9665,
    sigma8 = None,
    A_s = np.e**(3.047)/10**10, # 2.1052e-09
    Omega_k = 0.0,
    Omega_g = None,
    Neff = 3.046, # standard model of particle physics
    m_nu = 0.0, # in eV
    m_nu_type = None, # 'inverted', 'normal', 'equal' or 'list'
    w0 = -1.0,
    wa = 0.0,
    T_CMB = ccl.physical_constants.T_CMB, # 2.725
    bcm_log10Mc=14.079181246047625, # BCM baryon correction model on PK https://arxiv.org/abs/1510.06034
    bcm_etab=0.5,                   # BCM
    bcm_ks=55.0,                    # BCM
    mu_0=0.0,                       # modified gravity model parameter
    sigma_0=0.0,                    # modified gravity model parameter
    z_mg=None,                      # modified growth rate parameter
    df_mg=None,                     # modified growth rate parameter
    transfer_function='boltzmann_camb',  # for power spectrum analysis
    matter_power_spectrum='halofit',     # for power spectrum analysis
    baryons_power_spectrum='nobaryons',  # for power spectrum analysis
    mass_function='tinker10',            # for mass function analysis
    halo_concentration='duffy2008',      # for mass function analysis
    emulator_neutrinos='strict',         # for power spectrum analysis
)
cosmo = cosmo_planck18
#cosmo = ccl.Cosmology(Omega_c = Oc_MD, Omega_b = Ob_MD, h = 0.6777, sigma8 = 0.8228, n_s = 0.96)
cosmo_colossus = cosmology.setCosmology('multidark-planck')


#get distribution with redshift
print('Reading table...')
t=Table.read(p_2_cat)
mass_cut = t['HALO_M500c']>1e14
z_cut = (t['redshift_S']>0.1)&(t['redshift_S']<0.3)
t=t[(mass_cut)&(z_cut)]


r, wp_data, DD_cts, DR_cts, RR_cts = np.loadtxt(p_2_data, unpack=True, dtype='float32')
pimax = 30.0

print('Now work on the model')
def model(x, pars):
    '''
    :param x: array. Separation perpendicular to the line of sight in Mpc
    :param pars: cosmological parameters. Available: OmegaM, sigma8, h, b.
    :return: projected correlation function wp(rp). Same dimension of x
    '''
    #Om,s8,h,b = pars
    Om,s8, h = pars
    Oc = Om - Ob_MD
    cosmol = ccl.Cosmology(
        Omega_c = Oc, # 0.26066
        Omega_b = 0.02242/0.6766**2, # 0.04897
        h = h,
        n_s = 0.9665,
        sigma8 = s8,
        Omega_k = 0.0,
        Omega_g = None,
        Neff = 3.046, # standard model of particle physics
        m_nu = 0.0, # in eV
        m_nu_type = None, # 'inverted', 'normal', 'equal' or 'list'
        w0 = -1.0,
        wa = 0.0,
        T_CMB = ccl.physical_constants.T_CMB, # 2.725
        bcm_log10Mc=14.079181246047625, # BCM baryon correction model on PK https://arxiv.org/abs/1510.06034
        bcm_etab=0.5,                   # BCM
        bcm_ks=55.0,                    # BCM
        mu_0=0.0,                       # modified gravity model parameter
        sigma_0=0.0,                    # modified gravity model parameter
        z_mg=None,                      # modified growth rate parameter
        df_mg=None,                     # modified growth rate parameter
        transfer_function='boltzmann_camb',  # for power spectrum analysis
        matter_power_spectrum='halofit',     # for power spectrum analysis
        baryons_power_spectrum='nobaryons',  # for power spectrum analysis
        mass_function='tinker10',            # for mass function analysis
        halo_concentration='duffy2008',      # for mass function analysis
        emulator_neutrinos='strict',         # for power spectrum analysis
    )
    f = ccl.background.growth_factor(cosmol, 1 / (1 + np.average(t['redshift_S'])))
    nu = peaks.peakHeight(np.average(t['HALO_Mvir']), np.average(t['redshift_S']))
    b = bias.haloBiasFromNu(nu, model='comparat17')
    beta = f / b
    xi_model = np.empty((int(pimax), len(x)))
    for jj, pi in enumerate(np.arange(pimax)):
        xi_model[jj] = ccl.correlation_pi_sigma(cosmol, a=1 / (1 + np.average(t['redshift_S'])), beta=beta, pi=pi,
                                                sig=x)
    #in ccl.correlation_pi_sigma beta is used to model redshift space distrotion
    #We still need to rescale the correlation function from large scale to halos.
    xi_model = xi_model * b**2
    wp_model = np.empty(len(x))
    for jj in range(len(wp_model)):
        wp_model[jj] = 2.0 * integrate.simps(xi_model[:, jj], np.arange(pimax))
    return wp_model

#parameters = np.array([Om_MD,0.8228,0.6777,4.2])
parameters = np.array([Om_MD,0.8228,0.6777])#,4.2])
test_wp_model = model(r,parameters)


outpl = os.path.join(outdir,'corr_func_wp.png')
plt.figure(figsize=(5,5))
plt.scatter(r, wp_data, label='MD40')
plt.plot(r, test_wp_model, label='Model MD')
plt.xticks(fontsize=13)
plt.yticks(fontsize=13)
#plt.ylim(top=0.3)
plt.xscale('log')
plt.legend(loc='upper right', fontsize=13)
plt.xlabel(r'$r_p$ (Mpc)',fontsize=13)
plt.ylabel(r'$w_p(r_p)$',fontsize=13)
plt.grid(True)
plt.tight_layout()
plt.savefig(outpl, overwrite=True)

outpl = os.path.join(outdir,'corr_func_wp_loglog.png')
plt.figure(figsize=(5,5))
plt.scatter(r, wp_data, label='MD40')
plt.plot(r, test_wp_model, label='Model MD')
plt.xticks(fontsize=13)
plt.yticks(fontsize=13)
#plt.ylim(top=0.3)
plt.yscale('log')
plt.xscale('log')
plt.legend(loc='upper right', fontsize=13)
plt.xlabel(r'$r_p$ (Mpc)',fontsize=13)
plt.ylabel(r'$w_p(r_p)$',fontsize=13)
plt.grid(True)
plt.tight_layout()
plt.savefig(outpl, overwrite=True)



# C_ell^2/(Area of erass survey*ell*delta ell)
#cov = np.diag(cls_clu**2)*2*np.pi/((4*np.pi*18000)*(np.pi/180)**2*ell*delta_ell) 
#cov = np.diag((0.1*wtheta)**2)
sel = ~np.isnan(wp_data)
#frac_err = 1./(np.sqrt(DD_cts[sel]))
frac_err = 0.2
d_wp = frac_err*wp_data[sel]
cov = np.diag((d_wp)**2)
icov = np.linalg.inv(cov) #inverse of cov



#define Likelihood - fits Omegac sigma8

def lnprior(cube):
    pars = cube.copy()
    pars[0] = (cube[0])*0.2 + 0.2  #OmegaM = 0.2 0.4
    pars[1] = (cube[1])*0.6 + 0.5  #sigma8 = 0.5 1.1
    pars[2] = (cube[2])*0.6 + 0.5  #h = 0.5 0.9
    return pars

def lnlike(pars):
    diff = wp_data[sel]-model(r[sel], pars)
    return -np.dot(diff,np.dot(icov,diff))/2.0


names = [r'$\Omega_{M}$', r'$\sigma_8$', 'h'] #, 'b']
sampler = ultranest.ReactiveNestedSampler(param_names=names, loglike=lnlike, transform=lnprior, log_dir=logdir, resume=True)
result = sampler.run(min_num_live_points=400)

sampler.print_results()

outpl = os.path.join(outdir, 'corr_func_wp_final.png')
plt.figure(figsize=(5., 5.))
plt.errorbar(r[sel], wp_data[sel], yerr=d_wp, fmt=".C0", capsize=0)

r_grid = np.logspace(np.log10(4), np.log10(100), 50)
band = PredictionBand(r_grid)
for Om_, s8_, h_ in sampler.results['samples']:
    # compute for each time the y value
    band.add(model(r_grid, [Om_, s8_, h_]))#, h]))

band.line(color='k', label='best fit')
# add 1 sigma quantile
band.shade(color='k', alpha=0.3, label=r'1$\sigma$')
# add 2 sigma quantile
band.shade(q=0.477, color='gray', alpha=0.2, label=r'2$\sigma$')
plt.xticks(fontsize=13)
plt.yticks(fontsize=13)
plt.yscale('log')
plt.xscale('log')
plt.legend(fontsize=13)
plt.xlabel(r'$r_p$ (Mpc)',fontsize=13)
plt.ylabel(r'$w_p(r_p)$',fontsize=13)
plt.grid(True)
plt.tight_layout()
plt.savefig(outpl, overwrite=True)

outpl = os.path.join(outdir, 'corner_wp.png')
parameters = np.array(result['weighted_samples']['points'])
weights = np.array(result['weighted_samples']['weights'])
weights /= weights.sum()
cumsumweights = np.cumsum(weights)
mask = cumsumweights > 1e-4

# note about sigmas: 1sigma in 2d contains 1-e^-0.5 (39%, not 68%)
# note about sigmas: 2sigma in 2d contains 1-e^-2 (86%, not 95%)
fig=corner.corner(parameters[mask,:], weights=weights[mask], labels=names, show_titles=True, color='r',bins=50,smooth=True,
                  smooth1d=True, quantiles=[0.025,0.16,0.84,0.975], label_kwargs={'fontsize':15,'labelpad':20},
                  title_kwargs={"fontsize":15},levels=[0.393, 0.865], fill_contours=True,title_fmt='.3f')


fig.tight_layout()
fig.savefig(outpl, overwrite=True)
sampler.plot()

print('done!')

sys.exit()


p = '/home/rseppi/clustering/correlation_functions/Zmin_0.1_Zmax_0.3.3dps.2pcf'
data = np.loadtxt(p)
pi,sigma,xi = data.T[0],data.T[1],data.T[2]
wp = np.empty(len(pi))
for jj in range(len(wp)):
    wp[jj] = 2.0 * integrate.simps(xi_[jj, :], pi)
