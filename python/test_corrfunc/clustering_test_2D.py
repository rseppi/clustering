import sys,os
import numpy as np
import pyccl as ccl
from astropy.table import Table
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import emcee
import corner
from colossus.cosmology import cosmology
from colossus.lss import bias
from colossus.lss import peaks
from schwimmbad import MPIPool
from shutil import copy2

'''
Test code to compute clustering and fits cosmology
Uses eroconda (rseppipy) and pyccl

Needs AXV instructions

R.Seppi (MPE)
04.02.2021

todo: compute directly correlation function
then fit it with the model obtained from power spectrum
'''
p_2_cat = os.path.join('/home/rseppi/Downloads','MD40_eRO_CLU_b8_CM_0_pixS_20.0.fits')
p_2_data = os.path.join('/home/rseppi/clustering','correlation_functions','corr_func_2D.txt')
outdir = os.path.join('/home/rseppi/clustering','figures')
backendfile = os.path.join('/home/rseppi/clustering','logs','backend_2D.h5')
backendfile_cp = os.path.join('/home/rseppi/clustering','logs','backend_2D_copy.h5')

#p_2_cat = os.path.join('/ptmp/rseppi/catalogs','MD40_eRO_CLU_b8_CM_0_pixS_20.0.fits')
#p_2_data = os.path.join('/u/rseppi/clustering','correlation_functions','corr_func_2D.txt')
#outdir = os.path.join('/u/rseppi/clustering','figures')
#backendfile = os.path.join('/u/rseppi/clustering','logs','backend_2D.h5')
#backendfile_cp = os.path.join('/u/rseppi/clustering','logs','backend_2D_cp.h5')

# Turn off multi-threading to avoid issues with MPIPool.
os.environ["OMP_NUM_THREADS"] = "1"

#Use MultiDark cosmology
Om_MD = 0.307115 #matter
Ob_MD = 0.048206 #baryon
Oc_MD = Om_MD-Ob_MD #cold dark matter
cosmo_planck18 = ccl.Cosmology(
    Omega_c = 0.11933/0.6766**2, # 0.26066
    Omega_b = 0.02242/0.6766**2, # 0.04897
    h = 0.6766,
    n_s = 0.9665,
    sigma8 = None,
    A_s = np.e**(3.047)/10**10, # 2.1052e-09
    Omega_k = 0.0,
    Omega_g = None,
    Neff = 3.046, # standard model of particle physics
    m_nu = 0.0, # in eV
    m_nu_type = None, # 'inverted', 'normal', 'equal' or 'list'
    w0 = -1.0,
    wa = 0.0,
    T_CMB = ccl.physical_constants.T_CMB, # 2.725
    bcm_log10Mc=14.079181246047625, # BCM baryon correction model on PK https://arxiv.org/abs/1510.06034
    bcm_etab=0.5,                   # BCM
    bcm_ks=55.0,                    # BCM
    mu_0=0.0,                       # modified gravity model parameter
    sigma_0=0.0,                    # modified gravity model parameter
    z_mg=None,                      # modified growth rate parameter
    df_mg=None,                     # modified growth rate parameter
    transfer_function='boltzmann_camb',  # for power spectrum analysis
    matter_power_spectrum='halofit',     # for power spectrum analysis
    baryons_power_spectrum='nobaryons',  # for power spectrum analysis
    mass_function='tinker10',            # for mass function analysis
    halo_concentration='duffy2008',      # for mass function analysis
    emulator_neutrinos='strict',         # for power spectrum analysis
)
cosmo = cosmo_planck18
#cosmo = ccl.Cosmology(Omega_c = Oc_MD, Omega_b = Ob_MD, h = 0.6777, sigma8 = 0.8228, n_s = 0.96)
cosmo_colossus = cosmology.setCosmology('multidark-planck')


#get distribution with redshift
t=Table.read(p_2_cat)
mass_cut = t['HALO_M500c']>1e14
z_cut = (t['redshift_S']>0.1)&(t['redshift_S']<0.8)
t=t[(mass_cut)&(z_cut)]

Nz, zbins = np.histogram(t['redshift_R'], bins=50)
z = (zbins[1:]+zbins[:-1])/2.

theta, wtheta, DD_cts, DR_cts, RR_cts = np.loadtxt(p_2_data, unpack=True, dtype='float32')

print('Now work on the model')
def model(x, pars):
    '''
    :param x: array. Angles in degrees
    :param pars: cosmological parameters. Available: OmegaM, sigma8, h, b.
    :return: 2D correlation function w(theta). Same dimension of x
    '''
    #Om,s8,h,b = pars
    Om,s8,h = pars
    Oc = Om - Ob_MD
    nu = peaks.peakHeight(np.average(t['HALO_Mvir']), np.average(t['redshift_S']))
    b = bias.haloBiasFromNu(nu, model='comparat17')
    b = np.tile(b, len(z))
    cosmol = ccl.Cosmology(
        Omega_c = Oc, # 0.26066
        Omega_b = 0.02242/0.6766**2, # 0.04897
        h = h,
        n_s = 0.9665,
        sigma8 = s8,
        Omega_k = 0.0,
        Omega_g = None,
        Neff = 3.046, # standard model of particle physics
        m_nu = 0.0, # in eV
        m_nu_type = None, # 'inverted', 'normal', 'equal' or 'list'
        w0 = -1.0,
        wa = 0.0,
        T_CMB = ccl.physical_constants.T_CMB, # 2.725
        bcm_log10Mc=14.079181246047625, # BCM baryon correction model on PK https://arxiv.org/abs/1510.06034
        bcm_etab=0.5,                   # BCM
        bcm_ks=55.0,                    # BCM
        mu_0=0.0,                       # modified gravity model parameter
        sigma_0=0.0,                    # modified gravity model parameter
        z_mg=None,                      # modified growth rate parameter
        df_mg=None,                     # modified growth rate parameter
        transfer_function='boltzmann_camb',  # for power spectrum analysis
        matter_power_spectrum='halofit',     # for power spectrum analysis
        baryons_power_spectrum='nobaryons',  # for power spectrum analysis
        mass_function='tinker10',            # for mass function analysis
        halo_concentration='duffy2008',      # for mass function analysis
        emulator_neutrinos='strict',         # for power spectrum analysis
    )
    #cosmol = ccl.Cosmology(Omega_c = Oc, Omega_b = Ob_MD, h = h, sigma8 = s8, n_s = 0.96)
    tracer = ccl.NumberCountsTracer(cosmol, has_rsd=True, dndz=(z,Nz), bias=(z,b))
    ell = 180. / x
    ell = np.sort(ell)
    cl = ccl.angular_cl(cosmol, tracer, tracer, ell)  # Clustering: angular power spectrum
    wtheta_model = ccl.correlation(cosmol, ell, cl, x, type='NN', method='FFTLog')
    return wtheta_model

nu = peaks.peakHeight(np.average(t['HALO_Mvir']), np.average(t['redshift_S']))
b = bias.haloBiasFromNu(nu, model='comparat17')
parameters = np.array([Om_MD,0.8228,0.6777,4.2])
parameters = np.array([Om_MD,0.8228,0.6777])#,4.2])
test_model = model(theta, parameters)


outpl = os.path.join(outdir,'corr_func_2D.png')
plt.figure(figsize=(5,5))
plt.scatter(theta, wtheta, label='MD40')
plt.plot(theta, test_model, label='Model MD')
plt.xticks(fontsize=13)
plt.yticks(fontsize=13)
#plt.ylim(top=0.3)
plt.xscale('log')
plt.legend(loc='upper right', fontsize=13)
plt.xlabel(r'$\theta$ (deg)',fontsize=13)
plt.ylabel(r'$w(\theta)$',fontsize=13)
plt.grid(True)
plt.tight_layout()
plt.savefig(outpl, overwrite=True)

outpl = os.path.join(outdir,'corr_func_2D_loglog.png')
plt.figure(figsize=(5,5))
plt.scatter(theta, wtheta, label='MD40')
plt.plot(theta, test_model, label='Model MD')
plt.xticks(fontsize=13)
plt.yticks(fontsize=13)
#plt.ylim(top=0.3)
plt.yscale('log')
plt.xscale('log')
plt.legend(loc='upper right', fontsize=13)
plt.xlabel(r'$\theta$ (deg)',fontsize=13)
plt.ylabel(r'$w(\theta)$',fontsize=13)
plt.grid(True)
plt.tight_layout()
plt.savefig(outpl, overwrite=True)



# C_ell^2/(Area of erass survey*ell*delta ell)
#cov = np.diag(cls_clu**2)*2*np.pi/((4*np.pi*18000)*(np.pi/180)**2*ell*delta_ell) 
#cov = np.diag((0.1*wtheta)**2)
sel = ~np.isnan(wtheta)
#frac_err = 1./(np.sqrt(DD_cts[sel]))
frac_err = 0.2
d_wtheta = frac_err*wtheta[sel]
cov = np.diag((d_wtheta)**2)
print(cov)
icov = np.linalg.inv(cov) #inverse of cov



#define Likelihood - fits Omegac sigma8

def lnprior(pars):
    Om, s8, h = pars
#    Om, s8, h, b = pars
    #if 0.1<Om<0.6 and 0.3<s8<1.3 and 0.3<h<1.1 and 1.0<b<7.0:
    if 0.1 < Om < 0.6 and 0.3 < s8 < 1.3 and 0.3 < h < 1.1:# and 1.0 < b < 7.0:
        return 0.0
    else:
        return -np.inf

def lnlike(pars, x, y):#, yerr):
    diff = y-model(x, pars)
    #return -0.5*(np.sum(diff)**2/yerr**2 +np.log(yerr**2))
    return -np.dot(diff,np.dot(icov,diff))/2.0

def lnprob(pars):
    lp = lnprior(pars)
    if not np.isfinite(lp):
        return -np.inf
    return lp + lnlike(pars, theta[sel], wtheta[sel])


ndim=3  #4 #Omegac sigma8 h (b)

#start from 0.29 0.83 0.7 3
nwalkers = 10#25*ndim #100
nsteps = 50 #1000
fburn = 0.25
def nr(loc,var):
    return np.random.normal(loc,var)

def make_plots(sampler):
    '''
    Function that makes fitting plots.
    Chains as a function of number of steps
    Corner plot with OmegaM, sigma8, h, bias
    Best fit model with 100 additional models picked randomly from the burnt-in chain.

    :param sampler: an emcee sampler
    '''
    names = [r'$\Omega_{M}$', r'$\sigma_8$', 'h', 'b']
    names = [r'$\Omega_{M}$', r'$\sigma_8$', 'h'] #, 'b']
    samples = sampler.get_chain()

    fig,axes = plt.subplots(ndim,1,figsize=(10,6))
    outpl = os.path.join(outdir, 'chains_2D.png')
    for i in range(ndim):
        ax = axes[i]
        ax.plot(samples[:, :, i], "k", alpha=0.3)
        ax.set_xlim(0, len(samples))
        ax.set_ylabel(names[i])
    axes[-1].set_xlabel("step number")
    fig.savefig(outpl, overwrite=True)

    flat_samples = sampler.get_chain(discard=int(fburn*nsteps), flat=True) #400
    popt = np.percentile(np.array(flat_samples),50,axis=0)
    outpl = os.path.join(outdir, 'corner_2D.png')
    #note about sigmas: 1sigma in 2d contains 1-e^-0.5 (39%, not 68%)
    #note about sigmas: 2sigma in 2d contains 1-e^-2 (86%, not 95%)
    fig = corner.corner(flat_samples, labels=names, truths=popt,show_titles=True,color='r',bins=50,smooth=True,smooth1d=True,quantiles=[0.025,0.16,0.84,0.975],label_kwargs={'fontsize':20,'labelpad':20},title_kwargs={"fontsize":17},levels=[0.393,0.865],fill_contours=True,title_fmt='.3f')
    fig.tight_layout()
    fig.savefig(outpl, overwrite=True)

    outpl = os.path.join(outdir,'corr_func_2d_final.png')
    plt.figure(figsize=(5.,5.))
    inds = np.random.randint(len(flat_samples), size=100)
    for ind in inds:
        sample = flat_samples[ind]
        plt.plot(theta, model(theta,sample), "C1", alpha=0.1)
    plt.errorbar(theta[sel], wtheta[sel], yerr=d_wtheta, fmt=".C0", capsize=0)
    plt.plot(theta, model(theta,popt), "C2", label="truth")
    plt.xticks(fontsize=13)
    plt.yticks(fontsize=13)
    plt.ylim(1e-3)
    plt.yscale('log')
    plt.xscale('log')
    plt.legend(fontsize=13)
    plt.xlabel(r'$\theta$ (deg)',fontsize=13)
    plt.ylabel(r'$w(\theta)$',fontsize=13)
    plt.grid(True)
    plt.tight_layout()
    plt.savefig(outpl, overwrite=True)


def run_mcmc(p0, pool, backend):
    '''
    Function to run mcmc sampling
    :param p0: starting points in the parameter space
    :param pool: object needed for mpi parallelization
    :param backend: class used to append to existing chains
    '''
    sampler = emcee.EnsembleSampler(nwalkers, ndim, lnprob, pool=pool, backend=backend)

    #if the max iterations have already been done, exit
    if sampler.iteration >= nsteps:
        return

    # We'll track how the average autocorrelation time estimate changes
    index, autocorr, old_tau = 0, np.empty(nsteps), np.inf

    # Now we'll sample for up to steps
    #The mcmc breaks if the chain is longer than 100correlation time
    #or the relative change in autocorr is < 1%
    for _ in sampler.sample(p0, iterations=nsteps, progress=True):
        # Only check convergence every 100 steps
        if sampler.iteration % 100:
            continue

        # Save backend every 100 steps to safely avoid corruption.
        if os.path.isfile(backendfile):
            os.remove(backendfile)
        copy2(backendfile_cp, backendfile)
        # Compute the autocorrelation time so far
        # Using tol=0 means that we'll always get an estimate even
        # if it isn't trustworthy
        tau = sampler.get_autocorr_time(tol=0)
        autocorr[index] = np.mean(tau)
        index += 1

        # Check convergence
        converged = np.all(tau * 100 < sampler.iteration)
        converged &= np.all(np.abs(old_tau - tau) / tau < 0.01)
        if converged or sampler.iteration>nsteps:
            break
        old_tau = tau
    make_plots(sampler)

#set it up for MPI
mpi = True  #these will have to be from keyboard or command line
append = True

if mpi:
    pool = MPIPool()
    if not pool.is_master():
        pool.wait()
        sys.exit(0)
else:
    pool = None

#setup backend --> this has to be figured out
if os.path.isfile(backendfile_cp):
    os.remove(backendfile_cp)

if os.path.isfile(backendfile):
    copy2(backendfile, backendfile_cp)


backend = emcee.backends.HDFBackend(backendfile_cp)
try:
    if append:
        # Append chain, initial position in param space -> last position.
        p0 = backend.get_chain()[-1, :, :]
    else:
        # Chain is not appendable.
        raise AttributeError

except AttributeError:
    # Reset chain, initial position in param space -> fiducial + noise.
    backend.reset(nwalkers, ndim)
    #p0 = [np.array([nr(0.28, 0.03), nr(0.82, 0.03), nr(0.67, 0.03), nr(4.2, 0.2)]) for i in range(nwalkers)]
    p0 = [np.array([nr(0.28, 0.03), nr(0.82, 0.03), nr(0.67, 0.03)]) for i in range(nwalkers)]
    p0 = np.array(p0)


run_mcmc(p0, pool, backend)
if isinstance(pool, MPIPool):
    pool.close()
#sampler.run_mcmc(p0, nsteps, progress=True)

print('done!')

