import os, sys, glob
import numpy as np
from astropy.table import Table, Column
from astropy.io import ascii
import random

'''
Create random catalogs
Necessary to compute correlation function

R.Seppi (MPE)
05.02.2021
'''

p_2_cat = os.path.join('/data37s/simulation_1/MD/MD_4.0Gpc','MD40_eRO_CLU_b8_CM_0_pixS_20.0.fits')
p_2_cat = os.path.join('/home/rseppi/Downloads','MD40_eRO_CLU_b8_CM_0_pixS_20.0.fits')

print('Reading HMD...')
t = Table.read(p_2_cat)
#cut at M500c 1e14 (to apply /h) #cut z <0.8 >0.1
z_cut = (t['redshift_S']>0.1)&(t['redshift_S']<0.3)
mass_cut = t['HALO_M500c']>1e14
t=t[(mass_cut)&(z_cut)]        # redshift_S in (0.1,0.8) and HALO_M500c > 1e14 ---> 233477 halos

# take N random = 10*N_data :
N=10
size = N*len(t)
print(size)
uu = np.random.uniform(size=size)
dec = np.arccos(1 - 2 * uu) * 180 / np.pi - 90.
ra = np.random.uniform(size=size) * 2 * np.pi * 180 / np.pi
z = np.tile(t['redshift_S'],N)
print(z)
random.shuffle(z)

outdir = os.path.join('/home/rseppi/clustering','randoms')

DATA = np.transpose( [ra, dec, z])
np.savetxt(os.path.join(outdir, 'randoms_3D.txt'), DATA )

tab = Table()
tab.add_column(Column(data=ra, name='RA'))
tab.add_column(Column(data=dec, name='DEC'))
tab.add_column(Column(data=z, name='z'))
tab.write(os.path.join(outdir, 'randoms_3D.fits'), overwrite=True)
print('done!')
