import sys,os,glob
import numpy as np
import pyccl as ccl
from astropy.table import Table
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import corner
from colossus.cosmology import cosmology
from colossus.lss import bias
import ultranest
from ultranest.plot import PredictionBand
from scipy import integrate

'''
Takes N from keyboard

N=multiplicative factor to rescale the covariance matrix to increase/decrease signal to noise
'''

N = sys.argv[1]

#logdir = os.path.join('/home/rseppi/clustering','python','test0','my_fit_wp')
#outdir = os.path.join('/home/rseppi/clustering','python','test0')
logdir = os.path.join('/u/rseppi/clustering','python','test0','my_fit_wp_N'+N)
outdir = os.path.join('/u/rseppi/clustering','python','test0')


#Use MultiDark cosmology
Om_MD = 0.307115 #matter
Ob_MD = 0.048206 #baryon
Oc_MD = Om_MD-Ob_MD #cold dark matter
h_MD = 0.6766
print('Setting up cosmology...')
cosmo_planck18 = ccl.Cosmology(
    Omega_c = 0.11933/0.6766**2, # 0.26066
    Omega_b = 0.02242/0.6766**2, # 0.04897
    h = h_MD,
    n_s = 0.9665,
    sigma8 = 0.8228,
    #A_s = np.e**(3.047)/10**10, # 2.1052e-09
    Omega_k = 0.0,
    Omega_g = None,
    Neff = 3.04, # standard model of particle physics
    m_nu = 0.0, # in eV
    m_nu_type = None, # 'inverted', 'normal', 'equal' or 'list'
    w0 = -1.0,
    wa = 0.0,
    T_CMB = ccl.physical_constants.T_CMB, # 2.725
    bcm_log10Mc=14.079181246047625, # BCM baryon correction model on PK https://arxiv.org/abs/1510.06034
    bcm_etab=0.5,                   # BCM
    bcm_ks=55.0,                    # BCM
    mu_0=0.0,                       # modified gravity model parameter
    sigma_0=0.0,                    # modified gravity model parameter
    z_mg=None,                      # modified growth rate parameter
    df_mg=None,                     # modified growth rate parameter
    transfer_function='boltzmann_camb',  # for power spectrum analysis
    matter_power_spectrum='halofit',     # for power spectrum analysis
    baryons_power_spectrum='nobaryons',  # for power spectrum analysis
    mass_function='tinker10',            # for mass function analysis
    halo_concentration='duffy2008',      # for mass function analysis
    emulator_neutrinos='strict',         # for power spectrum analysis
)
cosmo = cosmo_planck18

z_mid = np.array([0.2,0.4,0.6])
#halo_mass = np.array([1.827e14,1.7415e14,1.6663e14]) #Msun/h #cutting M500c>1e14
halo_mass = np.array([1.05872e14,1.0004e14,9.43983e13]) #Msun/h #no cut
pi = np.arange(0.5,60,1.0)
rp = np.array([ 0.2128236,  0.24099  ,  0.2728843,  0.3089996,  0.3498946,
        0.396202 ,  0.448638 ,  0.5080138,  0.5752477,  0.6513798,
        0.7375878,  0.835205 ,  0.9457416,  1.070907 ,  1.212638 ,
        1.373127 ,  1.554856 ,  1.760636 ,  1.99365  ,  2.257503 ,
        2.556276 ,  2.89459  ,  3.27768  ,  3.71147  ,  4.20267  ,
        4.758879 ,  5.388701 ,  6.101878 ,  6.909441 ,  7.823882 ,
        8.859347 , 10.03185  , 11.35953  , 12.86293  , 14.5653   ,
       16.49296  , 18.67575  , 21.14743  , 23.94622  , 27.11542  ,
       30.70405  , 34.76763  , 39.36901  , 44.57937  , 50.4793   ,
       57.16006  , 64.72501  , 73.29114  , 82.99098  , 93.97456  ])
wp_fake = []
cov_list = []
for (z,mass) in zip(z_mid,halo_mass):
    cosmopars = {'flat': True, 'H0': 0.6766 * 100, 'Om0': Om_MD, 'Ob0': 0.02242 / 0.6766 ** 2, 'sigma8': 0.8228, 'ns': 0.9665}
    cosmo_colossus = cosmology.setCosmology('newcosmo', cosmopars)
    f = ccl.background.growth_rate(cosmo, a=1/(1+z))
    b = bias.haloBias(mass, model='comparat17', z=z, mdef='500c')
    beta = f / b
    xi_model = np.empty((len(pi), len(rp)))
    for jj, pi_ in enumerate(pi):
        xi_model[jj] = ccl.correlation_pi_sigma(cosmo, a=1/(1+z), beta=beta, pi=pi_/h_MD,sig=rp/h_MD)
    #in ccl.correlation_pi_sigma beta is used to model redshift space distrotion
    #We still need to rescale the correlation function from large scale to halos.
    xi_model = xi_model * b**2
    wp_model = np.empty(len(rp))
    for jj in range(len(wp_model)):
        wp_model[jj] = 2.0*integrate.simps(xi_model[:, jj], pi)
    rp_wp_model = rp * wp_model
    wp_fake.append(rp_wp_model)

    p_2_cov_mat = os.path.join('/u/rseppi/clustering', 'correlation_functions', 'covariance_matrix',
                               'covariance_matrix_rp_wprp_Zmin_' + str(np.round(z-0.1, 1)) + '_Zmax_' + str(
                                   np.round(z+0.1, 1)) + '.txt')

    cov_mat = np.loadtxt(p_2_cov_mat,unpack=True)
    cov_mat = float(N)*cov_mat
    cov_list.append(cov_mat)



def model(x_rp, x_pi, z, mass, pars):
    '''
    :param x: array. Separation in Mpc/h
    :param pars: cosmological parameters. Available: OmegaM, sigma8, h, b.
    :return: 3D correlation function xi(r). Same dimension of x
    '''
    #Om,s8,h,b = pars
    Om,s8, h = pars
    Oc = Om - Ob_MD
    cosmol = ccl.Cosmology(
        Omega_c = Oc, # 0.26066
        Omega_b = 0.02242/0.6766**2, # 0.04897
        h = h,
        n_s = 0.9665,
        sigma8 = s8,
        Omega_k = 0.0,
        Omega_g = None,
        Neff = 3.04, # standard model of particle physics
        m_nu = 0.0, # in eV
        m_nu_type = None, # 'inverted', 'normal', 'equal' or 'list'
        w0 = -1.0,
        wa = 0.0,
        T_CMB = ccl.physical_constants.T_CMB, # 2.725
        bcm_log10Mc=14.079181246047625, # BCM baryon correction model on PK https://arxiv.org/abs/1510.06034
        bcm_etab=0.5,                   # BCM
        bcm_ks=55.0,                    # BCM
        mu_0=0.0,                       # modified gravity model parameter
        sigma_0=0.0,                    # modified gravity model parameter
        z_mg=None,                      # modified growth rate parameter
        df_mg=None,                     # modified growth rate parameter
        transfer_function='boltzmann_camb',  # for power spectrum analysis
        matter_power_spectrum='halofit',     # for power spectrum analysis
        baryons_power_spectrum='nobaryons',  # for power spectrum analysis
        mass_function='tinker10',            # for mass function analysis
        halo_concentration='duffy2008',      # for mass function analysis
        emulator_neutrinos='strict',         # for power spectrum analysis
    )

    cosmopars = {'flat': True, 'H0': h * 100, 'Om0': Om, 'Ob0': 0.02242 / 0.6766 ** 2, 'sigma8': s8, 'ns': 0.9665}
    cosmol_colossus = cosmology.setCosmology('newcosmo', cosmopars)

    a = 1 / (1 + z)
    f = ccl.background.growth_rate(cosmol, a)
    b = bias.haloBias(mass, model='comparat17', z=z, mdef='500c')
    beta = f / b
    xi_model = np.empty((len(x_pi), len(x_rp)))
    for jj, pi_ in enumerate(x_pi):
        xi_model[jj] = ccl.correlation_pi_sigma(cosmol, a=a, beta=beta, pi=pi_/h,sig=x_rp/h)
    #in ccl.correlation_pi_sigma beta is used to model redshift space distrotion
    #We still need to rescale the correlation function from large scale to halos.
    xi_model = xi_model * b**2
    wp_model = np.empty(len(x_rp))
    for jj in range(len(wp_model)):
        wp_model[jj] = 2.0*integrate.simps(xi_model[:, jj], x_pi)
    rp_wp_model = x_rp * wp_model
    return rp_wp_model



icov = []
sel=[]
dwp_list = []
for wp_rp, covar in zip(wp_fake, cov_list):
    sel_ = ~np.isnan(wp_rp) & (rp > 5) & (rp < 90)
    sel.append(sel_)
    cov = covar[sel_][:,sel_]
    icov_ = np.linalg.inv(cov) #inverse of cov
    icov.append(icov_)
    dwp_list.append(np.sqrt(np.diag(cov)))

#for wp, s, dwp, mass, z in zip(wp_fake, sel, dwp_list, halo_mass, z_mid):
#    plt.errorbar(rp[s],wp[s],yerr=dwp, fmt=".", capsize=0)
#    plt.plot(rp,model(rp,pi,z,mass,[0.31,0.82,0.67]))

#plt.show()

def lnprior(cube):
    pars = cube.copy()
    pars[0] = (cube[0])*0.45 + 0.15  #OmegaM = 0.15 0.4
    pars[1] = (cube[1])*0.6 + 0.5  #sigma8 = 0.5 1.5
    pars[2] = (cube[2])*0.6 + 0.4  #h = 0.4 1.4
    #pars[3] = (cube[3])*5.5 + 0.5  #h = 0.5 5.5
    return pars

def lnlike(pars):
    likelihood = 0
    for w,sele,ic,z,mass in zip(wp_fake, sel, icov, z_mid,halo_mass):
        try:
            diff = w[sele]-model(rp[sele], pi, z, mass, pars)
            likelihood += -np.dot(diff,np.dot(ic,diff))/2.0
        except:
            likelihood = -1e20
    return likelihood

names = [r'$\Omega_{M}$', r'$\sigma_8$', 'h']#, 'b']
print('initialize sampler...')
sampler = ultranest.ReactiveNestedSampler(param_names=names, loglike=lnlike, transform=lnprior, log_dir=logdir, resume=True)
print('run sampler...')
result = sampler.run(min_num_live_points=400)

print('print results...')
sampler.print_results()
print('plot sampler...')
sampler.plot()

popt = np.array(result['posterior']['mean'])
pvar = np.array(result['posterior']['stdev'])

fil=open(os.path.join(outdir,'values_wp.txt'),'a+')
#fil.write('OmegaM sigma8 h deltaOmegaM deltasigma8 deltah\n')
fil.write('\n{:.2f} {:.3f} {:.3f} {:.3f} {:.3f} {:.3f} {:.3f}\n'.format(float(N),popt[0],popt[1],popt[2], pvar[0],pvar[1],pvar[2]))
fil.close()

outpl = os.path.join(outdir, 'corr_func_wp_final_N'+N+'.png')
colors = ['C0', 'C1', 'C2', 'C3']


fig3 = plt.figure(figsize=(6,6))
gs3 = fig3.add_gridspec(4,1,hspace=0.0)
ax31 = fig3.add_subplot(gs3[0:3, :])

for wp_rp, s, dwp, z,mass, col in zip(wp_fake,sel, dwp_list, z_mid, halo_mass, colors):
    ax31.errorbar(rp[s], wp_rp[s], yerr=dwp, fmt=".", capsize=0, label='z={:.1f}'.format(z))
    ax31.plot(rp[s],model(rp[s],pi,z,mass,popt),color=col)
    #rp_grid = np.logspace(np.log10(5), np.log10(90), 200)
    #band = PredictionBand(rp_grid)
    #for Om_, s8_, h_ in sampler.results['samples']:
    # compute for each time the y value
    #    band.add(model(rp_grid,pi, z, mass, [Om_, s8_, h_]))#, b_]))

    #band.line(color=col)
    # add 1 sigma quantile
    #band.shade(alpha=0.4, color=col)#label=r'1$\sigma$')
    # add 2 sigma quantile
    #band.shade(q=0.477, alpha=0.2, color=col)#label=r'2$\sigma$')

ax32 = fig3.add_subplot(gs3[3, :], sharex=ax31)
for wp_rp, s, dwp, z,mass, col in zip(wp_fake,sel, dwp_list, z_mid, halo_mass, colors):
    ax32.errorbar(rp[s], wp_rp[s]/model(rp[s],pi,z,mass,popt), yerr=dwp/model(rp[s],pi,z,mass,popt), fmt=".", capsize=0, label='z={:.1f}'.format(z))

ax32.hlines(1,min(rp),max(rp), color='black')

ax31.tick_params(labelsize=13)
ax32.tick_params(labelsize=13)
ax32.set_ylim(0.3,1.7)
ax31.set_xlim(5,90)
ax32.set_xlim(5,90)
#plt.ylim(0,1300)
ax31.set_xscale('log')
ax32.set_xscale('log')
ax31.legend(fontsize=13)
ax32.set_xlabel(r'$r_p$ (Mpc/h)',fontsize=13)
ax31.set_ylabel(r'$r_p w_p(r_p)$',fontsize=13)
ax32.set_ylabel('Data/Model',fontsize=13)
ax31.grid(True)
ax32.grid(True)
fig3.tight_layout()
fig3.savefig(outpl, overwrite=True)

print('done!')