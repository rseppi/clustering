import os,glob,sys
import numpy as np
import matplotlib.pyplot as plt

'''
Reads the txt files created after 000 with cosmology params and scaling of the cov matrix
Makes plots
'''

p_2_wp = os.path.join('/u/rseppi/clustering/python/test0','values_wp.txt')
p_2_2d = os.path.join('/u/rseppi/clustering/python/test0','values_2D.txt')
p_2_3d = os.path.join('/u/rseppi/clustering/python/test0','values_3D.txt')

p_2_out = os.path.join('/u/rseppi/clustering/python/test0')

data_wp = np.loadtxt(p_2_wp,unpack=True)
data_2d = np.loadtxt(p_2_2d, unpack=True)
data_3d = np.loadtxt(p_2_3d, unpack=True)

OmegaM_planck = 0.307115
sigma8_planck = 0.8228
h_planck = 0.6766
N_erass8 = np.sqrt(0.98e6/1e5)
N_erass1 = np.sqrt(0.98e6/1e4)

N_wp, OmegaM_wp, sigma8_wp, h_wp, OmegaM_err_wp, sigma8_err_wp, h_err_wp = np.loadtxt(p_2_wp,unpack=True)
N_wp, index_wp = np.unique(N_wp,return_index=True)
OmegaM_wp, sigma8_wp, h_wp = OmegaM_wp[index_wp], sigma8_wp[index_wp], h_wp[index_wp]
OmegaM_err_wp, sigma8_err_wp, h_err_wp = OmegaM_err_wp[index_wp], sigma8_err_wp[index_wp], h_err_wp[index_wp]


N_3d, OmegaM_3d, sigma8_3d, h_3d, OmegaM_err_3d, sigma8_err_3d, h_err_3d = np.loadtxt(p_2_3d,unpack=True)
N_3d, index_3d = np.unique(N_3d,return_index=True)
OmegaM_3d, sigma8_3d, h_3d = OmegaM_3d[index_3d], sigma8_3d[index_3d], h_3d[index_3d]
OmegaM_err_3d, sigma8_err_3d, h_err_3d = OmegaM_err_3d[index_3d], sigma8_err_3d[index_3d], h_err_3d[index_3d]

N_2d, OmegaM_2d, sigma8_2d, h_2d, OmegaM_err_2d, sigma8_err_2d, h_err_2d = np.loadtxt(p_2_2d,unpack=True)
N_2d, index_2d = np.unique(N_2d,return_index=True)
OmegaM_2d, sigma8_2d, h_2d = OmegaM_2d[index_2d], sigma8_2d[index_2d], h_2d[index_2d]
OmegaM_err_2d, sigma8_err_2d, h_err_2d = OmegaM_err_2d[index_2d], sigma8_err_2d[index_2d], h_err_2d[index_2d]

outpl = os.path.join(p_2_out,'all_cosmo_trend.png')
fig = plt.figure(figsize=(6,6))
gs = fig.add_gridspec(3,1,hspace=0.0)
ax1 = fig.add_subplot(gs[0, :])
ax1.errorbar(N_2d-0.05*N_2d,OmegaM_2d,yerr=OmegaM_err_2d, capsize=0, fmt='.',label=r'w($\theta$)',color='C0')
ax1.errorbar(N_wp,OmegaM_wp,yerr=OmegaM_err_wp, capsize=0, fmt='.',label=r'w$_{p}(r_p)$',color='C1')
ax1.errorbar(N_3d+0.05*N_3d,OmegaM_3d,yerr=OmegaM_err_3d, capsize=0, fmt='.',label=r'$\xi$(r)',color='C2')
ax1.hlines(OmegaM_planck,0,23, color='red')
ax2 = fig.add_subplot(gs[1, :])
ax2.errorbar(N_2d-0.05*N_2d,sigma8_2d, yerr=sigma8_err_2d, capsize=0, fmt='.',label=r'w($\theta$)',color='C0')
ax2.errorbar(N_wp,sigma8_wp, yerr=sigma8_err_wp, capsize=0, fmt='.',label=r'w$_{p}(r_p)$',color='C1')
ax2.errorbar(N_3d+0.05*N_3d,sigma8_3d, yerr=sigma8_err_3d, capsize=0, fmt='.',label=r'$\xi$(r)',color='C2')
ax2.hlines(sigma8_planck,0,23, color='red')
ax3 = fig.add_subplot(gs[2, :])
ax3.errorbar(N_2d-0.05*N_2d, h_2d, yerr=h_err_2d, capsize=0, fmt='.',label=r'w($\theta$)',color='C0')
ax3.errorbar(N_wp, h_wp, yerr=h_err_wp, capsize=0, fmt='.',label=r'w$_{p}(r_p)$',color='C1')
ax3.errorbar(N_3d+0.05*N_3d, h_3d, yerr=h_err_3d, capsize=0, fmt='.',label=r'$\xi$(r)',color='C2')
ax3.hlines(h_planck,0,23, color='red')

ymin, ymax = ax1.get_ylim()
ax1.vlines(N_erass8,ymin, ymax,label='eRASS8',color='black')
ax1.vlines(N_erass1,ymin, ymax,label='eRASS1',color='black',linestyle='dashed')
ymin, ymax = ax2.get_ylim()
ax2.vlines(N_erass8,ymin, ymax,color='black')
ax2.vlines(N_erass1,ymin, ymax,color='black',linestyle='dashed')
ymin, ymax = ax3.get_ylim()
ax3.vlines(N_erass8,ymin, ymax,color='black')
ax3.vlines(N_erass1,ymin, ymax,color='black',linestyle='dashed')


ax1.legend(fontsize=14,bbox_to_anchor=(0, 1.03, 1.0, .3), loc='lower left', ncol=3, mode="expand", borderaxespad=0.)
ax1.tick_params(labelsize=14)
ax2.tick_params(labelsize=14)
ax3.tick_params(labelsize=14)
ax1.set_xscale('log')
ax2.set_xscale('log')
ax3.set_xscale('log')
ax1.grid(True)
ax2.grid(True)
ax3.grid(True)
ax1.set_ylabel(r'$\Omega_M$', fontsize=14)
ax2.set_ylabel(r'$\sigma_8$', fontsize=14)
ax3.set_ylabel(r'$h$', fontsize=14)
ax3.set_xlabel('N', fontsize=14)

fig.tight_layout()
fig.savefig(outpl)


outpl = os.path.join(p_2_out,'all_cosmo_error_trend.png')
fig = plt.figure(figsize=(6,6))
gs = fig.add_gridspec(3,1,hspace=0.0)
ax1 = fig.add_subplot(gs[0, :])
ax1.plot(N_2d,OmegaM_err_2d,label=r'w($\theta$)',color='C0')
ax1.plot(N_wp,OmegaM_err_wp,label=r'w$_{p}(r_p)$',color='C1')
ax1.plot(N_3d,OmegaM_err_3d,label=r'$\xi$(r)',color='C2')
ax2 = fig.add_subplot(gs[1, :])
ax2.plot(N_2d, sigma8_err_2d, label=r'w($\theta$)',color='C0')
ax2.plot(N_wp, sigma8_err_wp, label=r'w$_{p}(r_p)$',color='C1')
ax2.plot(N_3d, sigma8_err_3d, label=r'$\xi$(r)',color='C2')
ax3 = fig.add_subplot(gs[2, :])
ax3.plot(N_2d, h_err_2d, label=r'w($\theta$)',color='C0')
ax3.plot(N_wp, h_err_wp, label=r'w$_{p}(r_p)$',color='C1')
ax3.plot(N_3d, h_err_3d, label=r'$\xi$(r)',color='C2')

ymin, ymax = ax1.get_ylim()
ax1.vlines(N_erass8,ymin, ymax,label='eRASS8',color='black')
ax1.vlines(N_erass1,ymin,ymax,label='eRASS1',color='black',linestyle='dashed')
ymin, ymax = ax2.get_ylim()
ax2.vlines(N_erass8,ymin, ymax,color='black')
ax2.vlines(N_erass1,ymin,ymax,color='black',linestyle='dashed')
ymin, ymax = ax3.get_ylim()
ax3.vlines(N_erass8,ymin, ymax,color='black')
ax3.vlines(N_erass1,ymin,ymax,color='black',linestyle='dashed')


ax1.legend(fontsize=14,bbox_to_anchor=(0, 1.03, 1.0, .3), loc='lower left', ncol=3, mode="expand", borderaxespad=0.)
ax1.tick_params(labelsize=14)
ax2.tick_params(labelsize=14)
ax3.tick_params(labelsize=14)
ax1.grid(True)
ax2.grid(True)
ax3.grid(True)
ax1.set_yscale('log')
ax1.set_xscale('log')
ax2.set_yscale('log')
ax2.set_xscale('log')
ax3.set_yscale('log')
ax3.set_xscale('log')
ax1.set_ylabel(r'$\delta \Omega_M$', fontsize=14)
ax2.set_ylabel(r'$\delta \sigma_8$', fontsize=14)
ax3.set_ylabel(r'$\delta h$', fontsize=14)
ax3.set_xlabel('N', fontsize=14)

fig.tight_layout()
fig.savefig(outpl)


sys.exit()



























outpl = os.path.join(p_2_out,'wp_cosmo_trend.png')
fig = plt.figure(figsize=(6,6))
gs = fig.add_gridspec(3,1,hspace=0.0)
ax1 = fig.add_subplot(gs[0, :])
ax1.errorbar(N,OmegaM,yerr=OmegaM_err, capsize=0, fmt='.')
ax1.hlines(OmegaM_planck,0,10, color='red')
ax2 = fig.add_subplot(gs[1, :])
ax2.errorbar(N,sigma8, yerr=sigma8_err, capsize=0, fmt='.')
ax2.hlines(sigma8_planck,0,10, color='red')
ax3 = fig.add_subplot(gs[2, :])
ax3.errorbar(N, h, yerr=h_err, capsize=0, fmt='.')
ax3.hlines(h_planck,0,10, color='red')


ax1.tick_params(labelsize=14)
ax2.tick_params(labelsize=14)
ax3.tick_params(labelsize=14)
ax1.grid(True)
ax2.grid(True)
ax3.grid(True)
ax1.set_ylabel(r'$\Omega_M$', fontsize=14)
ax2.set_ylabel(r'$\sigma_8$', fontsize=14)
ax3.set_ylabel(r'$h$', fontsize=14)
ax3.set_xlabel('N', fontsize=14)

ax1.set_title(r'$wp(r_p)$', fontsize=14)
fig.tight_layout()
fig.savefig(outpl)

outpl = os.path.join(p_2_out,'wp_cosmo_error_trend.png')
fig = plt.figure(figsize=(6,6))
gs = fig.add_gridspec(3,1,hspace=0.0)
ax1 = fig.add_subplot(gs[0, :])
ax1.plot(N,OmegaM_err)
ax2 = fig.add_subplot(gs[1, :])
ax2.plot(N,sigma8_err)
ax3 = fig.add_subplot(gs[2, :])
ax3.plot(N,h_err)

ax1.tick_params(labelsize=14)
ax2.tick_params(labelsize=14)
ax3.tick_params(labelsize=14)
ax1.grid(True)
ax2.grid(True)
ax3.grid(True)
ax1.set_ylabel(r'$\delta\ \Omega_M$', fontsize=14)
ax2.set_ylabel(r'$\delta\ \sigma_8$', fontsize=14)
ax3.set_ylabel(r'$\delta$ h', fontsize=14)
ax3.set_xlabel('N', fontsize=14)

ax1.set_title(r'$wp(r_p)$', fontsize=14)
fig.tight_layout()
fig.savefig(outpl)




outpl = os.path.join(p_2_out,'monopole_cosmo_trend.png')
fig = plt.figure(figsize=(6,6))
gs = fig.add_gridspec(3,1,hspace=0.0)
ax1 = fig.add_subplot(gs[0, :])
ax1.errorbar(N,OmegaM,yerr=OmegaM_err, capsize=0, fmt='.')
ax1.hlines(OmegaM_planck,0,10, color='red')
ax2 = fig.add_subplot(gs[1, :])
ax2.errorbar(N,sigma8, yerr=sigma8_err, capsize=0, fmt='.')
ax2.hlines(sigma8_planck,0,10, color='red')
ax3 = fig.add_subplot(gs[2, :])
ax3.errorbar(N, h, yerr=h_err, capsize=0, fmt='.')
ax3.hlines(h_planck,0,10, color='red')

ax1.tick_params(labelsize=14)
ax2.tick_params(labelsize=14)
ax3.tick_params(labelsize=14)
ax1.grid(True)
ax2.grid(True)
ax3.grid(True)
ax1.set_ylabel(r'$\Omega_M$', fontsize=14)
ax2.set_ylabel(r'$\sigma_8$', fontsize=14)
ax3.set_ylabel(r'$h$', fontsize=14)
ax3.set_xlabel('N', fontsize=14)

ax1.set_title(r'$\xi(r)$', fontsize=14)
fig.tight_layout()
fig.savefig(outpl)

outpl = os.path.join(p_2_out,'monopole_cosmo_error_trend.png')
fig = plt.figure(figsize=(6,6))
gs = fig.add_gridspec(3,1,hspace=0.0)
ax1 = fig.add_subplot(gs[0, :])
ax1.scatter(N,OmegaM_err)
ax2 = fig.add_subplot(gs[1, :])
ax2.scatter(N,sigma8_err)
ax3 = fig.add_subplot(gs[2, :])
ax3.scatter(N,h_err)

ax1.tick_params(labelsize=14)
ax2.tick_params(labelsize=14)
ax3.tick_params(labelsize=14)
ax1.grid(True)
ax2.grid(True)
ax3.grid(True)
ax1.set_ylabel(r'$\delta\ \Omega_M$', fontsize=14)
ax2.set_ylabel(r'$\delta\ \sigma_8$', fontsize=14)
ax3.set_ylabel(r'$\delta$ h', fontsize=14)
ax3.set_xlabel('N', fontsize=14)

ax1.set_title(r'$\xi(r)$', fontsize=14)
fig.tight_layout()
fig.savefig(outpl)




outpl = os.path.join(p_2_out,'angular_cosmo_trend.png')
fig = plt.figure(figsize=(6,6))
gs = fig.add_gridspec(3,1,hspace=0.0)
ax1 = fig.add_subplot(gs[0, :])
ax1.errorbar(N,OmegaM,yerr=OmegaM_err, capsize=0, fmt='.')
ax1.hlines(OmegaM_planck,0,10, color='red')
ax2 = fig.add_subplot(gs[1, :])
ax2.errorbar(N,sigma8, yerr=sigma8_err, capsize=0, fmt='.')
ax2.hlines(sigma8_planck,0,10, color='red')
ax3 = fig.add_subplot(gs[2, :])
ax3.errorbar(N, h, yerr=h_err, capsize=0, fmt='.')
ax3.hlines(h_planck,0,10, color='red')

ax1.tick_params(labelsize=14)
ax2.tick_params(labelsize=14)
ax3.tick_params(labelsize=14)
ax1.grid(True)
ax2.grid(True)
ax3.grid(True)
ax1.set_ylabel(r'$\Omega_M$', fontsize=14)
ax2.set_ylabel(r'$\sigma_8$', fontsize=14)
ax3.set_ylabel(r'$h$', fontsize=14)
ax3.set_xlabel('N', fontsize=14)

ax1.set_title(r'$w(\theta)$', fontsize=14)
fig.tight_layout()
fig.savefig(outpl)

outpl = os.path.join(p_2_out,'angular_cosmo_error_trend.png')
fig = plt.figure(figsize=(6,6))
gs = fig.add_gridspec(3,1,hspace=0.0)
ax1 = fig.add_subplot(gs[0, :])
ax1.scatter(N,OmegaM_err)
ax2 = fig.add_subplot(gs[1, :])
ax2.scatter(N,sigma8_err)
ax3 = fig.add_subplot(gs[2, :])
ax3.scatter(N,h_err)

ax1.tick_params(labelsize=14)
ax2.tick_params(labelsize=14)
ax3.tick_params(labelsize=14)
ax1.grid(True)
ax2.grid(True)
ax3.grid(True)
ax1.set_ylabel(r'$\delta\ \Omega_M$', fontsize=14)
ax2.set_ylabel(r'$\delta\ \sigma_8$', fontsize=14)
ax3.set_ylabel(r'$\delta$ h', fontsize=14)
ax3.set_xlabel('N', fontsize=14)

ax1.set_title(r'$w(\theta)$', fontsize=14)
fig.tight_layout()
fig.savefig(outpl)

