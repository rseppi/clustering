import sys,os,glob
import numpy as np
import pyccl as ccl
from astropy.table import Table
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import corner
from colossus.cosmology import cosmology
from colossus.lss import bias
import ultranest
from ultranest.plot import PredictionBand
from scipy import integrate

'''
Takes N from keyboard

N=multiplicative factor to rescale the covariance matrix to increase/decrease signal to noise
'''

N = sys.argv[1]

#logdir = os.path.join('/home/rseppi/clustering','python','test0','my_fit_3d')
#outdir = os.path.join('/home/rseppi/clustering','python','test0')
logdir = os.path.join('/u/rseppi/clustering','python','test0','my_fit_3d_N'+N)
outdir = os.path.join('/u/rseppi/clustering','python','test0')


#Use MultiDark cosmology
Om_MD = 0.307115 #matter
Ob_MD = 0.048206 #baryon
Oc_MD = Om_MD-Ob_MD #cold dark matter
h_MD = 0.6766
print('Setting up cosmology...')
cosmo_planck18 = ccl.Cosmology(
    Omega_c = 0.11933/0.6766**2, # 0.26066
    Omega_b = 0.02242/0.6766**2, # 0.04897
    h = h_MD,
    n_s = 0.9665,
    sigma8 = 0.8228,
    #A_s = np.e**(3.047)/10**10, # 2.1052e-09
    Omega_k = 0.0,
    Omega_g = None,
    Neff = 3.04, # standard model of particle physics
    m_nu = 0.0, # in eV
    m_nu_type = None, # 'inverted', 'normal', 'equal' or 'list'
    w0 = -1.0,
    wa = 0.0,
    T_CMB = ccl.physical_constants.T_CMB, # 2.725
    bcm_log10Mc=14.079181246047625, # BCM baryon correction model on PK https://arxiv.org/abs/1510.06034
    bcm_etab=0.5,                   # BCM
    bcm_ks=55.0,                    # BCM
    mu_0=0.0,                       # modified gravity model parameter
    sigma_0=0.0,                    # modified gravity model parameter
    z_mg=None,                      # modified growth rate parameter
    df_mg=None,                     # modified growth rate parameter
    transfer_function='boltzmann_camb',  # for power spectrum analysis
    matter_power_spectrum='halofit',     # for power spectrum analysis
    baryons_power_spectrum='nobaryons',  # for power spectrum analysis
    mass_function='tinker10',            # for mass function analysis
    halo_concentration='duffy2008',      # for mass function analysis
    emulator_neutrinos='strict',         # for power spectrum analysis
)
cosmo = cosmo_planck18

z_mid = np.array([0.2,0.4,0.6])
#halo_mass = np.array([1.827e14,1.7415e14,1.6663e14]) #Msun/h cutting M500c>1e14
halo_mass = np.array([1.05872e14,1.0004e14,9.43983e13]) #Msun/h #no cut
r = np.arange(1.5,150,3) #Mpc/h
xi_fake = []
cov_list = []
for (z,mass) in zip(z_mid,halo_mass):
    cosmopars = {'flat': True, 'H0': 0.6766 * 100, 'Om0': Om_MD, 'Ob0': 0.02242 / 0.6766 ** 2, 'sigma8': 0.8228, 'ns': 0.9665}
    cosmo_colossus = cosmology.setCosmology('newcosmo', cosmopars)
    #b = ccl.halos.hbias.HaloBiasTinker10(cosmo, float(halo_mass / h_MD), a=1 / (1 + z), overdensity=500)
    #bias = ccl.halos.hbias.HaloBiasBhattacharya11(cosmo)
    #b = bias.get_halo_bias(cosmo, M=float(mass / h_MD),a=1 / (1 + z))
    f=ccl.background.growth_rate(cosmo, a=1 / (1 + z))
    b = bias.haloBias(mass, model='comparat17', z=z, mdef='500c')
    beta = f/b
    xi = ccl.correlation_3d(cosmo, a=1 / (1 + z), r=r/h_MD)
    xi = xi * b**2 * (1 + 2./3.*beta + 1./5.*beta**2)
    r2_xi = r ** 2 * xi
    xi_fake.append(r2_xi)

    p_2_cov_mat = os.path.join('/u/rseppi/clustering', 'correlation_functions', 'covariance_matrix',
                               'covariance_matrix_r2_monopole_Zmin_' + str(np.round(z-0.1, 1)) + '_Zmax_' + str(
                                   np.round(z+0.1, 1)) + '.txt')

    cov_mat = np.loadtxt(p_2_cov_mat,unpack=True)
    cov_mat = float(N)*cov_mat
    cov_list.append(cov_mat)



def model(x, z, mass, pars):
    '''
    :param x: array. Separation in Mpc/h
    :param pars: cosmological parameters. Available: OmegaM, sigma8, h, b.
    :return: 3D correlation function xi(r). Same dimension of x
    '''
    #Om,s8,h,b = pars
    Om,s8, h = pars
    xh = x/h
    Oc = Om - Ob_MD
    #nu = peaks.peakHeight(np.average(t['HALO_Mvir']), np.average(t['redshift_S']))
    #b = bias.haloBiasFromNu(nu, model='comparat17')
    cosmol = ccl.Cosmology(
        Omega_c = Oc, # 0.26066
        Omega_b = 0.02242/0.6766**2, # 0.04897
        h = h,
        n_s = 0.9665,
        sigma8 = s8,
        Omega_k = 0.0,
        Omega_g = None,
        Neff = 3.04, # standard model of particle physics
        m_nu = 0.0, # in eV
        m_nu_type = None, # 'inverted', 'normal', 'equal' or 'list'
        w0 = -1.0,
        wa = 0.0,
        T_CMB = ccl.physical_constants.T_CMB, # 2.725
        bcm_log10Mc=14.079181246047625, # BCM baryon correction model on PK https://arxiv.org/abs/1510.06034
        bcm_etab=0.5,                   # BCM
        bcm_ks=55.0,                    # BCM
        mu_0=0.0,                       # modified gravity model parameter
        sigma_0=0.0,                    # modified gravity model parameter
        z_mg=None,                      # modified growth rate parameter
        df_mg=None,                     # modified growth rate parameter
        transfer_function='boltzmann_camb',  # for power spectrum analysis
        matter_power_spectrum='halofit',     # for power spectrum analysis
        baryons_power_spectrum='nobaryons',  # for power spectrum analysis
        mass_function='tinker10',            # for mass function analysis
        halo_concentration='duffy2008',      # for mass function analysis
        emulator_neutrinos='strict',         # for power spectrum analysis
    )

    cosmopars = {'flat': True, 'H0': h * 100, 'Om0': Om, 'Ob0': 0.02242 / 0.6766 ** 2, 'sigma8': s8, 'ns': 0.9665}
    cosmol_colossus = cosmology.setCosmology('newcosmo', cosmopars)

    #b = ccl.massfunction.halo_bias(cosmol, float(mass / h), a=1 / (1 + z), overdensity=500)
    f=ccl.background.growth_rate(cosmol, a=1 / (1 + z))
    b = bias.haloBias(mass, model='comparat17', z=z, mdef='500c')
    beta = f/b
    xi_model = ccl.correlation_3d(cosmol, a=1 / (1 + z), r=xh)
    xi_model = xi_model * b**2 * (1 + 2./3.*beta + 1./5.*beta**2)
    r2_xi_model = x**2 * xi_model
    return r2_xi_model



icov = []
sel=[]
dxi_list = []
for xir, covar in zip(xi_fake, cov_list):
    sel_ = (~np.isnan(xir))&(r>20)&(r<80)
    sel.append(sel_)
    cov = covar[sel_][:,sel_]
    icov_ = np.linalg.inv(cov) #inverse of cov
    icov.append(icov_)
    dxi_list.append(np.sqrt(np.diag(cov)))

#for xir, s, dxi, mass, z in zip(xi_fake, sel, dxi_list, halo_mass, z_mid):
#    plt.errorbar(r[s],xir[s],yerr=dxi, fmt=".", capsize=0)
#    plt.plot(r,model(r,z,mass,[0.31,0.82,0.67]))

#plt.show()

def lnprior(cube):
    pars = cube.copy()
    pars[0] = (cube[0])*0.45 + 0.15  #OmegaM = 0.15 0.4
    pars[1] = (cube[1])*0.6 + 0.5  #sigma8 = 0.5 1.5
    pars[2] = (cube[2])*0.6 + 0.4  #h = 0.4 1.4
    #pars[3] = (cube[3])*5.5 + 0.5  #h = 0.5 5.5
    return pars

def lnlike(pars):
    likelihood = 0
    for xi_r,s,ic,mass,z in zip(xi_fake, sel, icov, halo_mass, z_mid):
        try:
            diff = xi_r[s] - model(r[s], z, mass,pars)
            likelihood += -np.dot(diff,np.dot(ic,diff))/2.0
        except:
            likelihood = -1e20
    return likelihood

names = [r'$\Omega_{M}$', r'$\sigma_8$', 'h']#, 'b']
print('initialize sampler...')
sampler = ultranest.ReactiveNestedSampler(param_names=names, loglike=lnlike, transform=lnprior, log_dir=logdir, resume=True)
print('run sampler...')
result = sampler.run(min_num_live_points=400)

print('print results...')
sampler.print_results()
print('plot sampler...')
sampler.plot()

popt = np.array(result['posterior']['mean'])
pvar = np.array(result['posterior']['stdev'])

fil=open(os.path.join(outdir,'values_3D.txt'),'a+')
#fil.write('OmegaM sigma8 h deltaOmegaM deltasigma8 deltah\n')
fil.write('\n{:.2f} {:.3f} {:.3f} {:.3f} {:.3f} {:.3f} {:.3f}\n'.format(float(N),popt[0],popt[1],popt[2], pvar[0],pvar[1],pvar[2]))
fil.close()

outpl = os.path.join(outdir, 'corr_func_3d_final_N'+N+'.png')
colors = ['C0', 'C1', 'C2', 'C3']

fig3 = plt.figure(figsize=(6,6))
gs3 = fig3.add_gridspec(4,1,hspace=0.0)
ax31 = fig3.add_subplot(gs3[0:3, :])
for xir, s, dxi, mass, z, col in zip(xi_fake, sel, dxi_list, halo_mass, z_mid, colors):
    ax31.errorbar(r[s], (xir[s]), yerr=dxi, fmt=".", capsize=0, label='z={:.1f}'.format(z))
    ax31.plot(r[s],model(r[s],z,mass,popt))

    #r_grid = np.linspace(15, 100, 200)
    #band = PredictionBand(r_grid)
    #for Om_, s8_, h_ in sampler.results['samples']:
    # compute for each time the y value
    #    band.add(model(r_grid, z, mass, [Om_, s8_, h_]))#, b_]))

    #band.line(color=col)
    # add 1 sigma quantile
    #band.shade(alpha=0.4, color=col)#label=r'1$\sigma$')
    # add 2 sigma quantile
    #band.shade(q=0.477, alpha=0.2, color=col)#label=r'2$\sigma$')


ax32 = fig3.add_subplot(gs3[3, :], sharex=ax31)

for xir, s, dxi, mass, z, col in zip(xi_fake, sel, dxi_list, halo_mass, z_mid, colors):
    ax32.errorbar(r[s], xir[s]/model(r[s],z,mass,popt), yerr=dxi/model(r[s],z,mass,popt), fmt=".", capsize=0, label='z={:.1f}'.format(z))

ax32.hlines(1,min(r),max(r), color='black')

ax31.tick_params(labelsize=13)
ax32.tick_params(labelsize=13)
ax31.set_ylim(-100)#,400)
ax32.set_ylim(0.3,1.7)
ax31.set_xlim(15,90)
ax32.set_xlim(15,90)
#plt.yscale('log')
#plt.xscale('log')
ax31.legend(fontsize=13)
ax32.set_xlabel(r'$r$ [Mpc/h]', fontsize=13)
ax31.set_ylabel(r'$r^2\xi(r)$', fontsize=13)
ax32.set_ylabel('Data/Fit', fontsize=13)
ax31.grid(True)
ax32.grid(True)
fig3.tight_layout()
fig3.savefig(outpl, overwrite=True)

print('done!')