import os, glob
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import ticker, cm
from numpy import ma

p_2_xi = sorted(glob.glob(os.path.join('/home/rseppi/clustering','correlation_functions','*3dps.2pcf')))

for p2x in p_2_xi:
    pi, rp, xi, DD_cts, DR_cts, RR_cts = np.loadtxt(p2x, unpack=True, usecols=[0, 1, 2, 3, 4, 6])  # ,dtype='float32')

    pi = np.unique(pi)
    rp = np.unique(rp)
    xi = xi.reshape(len(rp), len(pi))
    xi = np.transpose(xi)
    xi = ma.masked_where(xi<0,xi)

    print(pi.shape,rp.shape,xi.shape)

    xi_new = np.zeros((2*len(pi),2*len(rp)))
    pi_new = np.unique(np.sort(np.append(-pi,pi)))
    rp_new = np.unique(np.sort(np.append(-rp,rp)))

    print(pi_new,rp_new)
    xi_new[:len(pi),:len(rp)] = xi[::-1,::-1]
    xi_new[len(pi):,:len(rp)] = xi[:,::-1]
    xi_new[:len(pi),len(rp):] = xi[::-1]
    xi_new[len(pi):,len(rp):] = xi


    print(pi.shape,rp.shape,xi_new.shape)


    rp_new,pi_new = np.meshgrid(rp_new,pi_new)
    print(pi_new,rp_new)


    outfig = os.path.join('/home/rseppi/clustering','figures','3dps',p2x[-27:-5]+'.png')
    ticks = [0.1,1,10,20,30,40]
    #contours = [0.1,1,5,10,20,25,30,35,40]
    #contours = np.logspace(np.log10(xi.min()+1e-2),np.log10(xi.max()-0.1),5)
    contours = np.round(np.geomspace(0.3,xi.max()-0.1,8),1)
    print(contours)
    levels = np.geomspace(1e-3,xi.max(),1000)
    plt.figure(figsize=(6,6))
    contf = plt.contourf(rp_new,pi_new,xi_new, levels=levels, cmap = cm.coolwarm)
    cont = plt.contour(rp_new,pi_new,xi_new, levels=contours, colors='k')
    cbar = plt.colorbar(contf, ticks=contours)
    cbar.ax.tick_params(labelsize=13)
    cbar.ax.set_yticklabels(contours)
    #plt.xscale('log')
    #plt.yscale('log')
    plt.tick_params(labelsize=13)
    cbar.set_label(r'$\xi(r_p,\pi)$', fontsize=13, labelpad=5)
    plt.xlabel(r'r$_p$ [Mpc/h]', fontsize=13)
    plt.ylabel(r'$\pi$ [Mpc/h]', fontsize=13)
    plt.tight_layout()
    plt.savefig(outfig)

    outfig = os.path.join('/home/rseppi/clustering','figures','3dps',p2x[-27:-5]+'_zoom.png')
    ticks = [0.1,1,10,20,30,40]
    #contours = [0.1,1,5,10,20,25,30,35,40]
    #contours = np.logspace(np.log10(xi.min()+1e-2),np.log10(xi.max()-0.1),5)
    contours = np.round(np.geomspace(0.3,xi.max()-0.1,8),1)
    print(contours)
    plt.figure(figsize=(6,6))
    contf = plt.contourf(rp_new,pi_new,xi_new, levels=levels, cmap = cm.coolwarm)
    cont = plt.contour(rp_new,pi_new,xi_new, levels=contours, colors='k')
    cbar = plt.colorbar(contf, ticks=contours)
    cbar.ax.tick_params(labelsize=13)
    cbar.ax.set_yticklabels(contours)
    #plt.xscale('log')
    plt.xlim(-15,15)
    plt.ylim(-15,15)
    plt.tick_params(labelsize=13)
    cbar.set_label(r'$\xi(r_p,\pi)$', fontsize=13, labelpad=5)
    plt.xlabel(r'r$_p$ [Mpc/h]', fontsize=13)
    plt.ylabel(r'$\pi$ [Mpc/h]', fontsize=13)
    plt.tight_layout()
    plt.savefig(outfig)