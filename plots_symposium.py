import numpy as np
import pyccl as ccl
import matplotlib.pyplot as plt
from colossus.cosmology import cosmology
from colossus.lss import bias
from scipy import integrate


#Use MultiDark cosmology
Om_MD = 0.307115 #matter
Ob_MD = 0.048206 #baryon
Oc_MD = Om_MD-Ob_MD #cold dark matter
h_MD = 0.6766
print('Setting up cosmology...')
cosmo_planck18 = ccl.Cosmology(
    Omega_c = 0.11933/0.6766**2, # 0.26066
    Omega_b = 0.02242/0.6766**2, # 0.04897
    h = h_MD,
    n_s = 0.9665,
    sigma8 = 0.8228,
    #A_s = np.e**(3.047)/10**10, # 2.1052e-09
    Omega_k = 0.0,
    Omega_g = None,
    Neff = 3.04, # standard model of particle physics
    m_nu = 0.0, # in eV
    m_nu_type = None, # 'inverted', 'normal', 'equal' or 'list'
    w0 = -1.0,
    wa = 0.0,
    T_CMB = ccl.physical_constants.T_CMB, # 2.725
    bcm_log10Mc=14.079181246047625, # BCM baryon correction model on PK https://arxiv.org/abs/1510.06034
    bcm_etab=0.5,                   # BCM
    bcm_ks=55.0,                    # BCM
    mu_0=0.0,                       # modified gravity model parameter
    sigma_0=0.0,                    # modified gravity model parameter
    z_mg=None,                      # modified growth rate parameter
    df_mg=None,                     # modified growth rate parameter
    transfer_function='boltzmann_camb',  # for power spectrum analysis
    matter_power_spectrum='halofit',     # for power spectrum analysis
    baryons_power_spectrum='nobaryons',  # for power spectrum analysis
    mass_function='tinker10',            # for mass function analysis
    halo_concentration='duffy2008',      # for mass function analysis
    emulator_neutrinos='strict',         # for power spectrum analysis
)

OmegaM1 = 0.35
sigma81 = 0.7
cosmo_my1 = ccl.Cosmology(
    Omega_c = OmegaM1 - Ob_MD, # 0.26066
    Omega_b = 0.02242/0.6766**2, # 0.04897
    h = h_MD,
    n_s = 0.9665,
    sigma8 = sigma81,
    #A_s = np.e**(3.047)/10**10, # 2.1052e-09
    Omega_k = 0.0,
    Omega_g = None,
    Neff = 3.04, # standard model of particle physics
    m_nu = 0.0, # in eV
    m_nu_type = None, # 'inverted', 'normal', 'equal' or 'list'
    w0 = -1.0,
    wa = 0.0,
    T_CMB = ccl.physical_constants.T_CMB, # 2.725
    bcm_log10Mc=14.079181246047625, # BCM baryon correction model on PK https://arxiv.org/abs/1510.06034
    bcm_etab=0.5,                   # BCM
    bcm_ks=55.0,                    # BCM
    mu_0=0.0,                       # modified gravity model parameter
    sigma_0=0.0,                    # modified gravity model parameter
    z_mg=None,                      # modified growth rate parameter
    df_mg=None,                     # modified growth rate parameter
    transfer_function='boltzmann_camb',  # for power spectrum analysis
    matter_power_spectrum='halofit',     # for power spectrum analysis
    baryons_power_spectrum='nobaryons',  # for power spectrum analysis
    mass_function='tinker10',            # for mass function analysis
    halo_concentration='duffy2008',      # for mass function analysis
    emulator_neutrinos='strict',         # for power spectrum analysis
)

OmegaM2 = 0.25
sigma82 = 0.9
cosmo_my2 = ccl.Cosmology(
    Omega_c = OmegaM2 - Ob_MD, # 0.26066
    Omega_b = 0.02242/0.6766**2, # 0.04897
    h = h_MD,
    n_s = 0.9665,
    sigma8 = sigma82,
    #A_s = np.e**(3.047)/10**10, # 2.1052e-09
    Omega_k = 0.0,
    Omega_g = None,
    Neff = 3.04, # standard model of particle physics
    m_nu = 0.0, # in eV
    m_nu_type = None, # 'inverted', 'normal', 'equal' or 'list'
    w0 = -1.0,
    wa = 0.0,
    T_CMB = ccl.physical_constants.T_CMB, # 2.725
    bcm_log10Mc=14.079181246047625, # BCM baryon correction model on PK https://arxiv.org/abs/1510.06034
    bcm_etab=0.5,                   # BCM
    bcm_ks=55.0,                    # BCM
    mu_0=0.0,                       # modified gravity model parameter
    sigma_0=0.0,                    # modified gravity model parameter
    z_mg=None,                      # modified growth rate parameter
    df_mg=None,                     # modified growth rate parameter
    transfer_function='boltzmann_camb',  # for power spectrum analysis
    matter_power_spectrum='halofit',     # for power spectrum analysis
    baryons_power_spectrum='nobaryons',  # for power spectrum analysis
    mass_function='tinker10',            # for mass function analysis
    halo_concentration='duffy2008',      # for mass function analysis
    emulator_neutrinos='strict',         # for power spectrum analysis
)

cosmo = cosmo_planck18

#z_mid = np.array([0.2,0.4,0.6])
z_mid = 0.2
#halo_mass = np.array([1.827e14,1.7415e14,1.6663e14]) #Msun/h #cutting M500c>1e14
#halo_mass = np.array([1.05872e14,1.0004e14,9.43983e13]) #Msun/h #no cut
halo_mass = 1.05872e14 #Msun/h #no cut
pi = np.arange(0.5,60,1.0)
rp = np.array([ 0.2128236,  0.24099  ,  0.2728843,  0.3089996,  0.3498946,
        0.396202 ,  0.448638 ,  0.5080138,  0.5752477,  0.6513798,
        0.7375878,  0.835205 ,  0.9457416,  1.070907 ,  1.212638 ,
        1.373127 ,  1.554856 ,  1.760636 ,  1.99365  ,  2.257503 ,
        2.556276 ,  2.89459  ,  3.27768  ,  3.71147  ,  4.20267  ,
        4.758879 ,  5.388701 ,  6.101878 ,  6.909441 ,  7.823882 ,
        8.859347 , 10.03185  , 11.35953  , 12.86293  , 14.5653   ,
       16.49296  , 18.67575  , 21.14743  , 23.94622  , 27.11542  ,
       30.70405  , 34.76763  , 39.36901  , 44.57937  , 50.4793   ,
       57.16006  , 64.72501  , 73.29114  , 82.99098  , 93.97456  ])

#for (z,mass) in zip(z_mid,halo_mass):
cosmopars = {'flat': True, 'H0': 0.6766 * 100, 'Om0': Om_MD, 'Ob0': 0.02242 / 0.6766 ** 2, 'sigma8': 0.8228, 'ns': 0.9665}
cosmo_colossus = cosmology.setCosmology('newcosmo', cosmopars)
f = ccl.background.growth_rate(cosmo, a=1/(1+z_mid))
b = bias.haloBias(halo_mass, model='comparat17', z=z_mid, mdef='500c')
beta = f / b
xi_model = np.empty((len(pi), len(rp)))
for jj, pi_ in enumerate(pi):
    xi_model[jj] = ccl.correlation_pi_sigma(cosmo, a=1/(1+z_mid), beta=beta, pi=pi_/h_MD,sig=rp/h_MD)
#in ccl.correlation_pi_sigma beta is used to model redshift space distrotion
#We still need to rescale the correlation function from large scale to halos.
xi_model = xi_model * b**2
wp_model = np.empty(len(rp))
for jj in range(len(wp_model)):
    wp_model[jj] = 2.0*integrate.simps(xi_model[:, jj], pi)
wp_fake = rp * wp_model


cosmo=cosmo_my1
#for (z,mass) in zip(z_mid,halo_mass):
cosmopars = {'flat': True, 'H0': 0.6766 * 100, 'Om0': OmegaM1, 'Ob0': 0.02242 / 0.6766 ** 2, 'sigma8': sigma81, 'ns': 0.9665}
cosmo_colossus = cosmology.setCosmology('newcosmo', cosmopars)
f = ccl.background.growth_rate(cosmo, a=1/(1+z_mid))
b = bias.haloBias(halo_mass, model='comparat17', z=z_mid, mdef='500c')
beta = f / b
xi_model = np.empty((len(pi), len(rp)))
for jj, pi_ in enumerate(pi):
    xi_model[jj] = ccl.correlation_pi_sigma(cosmo, a=1/(1+z_mid), beta=beta, pi=pi_/h_MD,sig=rp/h_MD)
#in ccl.correlation_pi_sigma beta is used to model redshift space distrotion
#We still need to rescale the correlation function from large scale to halos.
xi_model = xi_model * b**2
wp_model = np.empty(len(rp))
for jj in range(len(wp_model)):
    wp_model[jj] = 2.0*integrate.simps(xi_model[:, jj], pi)
wp_fake1 = rp * wp_model

cosmo=cosmo_my2
#for (z,mass) in zip(z_mid,halo_mass):
cosmopars = {'flat': True, 'H0': 0.6766 * 100, 'Om0': OmegaM2, 'Ob0': 0.02242 / 0.6766 ** 2, 'sigma8': sigma82, 'ns': 0.9665}
cosmo_colossus = cosmology.setCosmology('newcosmo', cosmopars)
f = ccl.background.growth_rate(cosmo, a=1/(1+z_mid))
b = bias.haloBias(halo_mass, model='comparat17', z=z_mid, mdef='500c')
beta = f / b
xi_model = np.empty((len(pi), len(rp)))
for jj, pi_ in enumerate(pi):
    xi_model[jj] = ccl.correlation_pi_sigma(cosmo, a=1/(1+z_mid), beta=beta, pi=pi_/h_MD,sig=rp/h_MD)
#in ccl.correlation_pi_sigma beta is used to model redshift space distrotion
#We still need to rescale the correlation function from large scale to halos.
xi_model = xi_model * b**2
wp_model = np.empty(len(rp))
for jj in range(len(wp_model)):
    wp_model[jj] = 2.0*integrate.simps(xi_model[:, jj], pi)
wp_fake2 = rp * wp_model



plt.figure(figsize=(6,6))
plt.plot(rp, wp_fake, lw=3, label='Planck')
plt.plot(rp, wp_fake1, lw=3, label=r'$\Omega_{\rm M}$=%.2f $\sigma_8$=%.2f'%(OmegaM1,sigma81))
plt.plot(rp, wp_fake2, lw=3, label=r'$\Omega_{\rm M}$=%.2f $\sigma_8$=%.2f'%(OmegaM2,sigma82))

plt.tick_params(labelsize=13)
plt.xscale('log')
plt.legend(fontsize=13)
plt.xlabel(r'$r_p$ [Mpc/h]',fontsize=15)
plt.ylabel(r'$r_p w_p(r_p)$',fontsize=15)
plt.grid(True)
plt.tight_layout()
#plt.savefig(outpl, overwrite=True)

plt.show()




