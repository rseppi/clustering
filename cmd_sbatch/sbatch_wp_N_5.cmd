#!/bin/bash -l
#SBATCH -J 3D_N5_RPPI_CLUSTERING
#SBATCH -o ./logs/clustering_wp_N5.out.%j
#SBATCH -e ./logs/clustering_wp_N5.err.%j
#SBATCH -D ../
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1
#SBATCH --cpus-per-task=96  #   I want one CPU per walker
#SBATCH --time=18:00:00

module load intel/19.1.3 impi/2019.9 mpi4py/3.0.3
conda activate clustering_conda

# Run the program:
echo "SLURM_JOB_NUM_NODES ${SLURM_JOB_NUM_NODES}"
echo "SLURM_NTASKS_PER_NODE ${SLURM_NTASKS_PER_NODE}"
echo "SLURM_CPUS_PER_TASK ${SLURM_CPUS_PER_TASK}"
echo "SLURM_MEM_PER_NODE ${SLURM_MEM_PER_NODE}"
n="$((${SLURM_JOB_NUM_NODES} * ${SLURM_NTASKS_PER_NODE} * ${SLURM_CPUS_PER_TASK}))"
echo "TOTAL CPUS ${n}"

srun python3 python/004_fit_clustering_wp.py 5.0