# Clustering in eROSITA

** Required packages **

First, we need to install anaconda. Let's start from the home directory and install what we need in .local. This allows to store packages in .local, leaving your home free. 
The following instructions download the Anaconda version from November 2020.
These instructions are tested for RAVEN.
Let's load some modules to make sure that the packages we install support parallel processing.
```
module load gcc/10
module load intel/19.1.3 impi/2019.9
module load gsl/2.4
module load mpip/3.4.1
```
When required, make sure you specify the directory where you want to install anaconda. For example:
/u/your_username/.local/anaconda3
```
cd ~
cd .local
wget https://repo.continuum.io/archive/Anaconda3-2020.11-Linux-x86_64.sh
bash Anaconda3-2020.11-Linux-x86_64.sh 
```

When asked if you want to intialize conda at the start of each session, reply 'yes' (defalut is 'no').
This adds anaconda to your PATH. Now you can activate anaconda in the current session.

```
cd ~
source .bashrc
```

Now we can create an environment with required packages. 
```
conda create --name clustering_conda python=3.8.5
```
Reply 'yes' when asked to install latest packages.

```
conda activate clustering_conda
conda install astropy
pip install corner
pip install colossus
conda install -c conda-forge pyccl
conda install -c conda-forge emcee
pip install cython
pip install ultranest
cd .local
git clone https://github.com/manodeep/Corrfunc/
cd Corrfunc
make
make install
python -m pip install . 
cd ..
git clone https://github.com/damonge/CUTE.git
cd CUTE/CUTE
```

With the current version of CUTE (01.03.2021), it is necessary to modify the make file to complete the installation.
Open the Makefile with your favourite text editor, e.g.

```
vim Makefile
```
Now comment and replace the lines with gsl instructions.
```
#GSL_INC = /home/alonso/include
#GSL_LIB = /home/alonso/lib
GSL_INC = ${GSL_HOME}/include
GSL_LIB = ${GSL_HOME}/lib
```
Save the changes. Now you are ready to compile.
```
make CUTE
make clean
```
In order to run CUTE on raven, I had to specify to the gcc compiler where the gsl library is.
The computation of the correlation function takes ~10 seconds. I created one sbatch example command to do it:
```
sbatch sbatch_corrfunc.cmd
```
This runs the code in python/single_command/002_tabulate_clustering_CUTE_single_command.py
There are 11 other similar commands stored on raven, computing the remaining 11 correlation functions.
Alternatively, you can do it interactively in raven (not recommended). 
You can do it directly from terminal, typing:
```
module load gcc/10 impi/2019.9 gsl/2.4 mpip/3.4.1
export OMP_NUM_THREADS=1
LD_LIBRARY_PATH=${GSL_HOME}/lib
export LD_LIBRARY_PATH
export OMP_PLACES=cores
srun -n 1 -p intractive python3 python/single_command/002_tabulate_clustering_CUTE_single_command.py
```
This computes the 3dps correlation function between z=0.1 and 0.3.
Similar commands are available for angular and monopole correlation functions and in different z intervals.

The correlation functions are stored in the `correlation_functions` directory.

In order to compute covariance matrixes with bootstrap method, I created a single exectuable to compute correlation functions without MPI.
This can be done by going to the CUTE/CUTE directory, commenting the MPI and OpenMP option in the Makefile. 
Before executing the 'make CUTE' command, make sure that you are not overwriting you CUTE executable with MPI!!
E.g.:
```
mv CUTE CUTE_0
make CUTE
mv CUTE CUTE_NO_MPI
mv CUTE_0 CUTE
make clean
```

This can be run one single time using a dedicated sbatch script
```
sbatch -a 1-8%1 sbatch_cov_mat.cmd
```
It computes 100 bootstrap samples for each correlation function needed.
As of 11.03.2021 it's:
4 z_slices * 3 2pcf * 100 bootstrap = 1200 total correlation functions.
Depending on the size of the sample this could be a long process, because it's not parallelized, otherwise CUTE computes more CFs at the same time and crashes.
For me, on raven, it takes ~10 hours. It creates many files (bootstrap catalogs, input files and 2pcf), that are deleted at the end.
 
Now we can fit for cosmology. 
The code is parallelized on raven. It runs ultranest and fits for OmegaM, sigma8, h. You can run it using the queue system slurm.
The following commands will run 8 jobs in series, i.e. the second one starts when the first one is finished
and keeps appending the chains. 

```
sbatch -a 1-8%1 sbatch_2d.cmd
sbatch -a 1-8%1 sbatch_wp.cmd
sbatch -a 1-8%1 sbatch_3d.cmd
```
